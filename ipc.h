/*
 * ipc.h
 *
 *  Created on: Apr 28, 2015
 *      Author: alberto
 */

#ifndef IPC_H_
#define IPC_H_

#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>

///
// Defines for PIPES
///
#if (defined APP_USE_PIPES)
	#define IPC_FIFO_RX_BUFFER_SIZE			256
#endif // #if (defined APP_USE_PIPES)

///
// defines for SHARED MEMORIES
///
#define IPC_SHARED_MEMORY_MAX_LEN 			255
#define IPC_SHARED_MEMORY_ENTRIES_NUM		100

typedef enum
{
	IPC_DATA_IS_UNKNOWN  = 0,
	IPC_DATA_IS_TEMP     = 1,
	IPC_DATA_IS_HUMIDITY = 2,
	IPC_DATA_IS_ACC_X	 = 3,
	IPC_DATA_IS_ACC_Y	 = 4,
	IPC_DATA_IS_ACC_Z	 = 5,
	IPC_DATA_IS_GYRO_X	 = 6,
	IPC_DATA_IS_GYRO_Y	 = 7,
	IPC_DATA_IS_GYRO_Z	 = 8,
} IPC_dataTypeEnumTy;

typedef struct
{
	IPC_dataTypeEnumTy type;
	float data;
} IPC_dataBufferTy;

typedef struct
{
	tBool used;
	tU8 writerId;
	float time;
	float temperature;
	float humidity;
	tU8 len;
	IPC_dataBufferTy dataBfr[IPC_SHARED_MEMORY_MAX_LEN];
} IPC_sharedMemoryElemTy;
typedef struct
{
	IPC_sharedMemoryElemTy entry[IPC_SHARED_MEMORY_ENTRIES_NUM];
} IPC_sharedMemoryRegionTy;

typedef enum
{
	IPC_MEMORY_POOL_FREE				= 0,
	IPC_MEMORY_POOL_LOCKED				= 1,
	IPC_MEMORY_POOL_SCHEDULED_FOR_USE	= 2
} IPC_memoryPoolStatusEnumTy;

typedef std::vector<boost::tuple<float, float, float, float> > dataVectTy;

extern tSInt IPC_FifoCreateAndOpen (void);

extern tSInt IPC_FifoCheckReceivedBytes (void);

extern tSInt IPC_FifoGetBytes (tPU8 dataPtr, tSInt maxData);

extern tSInt IPC_FifoWrite (void);

extern tSInt IPC_FifoClose (void);

extern tSInt IPC_SharedMemoryCreateAndOpen (void);

extern IPC_sharedMemoryRegionTy *IPC_SharedMemoryGetDataPointer (void);

extern IPC_sharedMemoryElemTy *IPC_SharedMemoryGetWriterDataPointer (void);

extern IPC_sharedMemoryElemTy *IPC_SharedMemoryGetReaderDataPointer (void);

extern tSInt IPC_MemoryPoolCreate (void);

extern tSInt IPC_MemoryPoolLockForWrite (void);

extern tSInt IPC_MemoryPoolUnLock (tSInt id);

extern tSInt IPC_MemoryPoolScheduleForUse (tSInt id);

extern tSInt IPC_MemoryPoolCheckForScheduled (void);

extern tSInt IPC_MemoryPoolPush (tSInt id, float data1, float data2, float data3, float data4);

extern tSInt IPC_MemoryPoolGetSize (tSInt id);

extern dataVectTy& IPC_MemoryPoolGetVectorReference (tSInt id);

#if 0
class IPC_memoryPool
{
	public:
		template <class TYPE> tSInt  IPC_MemoryPoolPush (TYPE data);

	private:
		std::vector<boost::tuple<double, double> > IPC_dataVector;
};
#endif // #if 0

#endif /* IPC_H_ */
