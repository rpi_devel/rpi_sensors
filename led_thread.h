/*
 * led_thread.h
 *
 *  Created on: Dec 2, 2014
 *      Author: alberto
 */

#ifndef LED_THREAD_H_
#define LED_THREAD_H_

extern void* LED_Thread (void *arg);

extern void LED_ExitThread (bool exitThread);

extern int LED_SignalHandler (void);

#endif // LED_THREAD_H_

// End of file
