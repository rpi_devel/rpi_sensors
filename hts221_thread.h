/*
 * hts221_thread.h
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#ifndef HTS221_THREAD_H_
#define HTS221_THREAD_H_

namespace HTS221_data
{
	const int MAX = 30;

	class device
	{
		public:
	    	device () {}
	};

};

extern void* HTS221_Thread (void *arg);

extern void HTS221_ExitThread (tBool exitThread);

extern tS32 HTS221_ReadTemperatureAndHumiditySequence (void);

extern tS32 HTS221_SignalHandler (void);

#endif // HTS221_THREAD_H_

// End of file
