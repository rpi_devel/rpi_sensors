/*
 * defines.h
 *
 *  Created on: Mar 20, 2015
 *      Author: alberto
 */

#ifndef DEFINES_H_
#define DEFINES_H_

// Error codes
#define ERROR_CODE_NO_ERROR			((int)0)
#define ERROR_CODE_GENERIC			((int)-1)
#define ERROR_CODE_I2C_ERROR		((int)-2)
#define ERROR_CODE_RESOURCE_UNAVAIL	((int)-3)
#define ERROR_CODE_SYSCALL_FAILED	((int)-4)
#define ERROR_CODE_GPIO_ACCESS_ERR 	((int)-5)

#define INLINE              		__inline    // specifies a routine to be in line
#define CONST               		const       // defines a constant item

#ifndef NULL
    #ifdef __cplusplus
        #define NULL            0
    #else
        #define NULL            ((void*)0)
    #endif
#endif

#ifndef TRUE
    #define TRUE                (1)
    #define SET                 TRUE
    #define true                TRUE
#endif

#ifndef FALSE
    #define FALSE               (0)
    #define CLEAR               FALSE
    #define false               FALSE
#endif

#define ARRAY_SIZE(x)			(sizeof(x) / sizeof((x)[0]))

#define C_UNUSED(x)				((void)x)

#ifdef __GNUC__
    #define MAYBE_UNUSED __attribute__((used))
#else
    #define MAYBE_UNUSED
#endif

#endif // DEFINES_H_

// End of file

