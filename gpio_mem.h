extern int gpio_mem_open (void);

extern int gpio_mem_set_direction (int gpioNum, GPIODEV_directionEnumTy direction);

extern GPIODEV_valEnumTy gpio_mem_get_value (int gpioNum);

extern int gpio_mem_set_value (int gpioNum, GPIODEV_valEnumTy value);

extern int gpio_mem_close (int gpioNum);

// End of file
