#ifndef LSM6DS3_LLD_H_
#define LSM6DS3_LLD_H_


/*
 * lsm6ds3_lld.h.h
 *
 *  Created on: Apr 12, 2015
 *      Author: sandro
 */


extern tS32 LSM6DS3_Init (void);

extern tS32 LSM6DS3_Open (void);

extern tS32 LSM6DS3_Close (void);

extern tS32 LSM6DS3_ReadAccelerationSequence (tSInt memoryPoolId);

extern void LSM6DS3_AccDataDisplay (void);

extern void LSM6DS3_SetAccSamplesNum (int AccSamplesNum);

extern void LSM6DS3_SetAccRange (int AccelerationRange);

extern void LSM6DS3_SetOutputDataRate (int OutputDatarate);

// End of file

#endif /* LSM6DS3_LLD_H_ */
