#include <iostream>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>	// For pthread
#include <bcm2835.h>	// For GPIO, I2C and SPI
#include <time.h>		// For sleep() and usleep()
#include <sys/time.h>	// For sleep() and usleep()
#include <semaphore.h>  // For semaphores
#include <termios.h>

#include <cstdlib>

#include <vector>
#include <tuple>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>

#include <boost/lexical_cast.hpp>

#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>

#include "target_config.h"

#include "types.h"
#include "defines.h"

#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
	#include "gpio_common.h"
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

#if (defined SWITCH_APP_ACTIVE)
	#include "gpio_thread.h"
#endif // #if (defined MOTOR_APP_ACTIVE)

#if (defined MOTOR_APP_ACTIVE)
	#include "motor_thread.h"
#endif // #if (defined MOTOR_APP_ACTIVE)

#include "i2c_lld.h"

#include "led_thread.h"
#include "spi_thread.h"
#include "i2c_thread.h"
#include "hts221_thread.h"
#include "lps25h_thread.h"
#include "lsm6ds0_thread.h"
#include "lsm6ds0_lld.h"
#include "lsm6ds3_lld.h"
#include "service_thread.h"
#include "ipc.h"

#if (defined APP_USE_PLOT_CAPABILITY)
	#include "plot_data.h"
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

using namespace std;
using namespace boost;
using namespace boost::algorithm;

using namespace LSM6DS0_data;

#define APP_USE_NEW_CONSOLE
//#undef APP_USE_NEW_CONSOLE

#define LSM6DS0_I2C_ADD			0x6B

/* typedef */ struct AccVal {
	    int	TimeTag_us ;
	    short int X_Val;
	    short int Y_Val;
	    short int Z_Val;
};

#define ACC_DATA_SIZE 0x1000

struct AccVal AccValList [ ACC_DATA_SIZE ] ;
int AccDataIdx_ ;

#if (defined GPIO_USE_LEDS)
	static pthread_t ledTid;
#endif // #if (defined GPIO_USE_LEDS)

#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
	static int GpioThread_ret = -1;
	static pthread_t gpioTid;
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

#if (defined USE_SPI_DEV)
	static pthread_t spiTid;
#endif // #if (defined USE_SPI_DEV)

#if (defined USE_I2C_DEV)
	static pthread_t i2cTid;
#endif // #if (defined USE_I2C_DEV)

#if (defined I2C_TEST_CODE)
	static void I2C_TestMode (void);
	static int GetUserInput (char * returnStr, int maxStringLength);
	static void AcqAccelData ( void );
#endif // #if (defined I2C_TEST_CODE)

#if (defined APP_USE_SERVICE_THREAD)
	static pthread_t stTid;
	static int ST_thread_ret  = -1;
#endif // #if (defined APP_USE_SERVICE_THREAD)

#if (defined SENSOR_USE_HTS221)
	static pthread_t hts221Tid;
	static int HTS221_thread_ret  = -1;
#endif // #if (defined SENSOR_USE_HTS221)

#if (defined SENSOR_USE_LPS25H)
	static pthread_t lps25hTid;
	static int LPS25H_thread_ret  = -1;
#endif // #if (defined SENSOR_USE_LPS25H)

#if (defined SENSOR_USE_LSM6DS0 || defined SENSOR_USE_LSM6DS3 )
	static pthread_t AccGyroId;
	static int AccGyroThread_ret = -1;
#endif // #if (defined SENSOR_USE_LSM6DS0)

// Define data for semaphores
#if (defined APP_USE_SEMAPHORES)
	static sem_t mySemaphore;
#endif // #if (defined APP_USE_SEMAPHORES)

#if (defined APP_USE_CONSOLE)
	static char GetMenuSelection (void);
	static char GetCharFromConsole (void);
	static void SetConsole (void);
#endif // #if (defined APP_USE_CONSOLE)

#if (defined APP_REDIRECT_STDOUT)
	static tSInt RedirectOutput (void);
#endif // #if (defined APP_REDIRECT_STDOUT)

#if (defined APP_USE_PIPES)
	static tSInt GetUserSelection (tChar &userCmd, tSInt &accGyroFs, tSInt &accGyroOdr);
	static vector<string> StrSplitter (std::string text, std::string pattern);
	static std::string StrFinder (vector<string> strVec, std::string pattern);
#endif // #if (defined APP_USE_PIPES)

static void CloseJobs (void);

#if (defined SENSOR_USE_HTS221)
	static MAYBE_UNUSED tSInt StartHumidityTemperature (void *threadArgsPtr);
	static MAYBE_UNUSED tSInt StopHumidityTemperature (void);
#endif // #if (defined SENSOR_USE_HTS221)

#if (defined SENSOR_USE_LPS25H)
	static MAYBE_UNUSED tSInt StartPressure (void *threadArgsPtr);
	static MAYBE_UNUSED tSInt StopPressure (void);
#endif // #if (defined SENSOR_USE_LPS25H)

#if (defined SENSOR_USE_LSM6DS0)
	static MAYBE_UNUSED tSInt StartAccGyro (void *threadArgsPtr);
	static MAYBE_UNUSED tSInt StopAccGyro (void);
#endif // #if (defined SENSOR_USE_LSM6DS0)

#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
	static MAYBE_UNUSED tSInt StartApplicationUsingGpio (void *threadArgsPtr, tBool serviceThread);
	static MAYBE_UNUSED tSInt StopApplicationUsingGpio (void);
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

int main (void)
{
    void *threadArgsPtr;
	tU8 initRes;
	tSInt ret;

#if (defined APP_USE_CONSOLE) || (defined APP_USE_PIPES)
    bool ExitFlag = false;
#endif // #if (defined APP_USE_CONSOLE) || (defined APP_USE_PIPES)

#if (defined APP_USE_CONSOLE)
	char MenuSelection;
	tSInt AccSamplesNum;
    tSInt AccRange;
    tSInt AccGyroDeviceId = 0 ;
    tSInt OutputDataRate ;
#endif // #if (defined APP_USE_CONSOLE)

#if (defined APP_USE_PIPES)
	tSInt fifoRes;
	tSInt fifoRxBytesNum;
	tChar inputSel;
	tSInt accGyroFs, accGyroOdr;
#endif // #if (defined APP_USE_PIPES)

#if (defined I2C_TEST_CODE)
	char buf[20] = {0};
#endif // #if (defined I2C_TEST_CODE)

#if (defined SWITCH_APP_ACTIVE)
    tU8 gpioList[] = {GPIO_23, GPIO_24, GPIO_25, GPIO_17, GPIO_27, GPIO_22};
#endif // #if (defined SWITCH_APP_ACTIVE)

#if (defined APP_USE_CONSOLE) && (defined APP_USE_NEW_CONSOLE)
	// Set console to avoid echoing characters
	SetConsole ();
#endif // #if (defined APP_USE_CONSOLE) && (defined APP_USE_NEW_CONSOLE)

#if (defined APP_REDIRECT_STDOUT)
	if (ERROR_CODE_NO_ERROR != RedirectOutput ())
	{
		cout << "Failed to redirect output to file" << '\n';
    }
#endif // #if (defined APP_REDIRECT_STDOUT)

#if (defined APP_USE_PLOT_CAPABILITY)
	PLOT_Init ();

	//PLOT_Draw ();
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

	// Initialize bcm2835 library
	initRes = bcm2835_init ();

	if (0 == initRes)
	{
		cout << "Failed to initialize bcm2835" << '\n';

		return ERROR_CODE_GENERIC;
    }

#if (defined APP_USE_SEMAPHORES)
	// We use semaphores
	printf ("Creating semaphore\n");

	// Create semaphore
	if (-1 == sem_init (&mySemaphore, 0, 1))
	{
		printf ("Error creating semaphore\n");
	}

	// Set threads argument
	threadArgsPtr = &mySemaphore;
#endif // #if (defined APP_USE_SEMAPHORES)

#if (defined APP_USE_SHAREDMEMS)
	// Create shared memory object and set its size
	if (ERROR_CODE_NO_ERROR != IPC_SharedMemoryCreateAndOpen ())
	{
		printf ("Error creating shared memories\n");
	}
#endif // #if (defined APP_USE_SHAREDMEMS)

#if (defined APP_USE_MEMORYPOOL)
	// Initialize the memory pool
	IPC_MemoryPoolCreate ();
#endif  // #if (defined APP_USE_MEMORYPOOL)

#if (defined APP_USE_SERVICE_THREAD)
	printf ("Creating service thread\n");

	// Create the thread for the HTS221 sensor
	ST_thread_ret = pthread_create (&stTid, NULL, &ST_Thread, threadArgsPtr);

    if (0 != ST_thread_ret)
    {
        printf ("\nCan't create service thread :[%s]", strerror (ST_thread_ret));
    }
    else
    {
        printf ("\nService thread created successfully\n");
    }
#endif // #if (defined APP_USE_SERVICE_THREAD)

#if (defined SWITCH_APP_ACTIVE)
    // Init GPIOs
    printf ("Initialize GPIOs\n");

    if (ERROR_CODE_NO_ERROR != GPIO_Init (gpioList, sizeof (gpioList)))
    {
    	printf ("ERROR initializing GPIOs\n");
    }
    else
    {
    	// Set direction out
    	for (tSInt cnt = 0; cnt < (tSInt)sizeof (gpioList); cnt++)
    	{
    		if (GPIODEV_DIRECTION_OUT != GPIO_SetDirection (gpioList[cnt], GPIODEV_DIRECTION_OUT))
    		{
    			printf ("ERROR setting direction for GPIO %d\n", gpioList[cnt]);
    		}
    	}

    	// Set the GPIOs values to HIGH (here we would like to fix the values to a defined value)
    	for (tSInt cnt = 0; cnt < (tSInt)sizeof (gpioList); cnt++)
    	{
    		if (GPIODEV_VALUE_HIGH != GPIO_SetValue (gpioList[cnt], GPIODEV_VALUE_HIGH))
    		{
    			printf ("ERROR setting value %d for GPIO %d\n", GPIODEV_VALUE_HIGH, gpioList[cnt]);
    		}
    	}

        // Create the service thread for the GPIOs (service thread accept commands)
        StartApplicationUsingGpio (NULL, true);
    }
#endif // #if (defined SWITCH_APP_ACTIVE)

#if (defined MOTOR_APP_ACTIVE)
    // Create the service thread for the GPIOs (service thread accept commands).
    // We invoke with parameters 1 = false to use the normal threada nd not the service one
    // (it does listen to command from the user)
    StartApplicationUsingGpio (NULL, false);
#endif // #if (defined MOTOR_APP_ACTIVE)

#if (defined APP_USE_PIPES)
	// Create FIFO for Inter-Process communication
	printf ("Creating FIFO for IPC\n");

	fifoRes = IPC_FifoCreateAndOpen ();
	if (ERROR_CODE_NO_ERROR != fifoRes)
	{
		printf ("ERROR: FIFO creation failed %d\n", fifoRes);
	}

	// Poll for bytes in the FIFO
	fifoRxBytesNum = 0;

	while (false == ExitFlag)
	{
		fifoRxBytesNum = IPC_FifoCheckReceivedBytes ();

		if (fifoRxBytesNum > 0)
		{
			// Get the user command from the IPC
			GetUserSelection (inputSel, accGyroFs, accGyroOdr);

			// We received some data, time to do some action
			if (0x41 == inputSel) 								// 'A'
			{
#if (defined SENSOR_USE_HTS221)
				StartHumidityTemperature (threadArgsPtr);
#else
				printf ("Humidity feature is not activated\n");
#endif // #if (defined SENSOR_USE_HTS221)
			}
			else if (0x42 == inputSel) 							// 'B'
			{
#if (defined SENSOR_USE_HTS221)
				StopHumidityTemperature ();
#else
				printf ("Humidity feature is not activated\n");
#endif // #if (defined SENSOR_USE_HTS221)
			}
			else if (0x45 == inputSel) 							// 'E'
			{
#if (defined SENSOR_USE_LSM6DS0)
				// Set the plot scale depending on the accelerometer FS used
#if (defined APP_USE_PLOT_CAPABILITY)
				PLOT_setScale (accGyroFs);
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

				// Set user-selected device
				AccGyroSelect ((tSInt)LSM6DS0_DEVICE);

				// CHECKME: Set output data rate. This call is not really implemented
				LSM6DS0_SetOutputDataRate (accGyroOdr);

				// Set acc range
				LSM6DS0_SetAccRange (accGyroFs);

				// Start thread
				StartAccGyro (threadArgsPtr);
#else
				printf ("Accelerometer/Gyroscope feature is not activated\n");
#endif // #if (defined SENSOR_USE_LSM6DS0)
			}
			else if (0x47 == inputSel) 							// 'G'
			{
#if (defined SENSOR_USE_LSM6DS0)
				// Set the plot scale depending on the accelerometer FS used
#if (defined APP_USE_PLOT_CAPABILITY)
				PLOT_setScale (accGyroFs);
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

				// Set user-selected device
				AccGyroSelect ((tSInt)LSM6DS3_DEVICE);

				// Set data rate
				LSM6DS3_SetOutputDataRate (accGyroOdr);

				// Set acc range
				LSM6DS3_SetAccRange (accGyroFs);

				// Start thread
				StartAccGyro (threadArgsPtr);
#else
				printf ("Accelerometer/Gyroscope feature is not activated\n");
#endif // #if (defined SENSOR_USE_LSM6DS0)
			}
			else if (0x46 == inputSel) 							// 'F'
			{
#if (defined SENSOR_USE_LSM6DS0)
				StopAccGyro ();
#else
				printf ("Accelerometer/Gyroscope feature is not activated\n");
#endif // #if (defined SENSOR_USE_LSM6DS0)
			}
			else if (0x53 == inputSel)							// 'S'
			{
#if (defined SWITCH_APP_ACTIVE)
				tSInt slot = accGyroFs - 1; // The channels are off by one to the used slots
				tSInt gpioA, gpioB;
				tSInt gpioNum = (tSInt)sizeof (gpioList);

				// Every slot shall activate a couple of GPIOs so we calculate here the numbers
				gpioA = slot * 2;
				gpioB = gpioA + 1;

				// Use parameters in following mode:
				// - inputSel   : command, it is 'S' for GPIOs
				// - accGyroFS  : channel number, it is translated in the proper GPIO,
				//                only initialized GPIOs are accepted
				// - accGyroOdr : level to set the GPIO, accepted values are 0 and 1
				if ((gpioA >= 0) && (gpioA < gpioNum) && (gpioB < gpioNum))
				{
					if (0 == accGyroOdr)
					{
						GPIO_Command (gpioList[gpioA], GPIO_CMD_SET_VALUE_HIGH);
						GPIO_Command (gpioList[gpioB], GPIO_CMD_SET_VALUE_HIGH);
					}
					else
					{
						GPIO_Command (gpioList[gpioA], GPIO_CMD_SET_VALUE_LOW);
						GPIO_Command (gpioList[gpioB], GPIO_CMD_SET_VALUE_LOW);
					}
				}
				else
				{
					printf ("ERROR: channel is not valid, accepted values are %d\n", (sizeof (gpioList) - 1));
				}
#else
				printf ("GPIO feature is not activated\n");
#endif // #if (defined SWITCH_APP_ACTIVE)
			}
			else if (0x58 == inputSel)							// 'X'
			{
				ExitFlag = true;
			}
		}

		sleep (1);
	}

	// Now close the FIFO
	IPC_FifoClose ();
#endif // #if (defined APP_USE_PIPES)

#if (defined SENSOR_USE_HTS221) && (defined SENSOR_USE_HTS221_TEST_MODE)
	printf ("Creating HTS221 thread\n");

	// Create the thread for the HTS221 sensor
    ret = pthread_create (&hts221Tid, NULL, &HTS221_Thread, threadArgsPtr);

    if (0 != ret)
    {
        printf ("\nCan't create HTS221 thread :[%s]", strerror (ret));
    }
    else
    {
        printf ("\nHTS221 thread created successfully\n");
    }
#endif // #if (defined SENSOR_USE_HTS221) && (defined SENSOR_USE_HTS221_TEST_MODE)

#if (defined SENSOR_USE_LPS25H) && (defined SENSOR_USE_LPS25H_TEST_MODE)
    printf ("Creating LPS25H thread\n");

    // Create the thread for LPS25H sensor
    ret = pthread_create (&lps25hTid, NULL, &LPS25H_Thread, threadArgsPtr);

    if (0 != ret)
    {
        printf ("\nCan't create LPS25H thread :[%s]", strerror (ret));
    }
    else
    {
        printf ("\nLPS25H thread created successfully\n");
    }
#endif // #if (defined SENSOR_USE_LPS25H) && (defined SENSOR_USE_LPS25H_TEST_MODE)

#if (defined SENSOR_USE_LSM6DS0) && (defined SENSOR_USE_LSM6DS0_TEST_MODE)
    // Test code for the LSM6DS0/3 sensor
    printf ("Creating Acc/Gyro thread\n");

	// Create the thread for the Acc/Gyro sensor
	AccGyroThread_ret = pthread_create (&AccGyroId, NULL, &AccGyro_Thread, threadArgsPtr);

	if (0 != AccGyroThread_ret)
	{
		printf ("\nCan't create Acc/Gyro thread :[%s]", strerror (AccGyroThread_ret));
    }
	else
	{
	    printf ("\Acc/Gyro thread created successfully\n");
    }

    while (1)
    {
	      // We stay here because the job is done in the thread
    	sleep(1);
    }
#endif // #if (defined SENSOR_USE_LSM6DS0) && (defined SENSOR_USE_LSM6DS0_TEST_MODE)

#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
    printf ("Creating GPIO USER thread\n");

    // Create the thread for the GPIOs
    StartApplicationUsingGpio (NULL, false);

    while (1) {};
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

#if (defined GPIO_USE_LEDS)
    printf ("Creating GPIO-LED thread\n");

    // Create the thread for the LEDs
    ret = pthread_create (&ledTid, NULL, &LED_Thread, NULL);

    if (0 != ret)
    {
        printf ("\nCan't create LED thread :[%s]", strerror (ret));
    }
    else
    {
        printf ("\nLED thread created successfully\n");
    }
#endif // #if (defined GPIO_USE_LEDS)

#if (defined USE_SPI_DEV)
    printf ("Creating SPI thread\n");

    // Create the thread for the SPI
    ret = pthread_create (&spiTid, NULL, &SPI_Thread, NULL);

    if (0 != ret)
    {
        printf ("\nCan't create SPI thread :[%s]", strerror (ret));
    }
    else
    {
        printf ("\nSPI thread created successfully\n");
    }
#endif // #if (defined USE_SPI_DEV)

#if (defined I2C_TEST_CODE)
	I2C_TestMode ();
#endif // #if (defined I2C_TEST_CODE)

#if (defined USE_I2C_DEV)
	printf ("Creating I2C thread\n");

	// Create the thread for the I2C
    ret = pthread_create (&i2cTid, NULL, &I2C_Thread, NULL);

    if (0 != ret)
    {
        printf ("\nCan't create I2C thread :[%s]", strerror (ret));
    }
    else
    {
        printf ("\nI2C thread created successfully\n");
    }
#endif // #if (defined USE_I2C_DEV)

#if (defined I2C_TEST_CODE)
    // Getting user input or exit
    while (1)
    {
    	printf("Enter commands [device (SPI, I2C) - mode (READ, WRITE) - length - bytes to write]:\n");
    	fflush (stdout);

    	// Get user input
    	GetUserInput (buf, sizeof (buf));
    	fflush (stdin);
    }
#endif // #if (defined I2C_TEST_CODE)

#if (defined APP_USE_CONSOLE)
    // Stay here until enter is pressed
	while ( false == ExitFlag )
	{
		MenuSelection = GetMenuSelection() ;

		switch (MenuSelection)
		{
        	case 'A':
        		StartHumidityTemperature (threadArgsPtr);
        	break;

        	case 'B':
        		StopHumidityTemperature ();
        	break;

        	case 'C':
        		StartPressure (threadArgsPtr);
        	break;

			case 'D':
				StopPressure ();
			break;

			case 'E':
				StartAccGyro (threadArgsPtr);
			break;

			case 'F':
			case 'H':
				StopAccGyro() ;

				if ('H' == MenuSelection)
				{
                     if ( 0 == AccGyroDeviceId ) {
                          LSM6DS0_AccDataDisplay();
                     }
                     else {
                          LSM6DS3_AccDataDisplay();
                     } /* endif */
				}
			break;

			case 'G' :
			{
                StopAccGyro() ;

#if (defined APP_USE_NEW_CONSOLE)
				std::string inputStr;

				printf ( "Select device [0: LSM6DS0 - 1: LSM6DS3]: " ) ;
				std::getline (std::cin, inputStr );
				std::cin.ignore();

				AccGyroDeviceId = strtol(inputStr.c_str(),0,10);

                printf ( "Enter number of samples: " ) ;
                std::getline (std::cin, inputStr );
                std::cin.ignore();

                AccSamplesNum = strtol(inputStr.c_str(),0,10);

                printf ( "Enter acc. range: " ) ;
                std::getline (std::cin, inputStr );
                std::cin.ignore();

                AccRange = strtol(inputStr.c_str(),0,10);

                printf ( "Enter acc. output data rate: " ) ;
                std::getline (std::cin, inputStr );
                std::cin.ignore();

                OutputDataRate = strtol(inputStr.c_str(),0,10);

#else
				printf ( "Enter number of samples: " ) ;

				scanf ( "%d" , &AccSamplesNum ) ;
#endif // #if (defined APP_USE_NEW_CONSOLE)

                AccGyroSelect ( AccGyroDeviceId ) ;

                if ( 1 == AccGyroDeviceId ) {

                     // Set the number of samples for the accelerometer
//TODO                     LSM6DS3_SetAccSamplesNum ( AccSamplesNum ) ;
                     LSM6DS3_SetAccRange ( AccRange ) ;
                     LSM6DS3_SetOutputDataRate ( OutputDataRate ) ;
                }
                else {
                     // Set the number of samples for the accelerometer
                     LSM6DS0_SetAccSamplesNum ( AccSamplesNum ) ;
                     LSM6DS0_SetAccRange ( AccRange ) ;
//                     LSM6DS0_SetOutputDataRate ( OutputDataRate ) ;
                } /* endif */
			}
			break ;

			case 'I' :
			{
#if (defined APP_USE_NEW_CONSOLE)
				std::string inputStr;

				printf ( "Enter acc. range: " ) ;
				std::getline (std::cin, inputStr );
				std::cin.ignore();

				AccRange = strtol(inputStr.c_str(),0,10);
#else
				printf ( "Enter acc. range: " ) ;

				scanf ( "%d" , &AccRange ) ;
#endif // #if (defined APP_USE_NEW_CONSOLE)

				LSM6DS0_SetAccRange ( AccRange ) ;
			}
			break ;

			case 'X' :
				ExitFlag = true ;
			break ;
    	} /* endswitch */
    } /* endwhile */
#endif // #if (defined APP_USE_CONSOLE)

    // Shutdown threads
    CloseJobs ();

    // Signal application exit
    cout << "Exiting..." << endl;

    // Declare maybe unused variables
    C_UNUSED (threadArgsPtr);
    C_UNUSED (ret);

    return ERROR_CODE_NO_ERROR;
}

#if (defined APP_USE_PIPES)
static vector<string> StrSplitter (std::string text, std::string pattern)
{
	using boost::is_any_of;
	vector<string> strVec;

	boost::algorithm::split (strVec, text, is_any_of(pattern));

	return strVec;
}

static std::string StrFinder (vector<string> strVec, std::string pattern)
{
	std::string resStr;
	std::string searchPattern = pattern + "=";

	resStr.clear ();

	for (vector<string>::iterator it = strVec.begin(); it != strVec.end(); it++)
	{
		if (boost::iterator_range<std::string::iterator> r = find_first (*it, searchPattern))
		{
			boost::trim_if (*it, is_any_of(searchPattern));

			resStr = *it;

			break;;
		}
	}

	return resStr;
}

static tSInt GetUserSelection (tChar &userCmd, tSInt &accGyroFs, tSInt &accGyroOdr)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tU8 inputBuffer[IPC_FIFO_RX_BUFFER_SIZE];
	std::string text; // = "CMD=G FS=4 ODR=1600";
	vector<string> strVect;
	//vector<string> checkPatternVect;
	std::string valueStr;

	// Get data from pipe
	IPC_FifoGetBytes (&inputBuffer[0], IPC_FIFO_RX_BUFFER_SIZE);

	// Convert to std::string
	text = std::string(reinterpret_cast<const char*>(inputBuffer));

	// Split string using spaces
	strVect = StrSplitter (text, "-\n");

	// Find commands
	valueStr = StrFinder (strVect, "CMD");
	userCmd = valueStr.at (0);

	valueStr = StrFinder (strVect, "FS");
	try
	{
		accGyroFs = boost::lexical_cast<int>(valueStr);
	}
	catch (...)
	{
		accGyroFs = 0;
	}

	valueStr = StrFinder (strVect, "ODR");
	try
	{
		accGyroOdr = boost::lexical_cast<int>(valueStr);
	}
	catch (...)
	{
		accGyroOdr = 0;
	}

	return res;
}
#endif // #if (defined APP_USE_PIPES)

#if (defined SENSOR_USE_HTS221)
static MAYBE_UNUSED tSInt StartHumidityTemperature (void *threadArgsPtr)
{
	if (0 == HTS221_thread_ret)
	{
		printf ("HTS221 thread already running\n");

		return ERROR_CODE_GENERIC;
	}

	// Create the thread for the HTS221 sensor
	HTS221_thread_ret = pthread_create (&hts221Tid, NULL, &HTS221_Thread, threadArgsPtr);

	if (0 != HTS221_thread_ret)
	{
		printf ("Can't create HTS221 thread :[%s]\n", strerror (HTS221_thread_ret));

		return ERROR_CODE_GENERIC;
	}

	printf ("HTS221 thread created successfully\n");

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt StopHumidityTemperature (void)
{
	if (0 == HTS221_thread_ret)
	{
		HTS221_ExitThread (true);

		while (0 == HTS221_SignalHandler ()) { }

		HTS221_thread_ret = -1;
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined SENSOR_USE_HTS221)

#if (defined SENSOR_USE_LPS25H)
static MAYBE_UNUSED tSInt StartPressure (void *threadArgsPtr)
{
	if ( 0 == LPS25H_thread_ret )
	{
		printf ("LPS25H thread already running\n");

		return ERROR_CODE_GENERIC;
	}

	// Create the thread for LPS25H sensor
	LPS25H_thread_ret = pthread_create (&lps25hTid, NULL, &LPS25H_Thread, threadArgsPtr);

	if (0 != LPS25H_thread_ret)
	{
		printf ("Can't create LPS25H thread :[%s]\n", strerror (LPS25H_thread_ret));

		return ERROR_CODE_GENERIC;
	}

	printf ("LPS25H thread created successfully\n");

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt StopPressure (void)
{
	if (0 == LPS25H_thread_ret)
	{
		LPS25H_ExitThread (true);

		while (0 == LPS25H_SignalHandler ()) { }

		LPS25H_thread_ret = -1 ;
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined SENSOR_USE_LPS25H)

#if (defined SENSOR_USE_LSM6DS0)
static MAYBE_UNUSED tSInt StartAccGyro (void *threadArgsPtr)
{
	pthread_attr_t tAttr;
	tSInt newPrio;
	sched_param param;

	if (0 == AccGyroThread_ret)
	{
		printf ("LSM6DS0 thread already running\n");

		return ERROR_CODE_GENERIC;
	}

	// Set thread attribute to be real-time

	// Init attributes
	AccGyroThread_ret = pthread_attr_init (&tAttr);
	if (0 != AccGyroThread_ret)
	{
		printf ("Can't 'pthread_attr_init' for Acc/Gyro thread :[%s]\n", strerror (AccGyroThread_ret));
	}

	// use the current scheduling policy
	AccGyroThread_ret = pthread_attr_setinheritsched (&tAttr, PTHREAD_EXPLICIT_SCHED);
	if (0 != AccGyroThread_ret)
	{
		printf ("Can't 'pthread_attr_setinheritsched' for Acc/Gyro thread :[%s]\n", strerror (AccGyroThread_ret));
	}

	// set the new scheduling priority SCHED_FIFO
	AccGyroThread_ret = pthread_attr_setschedpolicy (&tAttr, SCHED_FIFO);
	if (0 != AccGyroThread_ret)
	{
		printf ("Can't 'pthread_attr_setschedpolicy' for Acc/Gyro thread :[%s]\n", strerror (AccGyroThread_ret));
	}

	// set the priority
	newPrio = 1;
	param.sched_priority = newPrio;

	AccGyroThread_ret = pthread_attr_setschedparam (&tAttr, &param);
	if (0 != AccGyroThread_ret)
	{
		printf ("Can't 'pthread_attr_setschedparam' for Acc/Gyro thread :[%s]\n", strerror (AccGyroThread_ret));
	}

	// Create the thread for the Acc/Gyro sensor
	AccGyroThread_ret = pthread_create (&AccGyroId, &tAttr, &AccGyro_Thread, threadArgsPtr);
	if (0 != AccGyroThread_ret)
	{
		printf ("Can't create Acc/Gyro thread :[%s]\n", strerror (AccGyroThread_ret));

		return ERROR_CODE_GENERIC;
	}

	printf ("Acc/Gyro thread created successfully\n");

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt StopAccGyro (void)
{
	if (0 == AccGyroThread_ret)
	{
		AccGyro_ExitThread (true);

		while (0 == AccGyro_SignalHandler ()) { }

		AccGyroThread_ret = -1 ;
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined SENSOR_USE_LSM6DS0)

#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
static MAYBE_UNUSED tSInt StartApplicationUsingGpio (void *threadArgsPtr, tBool serviceThread)
{
	pthread_attr_t tAttr;
	tSInt newPrio;
	sched_param param;

	if (0 == GpioThread_ret)
	{
		printf ("GPIO thread already running\n");

		return ERROR_CODE_GENERIC;
	}

	// Set thread attribute to be real-time

	// Init attributes
	GpioThread_ret = pthread_attr_init (&tAttr);
	if (0 != GpioThread_ret)
	{
		printf ("Can't 'pthread_attr_init' for GPIO thread :[%s]\n", strerror (GpioThread_ret));
	}

	// use the current scheduling policy
	GpioThread_ret = pthread_attr_setinheritsched (&tAttr, PTHREAD_EXPLICIT_SCHED);
	if (0 != GpioThread_ret)
	{
		printf ("Can't 'pthread_attr_setinheritsched' for GPIO thread :[%s]\n", strerror (GpioThread_ret));
	}

	// set the new scheduling priority SCHED_FIFO
	GpioThread_ret = pthread_attr_setschedpolicy (&tAttr, SCHED_FIFO);
	if (0 != GpioThread_ret)
	{
		printf ("Can't 'pthread_attr_setschedpolicy' for GPIO thread :[%s]\n", strerror (GpioThread_ret));
	}

	// set the priority
	newPrio = 1;
	param.sched_priority = newPrio;

	GpioThread_ret = pthread_attr_setschedparam (&tAttr, &param);
	if (0 != GpioThread_ret)
	{
		printf ("Can't 'pthread_attr_setschedparam' for GPIO thread :[%s]\n", strerror (GpioThread_ret));
	}

	// Create the thread for the GPIO
	if (false == serviceThread)
	{
#if (defined SWITCH_APP_ACTIVE)
		GpioThread_ret = pthread_create (&gpioTid, &tAttr, &GPIO_Thread, threadArgsPtr);
#elif (defined MOTOR_APP_ACTIVE)
		GpioThread_ret = pthread_create (&gpioTid, &tAttr, &MOTOR_Thread, threadArgsPtr);
#endif // #if (defined SWITCH_APP_ACTIVE)
	}
	else
	{
#if (defined SWITCH_APP_ACTIVE)
		GpioThread_ret = pthread_create (&gpioTid, &tAttr, &GPIO_ServiceThread, threadArgsPtr);
#elif (defined MOTOR_APP_ACTIVE)
		GpioThread_ret = pthread_create (&gpioTid, &tAttr, &MOTOR_ServiceThread, threadArgsPtr);
#endif // #if (defined SWITCH_APP_ACTIVE)
	}

	if (0 != GpioThread_ret)
	{
		printf ("Can't create GPIO thread :[%s]\n", strerror (GpioThread_ret));

		return ERROR_CODE_GENERIC;
	}

	printf ("GPIO thread created successfully\n");

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt StopApplicationUsingGpio (void)
{
	if (0 == GpioThread_ret)
	{
#if (defined SWITCH_APP_ACTIVE)
		GPIO_ExitThread (true);

		while (0 == GPIO_SignalHandler ()) { }
#elif (defined MOTOR_APP_ACTIVE)
		MOTOR_ExitThread (true);

		while (0 == MOTOR_SignalHandler ()) { }
#endif // #if (defined SWITCH_APP_ACTIVE)

		GpioThread_ret = -1 ;
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

static void CloseJobs (void)
{
#if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)
	// Close the thread depending on the current active application
#if (defined SWITCH_APP_ACTIVE)
    GPIO_ExitThread (true);

    while (0 == GPIO_SignalHandler ()) { }
#elif (defined MOTOR_APP_ACTIVE)
    MOTOR_ExitThread (true);

    while (0 == MOTOR_SignalHandler ()) { }
#endif // #if (defined SWITCH_APP_ACTIVE)
#endif // #if (defined GPIO_USE_IO_DEV) || (defined GPIO_USE_MEM)

#if (defined GPIO_USE_LEDS)
    LED_ExitThread (true);

    while (0 == LED_SignalHandler ()) { }
#endif // #if (defined GPIO_USE_LEDS)

#if (defined USE_SPI_DEV)
    SPI_ExitThread (true);

    while (0 == SPI_SignalHandler ()) { }
#endif // #if (defined USE_SPI_DEV)

#if (defined USE_I2C_DEV)
    I2C_ExitThread (true);

    while (0 == I2C_SignalHandler ()) { }
#endif // #if (defined USE_I2C_DEV)

    // Check if all thread exited
#if (defined SENSOR_USE_HTS221)
    if ( 0 == HTS221_thread_ret )
    {
         HTS221_ExitThread (true);
         while (0 == HTS221_SignalHandler ()) { }
    } /* endif */
#endif // #if (defined SENSOR_USE_HTS221)

#if (defined SENSOR_USE_LPS25H)
    if ( 0 == LPS25H_thread_ret )
    {
         LPS25H_ExitThread (true);

         while (0 == LPS25H_SignalHandler ()) { }
    } /* endif */
#endif // #if (defined SENSOR_USE_LPS25H)

#if (defined SENSOR_USE_LSM6DS0)
    if ( 0 == AccGyroThread_ret )
    {
    	 AccGyro_ExitThread (true);

         while (0 == AccGyro_SignalHandler ()) { }
    } /* endif */
#endif // #if (defined SENSOR_USE_LSM6DS0)
}

#if (defined I2C_TEST_CODE)

static int GetUserInput (char * returnStr, int maxStringLength)
{
	char *tempStr;
	int totalCount = 0, overflow = 0;
	size_t  len;

	tempStr = (char *)malloc((maxStringLength + 2) * sizeof (char));  	// Temporary holder

	do
	{
		fgets (tempStr, maxStringLength +1 , stdin);  			// Get chars from input buffer

		len = strlen (tempStr);

		if (tempStr[len-1] == '\n') 							// 'newline' indicates end of string
		{
			tempStr[len-1] = '\0';   							// Delete it...
			len = strlen(tempStr);   							// ...and recalculate length
		}

		if (!overflow++)
		{
			strcpy(returnStr,tempStr);  						// Copy first string into output
		}

		totalCount += (int)len;
	}

	while ((int)len == maxStringLength);  						// Continue to flush extras if too long...
	free(tempStr);              								// ...and release memory

	return totalCount;   										// May be more than the number allowed
}

static void I2C_TestMode ( void )
{
  int initRes;
  int wrLen;
  // int rdLen;
  char wrBfr [10] = {0x00};
  char rdBfr [10] = {0x00};
  unsigned char readRes, writeRes;
  short int X_Val;
  short int Y_Val;
  short int Z_Val;
  char  MenuSelection ;
  int   RegReadAdd ;
  //int   RegReadBytesNum ;
  int   ByteToWrite ;
  int   RegWriteAdd ;
  struct timeval StartTaskTime ;
  struct timeval CurrTaskTime ;
  //long int Time_us ;
  float Msec ;
  gettimeofday ( &StartTaskTime , NULL ) ;

  while ( 1 ) {
          MenuSelection = GetMenuSelection() ;

          switch ( MenuSelection ) {
          	  	   case 'A' :
             	  		initRes = bcm2835_init() ; // Initialize I2C
                        if ( 0 == initRes ) {
                        	printf ( "Failed to initialize I2C\n" ) ;
                        }
                        else {

                    		bcm2835_i2c_begin() ; // Start I2C mode

                    		// Setup I2C
                    		bcm2835_i2c_setSlaveAddress (LSM6DS0_I2C_ADD);
                    		bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_626);

                    		readRes = I2C_GetData ( 0x0F , 1 , rdBfr ) ;
                    		printf ( "Who Am I reg read result = %d ID : %x\n", readRes, rdBfr[0]);

                        } /* endif */
                   break ;

          	  	   case 'R' :
                		printf ( "\nEnter read reg add [hex]: " ) ;
                 		scanf ( "%X" , &RegReadAdd ) ;
                		readRes = I2C_GetData ( RegReadAdd , 1 , rdBfr ) ;
                		printf ( "Reg read result = %d ID : %x\n", readRes, rdBfr[0]);
                   break ;
          	  	   case 'W' :
                 		printf ( "\nEnter write reg add [hex]: " ) ;
                		scanf ( "%X" , &RegWriteAdd ) ;
                		printf ( "\nEnter write val [hex]: " ) ;
                 		scanf ( "%X" , &ByteToWrite ) ;
                 		wrBfr [ 0 ] = ByteToWrite ;
                 		I2C_WriteData ( RegWriteAdd , 1 , wrBfr ) ;
                   break ;
          	  	   case 'S' :
                        AcqAccelData() ;
                   break ;
          	  	   case 'Q' :
                        gettimeofday ( &CurrTaskTime , NULL ) ;
                        Msec = (CurrTaskTime.tv_sec-StartTaskTime.tv_sec)*1000 +
                        	   (CurrTaskTime.tv_usec-StartTaskTime.tv_usec)/(float)1000;
                        printf("%0.2f ms.\n",Msec); //41-44
                   break ;
          	  	   case 'X' :
                        bcm2835_i2c_end() ; // Close I2C mode
                        return ;
                   break ;
          } /* endswitch */

  } /* endwhile */
	
	initRes = bcm2835_init() ; // Initialize I2C

	if (0 == initRes)
    {
		printf ("Failed to initialize I2C\n");
    }
	else
	{
		// Start I2C mode
		bcm2835_i2c_begin ();

		// Setup I2C
		bcm2835_i2c_setSlaveAddress (LSM6DS0_I2C_ADD);
		bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_626);


		readRes = I2C_GetData ( 0x0F , 1 , rdBfr ) ;
		printf("Read result = %d, data: %x\n", readRes, rdBfr[0]);


		wrLen = 2;
		wrBfr[0] = 0x20;
		wrBfr[1] = 0xC1;
		writeRes = bcm2835_i2c_write (wrBfr, wrLen);

		wrLen = 2;
		wrBfr[0] = 0x21;
		wrBfr[1] = 0x08;
		writeRes = bcm2835_i2c_write (wrBfr, wrLen);

		wrLen = 2;
		wrBfr[0] = 0x1F;
		wrBfr[1] = 0x38;
		writeRes = bcm2835_i2c_write (wrBfr, wrLen);

        while ( 1 ) {

      		    readRes = I2C_GetData ( 0x27 , 1 , rdBfr ) ; // Read status register

      		    if ( 0 !=  ( rdBfr[0] & 1 ) ) {

      		    	/* Read data */

      		    	readRes = I2C_GetData ( 0x28 , 6 , rdBfr ) ; // Read status register
      		    	X_Val = ( ( (int)rdBfr[4] ) << 8 ) + (int)rdBfr[5] ;
      		    	Y_Val = ( ( (int)rdBfr[2] ) << 8 ) + (int)rdBfr[3] ;
      		    	Z_Val = ( ( (int)rdBfr[0] ) << 8 ) + (int)rdBfr[1] ;

      		    	printf ( "\nX: %d - Y: %d - Z: %d" ,
      		    			X_Val , Y_Val , Z_Val ) ;
#if 0
      		    	printf ( "\nx: %.2X %.2X - y: %.2X %.2X - z: %.2X %.2X" ,
      		   			 rdBfr[0] , rdBfr[1] , rdBfr[2] , rdBfr[3] , rdBfr[4] , rdBfr[5] );
#endif
      		    } /* endif */

        } /* endwhile */

		readRes = I2C_GetData ( 0x1E , 1 , rdBfr ) ;
		printf("Read result = %d, data: %x\n", readRes, rdBfr[0]);

		wrLen = 2;
		wrBfr[0] = 0x1E;
		wrBfr[1] = 0x55;
		writeRes = bcm2835_i2c_write (wrBfr, wrLen);

		readRes = I2C_GetData ( 0x1E , 1 , rdBfr ) ;
		printf("Read result = %d, data: %x\n", readRes, rdBfr[0]);

#if 0
		// Read data and check result:
		// - BCM2835_I2C_REASON_OK
		// - BCM2835_I2C_REASON_ERROR_NACK
		// - BCM2835_I2C_REASON_ERROR_CLKT
		// - BCM2835_I2C_REASON_ERROR_DATA
		wrLen = 1;
		wrBfr[0] = 0x0F;
		writeRes = bcm2835_i2c_write (wrBfr, wrLen);

		if (BCM2835_I2C_REASON_OK != writeRes)
		{
			printf("Write result KO = %d\n", writeRes);
		}
		else
		{
			printf("Write result OK = %d\n", writeRes);

			rdLen = 1;
			rdBfr[0] = 0x00;
			readRes = bcm2835_i2c_read (rdBfr, rdLen);

			printf("Read result = %d, data: %.2X\n", readRes, rdBfr[0]);
		}
#endif
		
		bcm2835_i2c_end() ; // Close I2C mode
	}

	(void)writeRes;
}


/************************************************************************************/
/*                                                                                  */
/* static void AcqAccelData ( void )                                                */
/*                                                                                  */
/************************************************************************************/

static void AcqAccelData ( void )
{
  char ByteToWrite ;
  char rdBfr [ 10 ] ;
  struct timeval CurrTaskTime ;
  float  AccMin = -2. ;
  float  AccMax =  2. ;
  int   BitNum = 0x10000 ;
  float  Lsb ;

  Lsb = ( AccMax - AccMin) / BitNum ;

  ByteToWrite = 0xC1 ;
  I2C_WriteData ( 0x20 , 1 , &ByteToWrite ) ;
  ByteToWrite = 0x08 ;
  I2C_WriteData ( 0x21 , 1 , &ByteToWrite ) ;
  ByteToWrite = 0x38 ;
  I2C_WriteData ( 0x1F , 1 , &ByteToWrite ) ;

  AccDataIdx_ = 0 ;

  while ( AccDataIdx_ < ACC_DATA_SIZE ) {

  		  I2C_GetData ( 0x27 , 1 , rdBfr ) ; // Read status register

  		    if ( 0 != ( rdBfr[0] & 1 ) ) {

  		    	gettimeofday ( &CurrTaskTime , NULL ) ;

  		    	/* Read data */

  		    	I2C_GetData ( 0x28 , 6 , rdBfr ) ; // Read status register

  		    	AccValList [ AccDataIdx_ ] . TimeTag_us = CurrTaskTime . tv_usec ;
  		    	AccValList [ AccDataIdx_ ] . X_Val = ( ( (int)rdBfr[4] ) << 8 ) + (int)rdBfr[5] ;
  		    	AccValList [ AccDataIdx_ ] . Y_Val = ( ( (int)rdBfr[2] ) << 8 ) + (int)rdBfr[3] ;
  		    	AccValList [ AccDataIdx_ ] . Z_Val = ( ( (int)rdBfr[0] ) << 8 ) + (int)rdBfr[1] ;
  		    	AccDataIdx_++ ;
  		    } /* endif */

  } /* endwhile */

  AccDataIdx_ = 1 ;

  while ( AccDataIdx_ < ACC_DATA_SIZE ) {
          printf ( "\nI: %.04d - T: %.04d - X: %05.5f - Y: %05.5f - Z: %05.5f" ,AccDataIdx_ ,
		      AccValList [ AccDataIdx_ ] . TimeTag_us - AccValList [ AccDataIdx_ - 1 ] . TimeTag_us,
			  1000. * Lsb * AccValList [ AccDataIdx_ ] . X_Val ,
			  1000. * Lsb * AccValList [ AccDataIdx_ ] . Y_Val ,
			  1000. * Lsb * AccValList [ AccDataIdx_ ] . Z_Val ) ;
          AccDataIdx_++ ;
  } /* endwhile */

} /* end of AcqAccelData() function */

#endif // #if (defined I2C_TEST_CODE)

#if (defined APP_USE_CONSOLE)
/************************************************************************************/
/*                                                                                  */
/* static char GetMenuSelection ( void )                                            */
/*                                                                                  */
/************************************************************************************/
static char GetMenuSelection ( void )
{
  char Choice ;
  char InputString [ 20 ] ;

  printf ( "\n\nRaspberry I2C test\n" ) ;
  printf ( "\nA - Init I2C                   B - Init peripheral" ) ;
  printf ( "\nR - Read Register              W - Write register" ) ;
  printf ( "\nX - Exit" ) ;

  printf ( "\n\nEnter choice: " ) ;

  fgets ( InputString , sizeof ( InputString ) , stdin ) ;
  sscanf ( InputString , "%c" , &Choice ) ;

  if ( Choice >= 'a' && Choice <= 'z' ) {
	  Choice = Choice - 'a' + 'A' ;
  } /* endif */

  return ( Choice ) ;

} /* end of GetMenuSelection() function */

static char GetMenuSelection (void)
{
#if (defined APP_USE_NEW_CONSOLE)
  char Choice ;
  std::string name;

  fflush(stdin);

  printf ( "\n\nRaspberry I2C MEMS test - 2015 May 16th\n" ) ;
  printf ( "\nA - Start HTS221 Task       B - Stop HTS221 Task" ) ;
  printf ( "\nC - Start LPS25H Task       D - Stop LPS25H Task" ) ;
  printf ( "\nE - Start Acc/Gyro Task     F - Stop Acc/Gyro Task" ) ;
  printf ( "\nG - Acc/Gyro config         H - Acc/Gyro data display" ) ;

  printf ( "\nX - Exit" ) ;

  printf ( "\n\nEnter choice: " ) ;

  //Choice = GetCharFromConsole ();
  //GetCharFromConsole ();

  std::getline (std::cin, name);
  std::cin.ignore();
  Choice = name.at(0);

  if ( Choice >= 'a' && Choice <= 'z' )
  {
	   Choice = Choice - 'a' + 'A' ;
  } /* endif */

  return ( Choice ) ;
#else
  char Choice ;
  char InputString [ 20 ] ;

  printf ( "\n\nRaspberry I2C MEMS test - 2015 May 16th\n" ) ;
  printf ( "\nA - Start HTS221 Task       B - Stop HTS221 Task" ) ;
  printf ( "\nC - Start LPS25H Task       D - Stop LPS25H Task" ) ;
  printf ( "\nE - Start Acc/Gyro Task     F - Stop Acc/Gyro Task" ) ;
  printf ( "\nG - Acc/Gyro config         H - Acc/Gyro data display" ) ;

  printf ( "\nX - Exit" ) ;

  printf ( "\n\nEnter choice: " ) ;

  fgets ( InputString , sizeof ( InputString ) , stdin ) ;
  sscanf ( InputString , "%c" , &Choice ) ;

if ( Choice >= 'a' && Choice <= 'z' )
  {
	   Choice = Choice - 'a' + 'A' ;
  } /* endif */

  return ( Choice ) ;
#endif // #if (defined APP_USE_NEW_CONSOLE)
} /* end of GetMenuSelection() function */

static char GetCharFromConsole (void)
{
	char buf = 0;
	struct termios old = {0};

	if (tcgetattr(0, &old) < 0)
	{
		perror("tcsetattr()");
	}

	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;

	if (tcsetattr(0, TCSANOW, &old) < 0)
	{
		perror("tcsetattr ICANON");
	}

	if (read(0, &buf, 1) < 0)
	{
		perror ("read()");
	}

	old.c_lflag |= ICANON;
	old.c_lflag |= ECHO;

	if (tcsetattr(0, TCSADRAIN, &old) < 0)
	{
		perror ("tcsetattr ~ICANON");
	}

	return (buf);
}

static void SetConsole (void)
{
	struct termios old = {0};

	if (tcgetattr(0, &old) < 0)
	{
		perror("tcsetattr()");
	}

	old.c_lflag &= ~ICANON;
	old.c_lflag &= ~ECHO;
	old.c_cc[VMIN] = 1;
	old.c_cc[VTIME] = 0;

	if (tcsetattr(0, TCSANOW, &old) < 0)
	{
		perror("tcsetattr ICANON");
	}
}
#endif // #if (defined APP_USE_CONSOLE)

#if (defined APP_REDIRECT_STDOUT)
static tSInt RedirectOutput (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	FILE *fp;

	// Write stdout to a file
	fp = freopen ("/webroot/logs/console.txt", "w", stdout);

	if (NULL == fp)
	{
		res = ERROR_CODE_SYSCALL_FAILED;
	}
	else
	{
		// Now redirect stderr to the same file
		dup2 (fileno (stdout), fileno (stderr));

		// Now disable buffering on the file
		setbuf (fp, NULL);
	}

	return res;
}
#endif // #if (defined APP_REDIRECT_STDOUT)

// End of file
