/*
 * i2c_lld.h
 *
 *  Created on: Mar 22, 2015
 *      Author: alberto
 */

#ifndef I2C_LLD_H_
#define I2C_LLD_H_

extern char I2C_GetData (char RegAddress , char BytesNum , char *RegDataPnt);

extern char I2C_GetDataWithAutoIncrement (char RegAddress, char BytesNum, char *RegDataPnt);

extern char I2C_WriteData (char RegAddress , char BytesNum , char *RegDataPnt);

#endif // I2C_LLD_H_

// End of file

