/*
 * plot_data.h
 *
 *  Created on: May 24, 2015
 *      Author: alberto
 */

#ifndef PLOT_DATA_H_
#define PLOT_DATA_H_

extern void PLOT_Init (void);

extern void PLOT_setScale (tSInt scale);

extern void PLOT_Draw (void);

extern void PLOT_Draw (dataVectTy &pts);

#endif /* PLOT_DATA_H_ */
