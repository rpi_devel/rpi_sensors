/*
 * macros.h
 *
 *  Created on: Mar 21, 2015
 *      Author: alberto
 */

#ifndef MACROS_H_
#define MACROS_H_

#define cat(x,y) x ## y
#define xcat(x,y) cat(x,y)

// Peripherals register defines
// It reserves a memory space of 4 bytes (32 bits). It's used in the peripherals memory map definition
#define gap32(x) tU32 xcat(UNUSED,__LINE__)[x]

// It reserves a memory space of 2 bytes (16 bits). It's used in the peripherals memory map definition */
#define gap16(x) tU16 xcat(UNUSED,__LINE__)[x]

// It reserves a memory space of 2 bytes (16 bits). It's used in the peripherals memory map definition */
#define gap(x) tU16 xcat(UNUSED,__LINE__)[x]

// It reserves a memory space of 1 byte (8 bits). It's used in the peripherals memory map definition */
#define gap8(x) tU8 xcat(UNUSED,__LINE__)[x]

#endif /* MACROS_H_ */
