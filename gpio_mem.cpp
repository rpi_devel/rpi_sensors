#include <unistd.h>
#include <fcntl.h>

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "gpio_common.h"
#include "gpio_mem.h"

#define BCM2708_PERI_BASE       0x20000000
#define GPIO_BASE               (BCM2708_PERI_BASE + 0x200000)	// GPIO controller

#define BLOCK_SIZE 				(4*1024)

// IO Access
typedef struct
{
    unsigned long addr_p;
    int mem_fd;
    void *map;
    volatile unsigned int *addr;
} bcm2835_peripheral_Ty;

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x)
#define INP_GPIO(g, n)   		*(g + ((n) / 10)) &= ~(7 << (((n) % 10) * 3))
#define OUT_GPIO(g, n)   		*(g + ((n) / 10)) |=  (1 << (((n) % 10) * 3))
#define SET_GPIO_ALT(g, n, a) 	*(g + (((n) / 10))) |= (((a) <= 3 ? (a) + 4 : (a) == 4 ? 3:2) << (((n) % 10) * 3))

#define GPIO_SET(g)  			*(g + 7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR(g)  			*(g + 10) // clears bits which are 1 ignores bits which are 0

#define GPIO_READ(g, n) 		*(g + 13) &= (1 << (n))

static bcm2835_peripheral_Ty GPIOMEM_gpio = {GPIO_BASE, 0, NULL, NULL};

static bcm2835_peripheral_Ty *GPIOMEM_gpioPtr = NULL;

// Exposes the physical address defined in the passed structure using mmap on /dev/mem
int gpio_mem_open (void)
{
	// Set pointer to struct
	GPIOMEM_gpioPtr = &GPIOMEM_gpio;

	// Init globals
	GPIOMEM_gpio.addr_p = GPIO_BASE;
	GPIOMEM_gpio.mem_fd = 0;
	GPIOMEM_gpio.map = NULL;
	GPIOMEM_gpio.addr = NULL;

	// Open /dev/mem
	if ((GPIOMEM_gpioPtr->mem_fd = open ("/dev/mem", O_RDWR | O_SYNC)) < 0)
	{
		return -1;
	}

	GPIOMEM_gpioPtr->map = mmap (NULL,
				   	   	   	     BLOCK_SIZE,
								 PROT_READ | PROT_WRITE,
								 MAP_SHARED,
								 GPIOMEM_gpioPtr->mem_fd,  // File descriptor to physical memory virtual file '/dev/mem'
								 GPIOMEM_gpioPtr->addr_p); // Address in physical map that we want this memory block to expose

	if (MAP_FAILED == GPIOMEM_gpioPtr->map)
	{
		return -1;
	}

	GPIOMEM_gpioPtr->addr = (volatile unsigned int *)GPIOMEM_gpioPtr->map;

	return 0;
}

int gpio_mem_set_direction (int gpioNum, GPIODEV_directionEnumTy direction)
{
	if (GPIODEV_DIRECTION_OUT == direction)
	{
		// Define pin 7 (gpioNum = 4) as output
		INP_GPIO (GPIOMEM_gpioPtr->addr, gpioNum);
		OUT_GPIO (GPIOMEM_gpioPtr->addr, gpioNum);
	}
	else
	{
		// Define GPIO as input
	}

	return 0;
}

GPIODEV_valEnumTy gpio_mem_get_value (int gpioNum)
{
	int res;
	GPIODEV_valEnumTy retVal;

	res = GPIO_READ (GPIOMEM_gpioPtr->addr, gpioNum);

	if (0 == res)
	{
		retVal = GPIODEV_VALUE_LOW;
	}
	else
	{
		retVal = GPIODEV_VALUE_HIGH;
	}

	return retVal;
}

int gpio_mem_set_value (int gpioNum, GPIODEV_valEnumTy value)
{
	if (GPIODEV_VALUE_HIGH == value)
	{
		GPIO_SET (GPIOMEM_gpioPtr->addr) = 1 << gpioNum;
	}
	else
	{
		GPIO_CLR (GPIOMEM_gpioPtr->addr) = 1 << gpioNum;
	}

	return 0;
}

int gpio_mem_close (int gpioNum)
{
    munmap (GPIOMEM_gpioPtr->map, BLOCK_SIZE);

    close (GPIOMEM_gpioPtr->mem_fd);

    // Set globals to uninit values
    GPIOMEM_gpioPtr = NULL;

	GPIOMEM_gpio.addr_p = GPIO_BASE;
	GPIOMEM_gpio.mem_fd = 0;
	GPIOMEM_gpio.map = NULL;
	GPIOMEM_gpio.addr = NULL;

	return 0;
}

// End of file
