/*
 * gpio_thread.cpp
 *
 *  Created on: Dec 2, 2014
 *      Author: alberto
 */

#include <iostream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <time.h>
#include <sys/time.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "gpio_common.h"
#include "gpio_mem.h"
#include "gpio_dev.h"

#include "gpio_thread.h"

using namespace std;

#define ST_LOG_FILE_APP_SWITCH_STATUS		"/webroot/logs/switch_status.txt"

#define GPIO_MAX_NUMBER_TO_OPEN				10
#define GPIO_CMD_QUEUE_LEN					10

typedef struct
{
	tU8 gpioNum;
	tBool opened;
	GPIODEV_directionEnumTy dir;
	GPIODEV_valEnumTy value;
} GPIO_tableTy;

typedef struct
{
	GPIO_CommandEnumTy cmdToSend;

	tSInt internalCmdGpioNum;
	GPIODEV_valEnumTy internalCmdToSend;
} GPIO_cmdTy;


static GPIO_tableTy GPIO_table[GPIO_MAX_NUMBER_TO_OPEN];

//static GPIO_CommandEnumTy GPIO_cmdToSend = GPIO_CMD_NONE;
//static GPIODEV_valEnumTy GPIO_internalCmdToSend;
//static tSInt GPIO_internalCmdGpioNum;
static GPIO_cmdTy GPIO_cmdQueue[GPIO_CMD_QUEUE_LEN];
static tSInt GPIO_cmdQueueIndex = 0;

static pthread_t id;

static bool GPIO_exitThread = false;

static ofstream ST_reportGpioStatus;

volatile sig_atomic_t GPIO_ended = 0;

static tSInt GPIO_InitLogFile (void);
static tSInt GPIO_UpdateLogFile (void);

int GPIO_SignalHandler (void)
{
	return GPIO_ended;
}

void GPIO_ExitThread (bool exitThread)
{
	if (true == exitThread)
	{
		GPIO_exitThread = true;
	}
}

void* GPIO_Thread (void *arg)
{
#if (defined GPIO_USE_IO_DEV)
	char gpio[] = {GPIO_23, GPIO_24, GPIO_25}; // {GPIO_06, GPIO_13};
	int cnt;
	int gpioNum = sizeof (gpio);
#endif // #if (defined GPIO_USE_IO_DEV)

#if (defined GPIO_USE_MEM)
	int cnt;
	char gpio[] = {GPIO_23, GPIO_24, GPIO_25, GPIO_17, GPIO_22, GPIO_27};
	bool gpioOpened[] = {false, false, false};
	int gpioNum = sizeof (gpio);
#endif // #if (defined GPIO_USE_MEM)

	// Init globals
	GPIO_ended = 0;
	GPIO_exitThread = false;

	// Get thread ID to return back at the end
	id = pthread_self ();

///
// USe peripherals GPIOs
//
#if (defined GPIO_USE_IO_DEV)
    // Call GPIO open function
    for (cnt = 0; cnt < gpioNum; cnt++)
    {
		if (0 != gpio_dev_open (gpio[cnt]))
		{
			printf ("Failed to open GPIO%d\n", gpio[cnt]);
		}

		if (0 != gpio_dev_set_direction (gpio[cnt], GPIODEV_DIRECTION_OUT))
		{
			printf ("Failed to set GPIO%d as %d\n", gpio[cnt], (int)GPIODEV_DIRECTION_OUT);
		}
    }

    while (false == GPIO_exitThread)
    {
    	// Set GPIOs to HIGH
    	for (cnt = 0; cnt < gpioNum; cnt++)
    	{
    		gpio_dev_set_value (gpio[cnt], GPIODEV_VALUE_HIGH);
    	}

    	// Check if we set value correctly
    	for (cnt = 0; cnt < gpioNum; cnt++)
    	{
    		if (GPIODEV_VALUE_HIGH != gpio_dev_get_value (gpio[cnt]))
    		{
    			printf ("Failed to set GPIO%d to %d\n", gpio[cnt], (int)GPIODEV_VALUE_HIGH);
    		}
    	}

    	// Wait 1ms
    	usleep (1);

    	// Set GPIOs to LOW
    	for (cnt = 0; cnt < gpioNum; cnt++)
    	{
    		gpio_dev_set_value (gpio[cnt], GPIODEV_VALUE_LOW);
    	}

    	// Check if we set value correctly
    	for (cnt = 0; cnt < gpioNum; cnt++)
    	{
    		if (GPIODEV_VALUE_LOW != gpio_dev_get_value (gpio[cnt]))
    		{
    			printf ("Failed to set GPIO%d to %d\n", gpio[cnt], (int)GPIODEV_VALUE_LOW);
    		}
    	}
    }

    // Call GPIO close function
    for (cnt = 0; cnt < gpioNum; cnt++)
    {
		if (0 != gpio_dev_close (gpio[cnt]))
		{
			printf ("Failed to close GPIO%d\n", gpio[cnt]);
		}
    }
#endif // #if (defined GPIO_USE_IO_DEV)

#if (defined GPIO_USE_MEM)
    // Open GPIOs
    if (0 != gpio_mem_open ())
	{
    	printf ("Failed to open GPIOs for direct memory access\n");
	}
    else
    {
    	// Set direction
    	for (cnt = 0; cnt < gpioNum; cnt++)
    	{
        	if (0 != gpio_mem_set_direction (gpio[cnt], GPIODEV_DIRECTION_OUT))
        	{
        		printf ("Failed to set GPIO%d to direction %d\n", gpio[cnt], (int)GPIODEV_DIRECTION_OUT);
        	}
        	else
        	{
        		gpioOpened[cnt] = true;
        	}
    	}

    	if (true == gpioOpened[0] && true == gpioOpened[1])
    	{
    		while (1)
    		{
    	    	for (cnt = 0; cnt < gpioNum; cnt++)
    	    	{
    	    		if (true == gpioOpened[cnt])
    	    		{
    	    			if (0 != gpio_mem_set_value (gpio[cnt], GPIODEV_VALUE_HIGH))
    	    			{
    	    				printf ("Failed to set GPIO%d to value %d\n", gpio[cnt], (int)GPIODEV_VALUE_HIGH);
    	    			}
    	    		}
    	    	}

    			// Suspend execution for microsecond intervals
    			usleep (3000);

    	    	for (cnt = 0; cnt < gpioNum; cnt++)
    	    	{
    	    		if (true == gpioOpened[cnt])
    	    		{
						if (0 != gpio_mem_set_value (gpio[cnt], GPIODEV_VALUE_LOW))
						{
							printf ("Failed to set GPIO%d to value %d\n", gpio[cnt], (int)GPIODEV_VALUE_LOW);
						}
        	    	}
    	    	}

    			// Suspend execution for microsecond intervals
    			usleep (3000);
    		}
    	}
    }

    // Set as output a couple of GPIOs
#endif // #if (defined GPIO_USE_MEM)

    // Signal thread end
    GPIO_ended = 1;

    return (void *)&id;
}

void* GPIO_ServiceThread (void *arg)
{
	#define WRITE_LOG_DELTA_TIME_SEC			((__time_t)30)

	struct timeval curTimeValue, startTimeValue;
	__time_t deltaTime;

	// Init globals
	GPIO_ended = 0;
	GPIO_exitThread = false;

	//GPIO_cmdToSend = GPIO_CMD_NONE;
	memset ((void *)&GPIO_cmdQueue[0], 0, sizeof (GPIO_cmdQueue));
	GPIO_cmdQueueIndex = -1; // Set to -1 meaning queue empty

	// Get thread ID to return back at the end
	id = pthread_self ();

	// Setup log file
	GPIO_InitLogFile ();

	// Init the GPIOs status to ensure their status can be read from external application
	GPIO_UpdateLogFile ();

	// Get current time to use as start time
    gettimeofday (&startTimeValue, NULL);

	// Thread main loop
    while (false == GPIO_exitThread)
    {
    	// Check if some command has been issued
    	if (GPIO_cmdQueueIndex >= 0)
    	{
			if (GPIO_CMD_NONE != GPIO_cmdQueue[GPIO_cmdQueueIndex].cmdToSend)
			{
				if (GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdToSend !=
					GPIO_SetValue (GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdGpioNum, GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdToSend))
				{
					printf ("Failed to set GPIO%d to value %d\n", GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdGpioNum,
							(tSInt)GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdToSend);
				}

				// Update the GPIOs status to ensure their status can be read from external application
				GPIO_UpdateLogFile ();

				// Acquire command execution
				GPIO_cmdQueue[GPIO_cmdQueueIndex].cmdToSend = GPIO_CMD_NONE;

				// Signal one command has been served
				GPIO_cmdQueueIndex--;
			}
    	}
    	else
    	{
			// Check if it is time to refresh the log files, we do every 30 seconds anyway
			gettimeofday (&curTimeValue, NULL);

			// Calculate delta time from last file write in seconds
			deltaTime = curTimeValue.tv_sec - startTimeValue.tv_sec;

			if (deltaTime >= WRITE_LOG_DELTA_TIME_SEC)
			{
				// Update the GPIOs status to ensure their status can be read from external application
				GPIO_UpdateLogFile ();

				// Get current time to use as start time
				gettimeofday (&startTimeValue, NULL);
			}
    	}

		// Suspend execution for microsecond intervals (0.5 sec)
		usleep (500000);
    }

    return (void *)&id;
}

tSInt GPIO_Command (tSInt gpioNum, GPIO_CommandEnumTy cmd)
{
	tSInt res = ERROR_CODE_GENERIC;

	// Validate the command
	if (GPIO_CMD_SET_VALUE_LOW != cmd && GPIO_CMD_SET_VALUE_HIGH != cmd)
	{
		return res;
	}

	if (GPIO_cmdQueueIndex < GPIO_CMD_QUEUE_LEN)
	{
		// Increment command queue index
		GPIO_cmdQueueIndex++;

		// Save the GPIO number
		GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdGpioNum = gpioNum;

		// Translate the command
		if (GPIO_CMD_SET_VALUE_LOW == cmd)
		{
			GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdToSend = GPIODEV_VALUE_LOW;
		}
		else if (GPIO_CMD_SET_VALUE_HIGH == cmd)
		{
			GPIO_cmdQueue[GPIO_cmdQueueIndex].internalCmdToSend = GPIODEV_VALUE_HIGH;
		}

		// Save the command received
		GPIO_cmdQueue[GPIO_cmdQueueIndex].cmdToSend = cmd;

		// Set return code
		res = ERROR_CODE_NO_ERROR;
	}

	// The command is not executed here, it has been planned
	return res;
}

tSInt GPIO_Init (tPU8 gpioListPtr, tSInt gpioNum)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tSInt cnt;

	// Init GPIO table
	for (cnt = 0; cnt < gpioNum; cnt++)
	{
		GPIO_table[cnt].gpioNum = *gpioListPtr;
		GPIO_table[cnt].opened = true;
		GPIO_table[cnt].dir = GPIODEV_DIRECTION_UNKNOWN;
		GPIO_table[cnt].value = GPIODEV_VALUE_UNKNOWN;

		gpioListPtr++;
	}

	for (cnt = gpioNum; cnt < GPIO_MAX_NUMBER_TO_OPEN; cnt++)
	{
		GPIO_table[cnt].gpioNum = GPIO_INVALID;
		GPIO_table[cnt].opened = false;
		GPIO_table[cnt].dir = GPIODEV_DIRECTION_UNKNOWN;
		GPIO_table[cnt].value = GPIODEV_VALUE_UNKNOWN;
	}

	// Open memory device
#if (defined GPIO_USE_MEM)
    if (0 != gpio_mem_open ())
	{
    	res = ERROR_CODE_GENERIC;
	}
#endif // #if (defined GPIO_USE_MEM)

#if (defined GPIO_USE_IO_DEV)
    for (cnt = 0; cnt < gpioNum; cnt++)
    {
		if (0 != gpio_dev_open (GPIO_table[cnt].gpioNum))
		{
			res = ERROR_CODE_GENERIC;
		}
    }
#endif // #if (defined GPIO_USE_IO_DEV)

	return res;
}

GPIODEV_directionEnumTy GPIO_SetDirection (tSInt gpioNum, GPIODEV_directionEnumTy direction)
{
	GPIODEV_directionEnumTy res = GPIODEV_DIRECTION_UNKNOWN;
	tSInt cnt;

	// Check if a valid direction value has been given
	if (GPIODEV_DIRECTION_OUT != direction && GPIODEV_DIRECTION_IN != direction)
	{
		// Return UNKNOWN direction as error
		return res;
	}

	// Set direction
	for (cnt = 0; cnt < GPIO_MAX_NUMBER_TO_OPEN; cnt++)
	{
		if (gpioNum == GPIO_table[cnt].gpioNum && true == GPIO_table[cnt].opened)
		{
#if (defined GPIO_USE_MEM)
			if (0 != gpio_mem_set_direction (GPIO_table[cnt].gpioNum, direction))
#elif (defined GPIO_USE_IO_DEV)
			if (0 != gpio_dev_set_direction (GPIO_table[cnt].gpioNum, direction))
#else
			if (true)
#endif // #if (defined GPIO_USE_MEM)
			{
				res = GPIODEV_DIRECTION_UNKNOWN;
			}
			else
			{
				// Store GPIO direction
				GPIO_table[cnt].dir = direction;

				// Return the direction value
				res = direction;
			}

			// Exit, we have found the GPIO
			break;
		}
	}

	return res;
}

GPIODEV_valEnumTy GPIO_SetValue (tSInt gpioNum, GPIODEV_valEnumTy value)
{
	GPIODEV_valEnumTy res = GPIODEV_VALUE_UNKNOWN;
	tSInt cnt;

	// Check if a valid value has been given
	if (GPIODEV_VALUE_LOW != value && GPIODEV_VALUE_HIGH != value)
	{
		// Return UNKNOWN value as error
		return res;
	}

	// Set value
	for (cnt = 0; cnt < GPIO_MAX_NUMBER_TO_OPEN; cnt++)
	{
		if (gpioNum == GPIO_table[cnt].gpioNum && true == GPIO_table[cnt].opened && GPIODEV_DIRECTION_OUT == GPIO_table[cnt].dir)
		{
#if (defined GPIO_USE_MEM)
			if (0 != gpio_mem_set_value (GPIO_table[cnt].gpioNum, value))
#elif (defined GPIO_USE_IO_DEV)
			if (0 != gpio_dev_set_value (GPIO_table[cnt].gpioNum, value))
#else
			if (true)
#endif // #if (defined GPIO_USE_MEM)
			{
				res = GPIODEV_VALUE_UNKNOWN;
			}
			else
			{
				// Store GPIO value
				GPIO_table[cnt].value = value;

				// Return the value
				res = value;
			}

			// Exit, we have found the GPIO
			break;
		}
	}

	return res;
}

GPIODEV_valEnumTy GPIO_GetValue (tSInt gpioNum)
{
	GPIODEV_valEnumTy res = GPIODEV_VALUE_UNKNOWN;
	tSInt cnt;

	// Get value
	for (cnt = 0; cnt < GPIO_MAX_NUMBER_TO_OPEN; cnt++)
	{
		if (gpioNum == GPIO_table[cnt].gpioNum && true == GPIO_table[cnt].opened)
		{
#if (defined GPIO_USE_MEM)
			res = gpio_mem_get_value (GPIO_table[cnt].gpioNum);
#elif (defined GPIO_USE_IO_DEV)
			res = gpio_dev_get_value (GPIO_table[cnt].gpioNum);
#endif // #if (defined GPIO_USE_MEM)

			// Exit, we have found the GPIO
			break;
		}
	}

	return res;
}

static tSInt GPIO_InitLogFile (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;

	// Open report for TEXT data last temperature and humidity read
	ST_reportGpioStatus.open (ST_LOG_FILE_APP_SWITCH_STATUS, ios::out | ios::trunc);

	if (false == ST_reportGpioStatus.is_open ())
	{
		res = ERROR_CODE_GENERIC;
	}

	// Close report file
	ST_reportGpioStatus.close ();

	return res;
}

static tSInt GPIO_UpdateLogFile (void)
{
	std::stringstream textStream;
	tSInt cnt;
	tSInt channelNum = 1;

	// Open LAST READ file for writing
	ST_reportGpioStatus.open (ST_LOG_FILE_APP_SWITCH_STATUS, ios::out | ios::trunc);

	if (false == ST_reportGpioStatus.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear string
	    textStream.str (std::string());
	    textStream.clear();

	    // Build string for all channels
	    for (cnt = 0; cnt < GPIO_MAX_NUMBER_TO_OPEN; cnt += 2)
	    {
	    	// Set channel number
	    	textStream << channelNum << " ";
	    	channelNum++;

	    	// Set associated GPIO
	    	textStream << (tSInt)GPIO_table[cnt].gpioNum <<"_" << (tSInt)GPIO_table[(cnt + 1)].gpioNum << " ";

	    	if ((true == GPIO_table[cnt].opened && GPIODEV_DIRECTION_OUT == GPIO_table[cnt].dir) &&
	    		(true == GPIO_table[(cnt + 1)].opened && GPIODEV_DIRECTION_OUT == GPIO_table[(cnt + 1)].dir))
	    	{
    			// Set status
	    		if ((GPIODEV_VALUE_HIGH == GPIO_table[cnt].value) || (GPIODEV_VALUE_HIGH == GPIO_table[(cnt + 1)].value))
	    		{
	    			textStream << "OFF" << endl;
	    		}
	    		else
	    		{
	    			textStream << "ON" << endl;
	    		}
	    	}
	    	else
	    	{
    			textStream << "DISABLED" << endl;
	    	}
	    }

	    // Save text stream on a file
		std::string tmpStr = textStream.str();
		ST_reportGpioStatus << tmpStr;

		// Close report file
		ST_reportGpioStatus.close ();
	}

	return ERROR_CODE_NO_ERROR;
}

// End of file



