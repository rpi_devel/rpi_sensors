/*
 * lis3mdl_lld.cpp
 *
 *  Created on: Apr 3, 2015
 *      Author: alberto
 */

#include <string>       // std::string
#include <iostream>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#include <bcm2835.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "system_defines.h"

#include "ipc.h"

#include "i2c_lld.h"

#include "lsm6ds3_lld.h"

using namespace std;

///
// Compilation defines
///
// Define to enable gyro
//#define LSM6DS3_GYRO_ENABLED
#undef LSM6DS3_GYRO_ENABLED

// Define to disable FIFO
//#define LSM6DS3_FIFO_DISABLED
#undef LSM6DS3_FIFO_DISABLED

// Define to directly write data to file
//#define WRITE_TO_FILE
#undef WRITE_TO_FILE

// Define to use shared memory as indirect write method (used if WRITE_TO_FILE is undefined).
// If this define is undefined MEMORY POOL will be used instead
//#define WRITE_DELAYED_USING_SHARED_MEMORIES
#undef WRITE_DELAYED_USING_SHARED_MEMORIES

// Define for TEST
//#define LSM6DS3_TEST_ENABLED
#undef LSM6DS3_TEST_ENABLED

// Defines
#define ACC_BUFFER_SIZE         							0x1000
#define GYRO_BUFFER_SIZE        							0x1000

#define LSM6DS3_I2C_ADD                     				0x6A

#define LSM6DS3_DEVICE_ID                   				((tU8)0x69)

#define LSM6DS3_REG_FUNC_CFG_ACCESS                         0x01
#define LSM6DS3_REG_SENSOR_SYNC_TIME_FRAME                  0x04
#define LSM6DS3_REG_FIFO_CTRL1                              0x06
#define LSM6DS3_REG_FIFO_CTRL2                              0x07
#define LSM6DS3_REG_FIFO_CTRL3                              0x08
#define LSM6DS3_REG_FIFO_CTRL4                              0x09
#define LSM6DS3_REG_FIFO_CTRL5                              0x0A
#define LSM6DS3_REG_ORIENT_CFG_G                            0x0B
#define LSM6DS3_REG_RESERVED                                0x0C
#define LSM6DS3_REG_INT1_CTRL                               0x0D
#define LSM6DS3_REG_INT2_CTRL                               0x0E
#define LSM6DS3_REG_WHO_AM_I                                0x0F
#define LSM6DS3_REG_CTRL1_XL                                0x10
#define LSM6DS3_REG_CTRL2_G                                 0x11
#define LSM6DS3_REG_CTRL3_C                                 0x12
#define LSM6DS3_REG_CTRL4_C                                 0x13
#define LSM6DS3_REG_CTRL5_C                                 0x14
#define LSM6DS3_REG_CTRL6_C                                 0x15
#define LSM6DS3_REG_CTRL7_G                                 0x16
#define LSM6DS3_REG_CTRL8_XL                                0x17
#define LSM6DS3_REG_CTRL9_XL                                0x18
#define LSM6DS3_REG_CTRL10_C                                0x19
#define LSM6DS3_REG_MASTER_CONFIG                           0x1A
#define LSM6DS3_REG_WAKE_UP_SRC                             0x1B
#define LSM6DS3_REG_TAP_SRC                                 0x1C
#define LSM6DS3_REG_D6D_SRC                                 0x1D
#define LSM6DS3_REG_STATUS_REG                              0x1E
#define LSM6DS3_REG_OUT_TEMP_L                              0x20
#define LSM6DS3_REG_OUT_TEMP_H                              0x21
#define LSM6DS3_REG_OUTX_L_G                                0x22
#define LSM6DS3_REG_OUTX_H_G                                0x23
#define LSM6DS3_REG_OUTY_L_G                                0x24
#define LSM6DS3_REG_OUTY_H_G                                0x25
#define LSM6DS3_REG_OUTZ_L_G                                0x26
#define LSM6DS3_REG_OUTZ_H_G                                0x27
#define LSM6DS3_REG_OUTX_L_XL                               0x28
#define LSM6DS3_REG_OUTX_H_XL                               0x29
#define LSM6DS3_REG_OUTY_L_XL                               0x2A
#define LSM6DS3_REG_OUTY_H_XL                               0x2B
#define LSM6DS3_REG_OUTZ_L_XL                               0x2C
#define LSM6DS3_REG_OUTZ_H_XL                               0x2D
#define LSM6DS3_REG_SENSORHUB1_REG                          0x2E
#define LSM6DS3_REG_SENSORHUB2_REG                          0x2F
#define LSM6DS3_REG_SENSORHUB3_REG                          0x30
#define LSM6DS3_REG_SENSORHUB4_REG                          0x31
#define LSM6DS3_REG_SENSORHUB5_REG                          0x32
#define LSM6DS3_REG_SENSORHUB6_REG                          0x33
#define LSM6DS3_REG_SENSORHUB7_REG                          0x34
#define LSM6DS3_REG_SENSORHUB8_REG                          0x35
#define LSM6DS3_REG_SENSORHUB9_REG                          0x36
#define LSM6DS3_REG_SENSORHUB10_REG                         0x37
#define LSM6DS3_REG_SENSORHUB11_REG                         0x38
#define LSM6DS3_REG_SENSORHUB12_REG                         0x39
#define LSM6DS3_REG_FIFO_STATUS1                            0x3A
#define LSM6DS3_REG_FIFO_STATUS2                            0x3B
#define LSM6DS3_REG_FIFO_STATUS3                            0x3C
#define LSM6DS3_REG_FIFO_STATUS4                            0x3D
#define LSM6DS3_REG_FIFO_DATA_OUT_L                         0x3E
#define LSM6DS3_REG_FIFO_DATA_OUT_H                         0x3F
#define LSM6DS3_REG_TIMESTAMP0_REG                          0x40
#define LSM6DS3_REG_TIMESTAMP1_REG                          0x41
#define LSM6DS3_REG_TIMESTAMP2_REG                          0x42
#define LSM6DS3_REG_STEP_TIMESTAMP_L                        0x49
#define LSM6DS3_REG_STEP_TIMESTAMP_H                        0x4A
#define LSM6DS3_REG_STEP_COUNTER_L                          0x4B
#define LSM6DS3_REG_STEP_COUNTER_H                          0x4C
#define LSM6DS3_REG_SENSORHUB13_REG                         0x4D
#define LSM6DS3_REG_SENSORHUB14_REG                         0x4E
#define LSM6DS3_REG_SENSORHUB15_REG                         0x4F
#define LSM6DS3_REG_SENSORHUB16_REG                         0x50
#define LSM6DS3_REG_SENSORHUB17_REG                         0x51
#define LSM6DS3_REG_SENSORHUB18_REG                         0x52
#define LSM6DS3_REG_FUNC_SRC                                0x53
#define LSM6DS3_REG_TAP_THS_6D                              0x59
#define LSM6DS3_REG_INT_DUR2                                0x5A
#define LSM6DS3_REG_WAKE_UP_THS                             0x5B
#define LSM6DS3_REG_WAKE_UP_DUR                             0x5C
#define LSM6DS3_REG_FREE_FALL                               0x5D
#define LSM6DS3_REG_MD1_CFG                                 0x5E
#define LSM6DS3_REG_MD2_CFG                                 0x5F
#define LSM6DS3_REG_OUT_MAG_RAW_X_L                         0x66
#define LSM6DS3_REG_OUT_MAG_RAW_X_H                         0x67
#define LSM6DS3_REG_OUT_MAG_RAW_Y_L                         0x68
#define LSM6DS3_REG_OUT_MAG_RAW_Y_H                         0x69
#define LSM6DS3_REG_OUT_MAG_RAW_Z_L                         0x6A
#define LSM6DS3_REG_OUT_MAG_RAW_Z_H                         0x6B

// FIFO modes
#define LSM6DS3_FIFO_BYPASS_MODE             				0
#define LSM6DS3_FIFO_FIFO_MODE             					1
#define LSM6DS3_FIFO_CONT_MODE             					6

// FIFO status bits
#define LSM6DS3_REG_FIFO_OVERRUN           					0x40
#define LSM6DS3_REG_FIFO_THR               					0x80
#define LSM6DS3_REG_FIFO_FULL              					0x20
#define LSM6DS3_REG_FIFO_EMPTY             					0x10

typedef struct
{
  gap8 ( 1 ) ;
  tU8 FUNC_CFG_ACCESS ;
  gap8 ( 2 ) ;
  tU8 SENSOR_SYNC_TIME_FRAME ;
  gap8 ( 1 ) ;
  tU8 FIFO_CTRL1 ;
  tU8 FIFO_CTRL2 ;
  tU8 FIFO_CTRL3 ;
  tU8 FIFO_CTRL4 ;
  tU8 FIFO_CTRL5 ;
  tU8 ORIENT_CFG_G ;
  tU8 RESERVED ;
  tU8 INT1_CTRL ;
  tU8 INT2_CTRL ;
  tU8 WHO_AM_I ;
  tU8 CTRL1_XL ;
  tU8 CTRL2_G ;
  tU8 CTRL3_C ;
  tU8 CTRL4_C ;
  tU8 CTRL5_C ;
  tU8 CTRL6_C ;
  tU8 CTRL7_G ;
  tU8 CTRL8_XL ;
  tU8 CTRL9_XL ;
  tU8 CTRL10_C ;
  tU8 MASTER_CONFIG ;
  tU8 WAKE_UP_SRC ;
  tU8 TAP_SRC ;
  tU8 D6D_SRC ;
  tU8 STATUS_REG ;
  gap8 ( 1 ) ;
  tU8 OUT_TEMP_L ;
  tU8 OUT_TEMP_H ;
  tU8 OUTX_L_G ;
  tU8 OUTX_H_G ;
  tU8 OUTY_L_G ;
  tU8 OUTY_H_G ;
  tU8 OUTZ_L_G ;
  tU8 OUTZ_H_G ;
  tU8 OUTX_L_XL ;
  tU8 OUTX_H_XL ;
  tU8 OUTY_L_XL ;
  tU8 OUTY_H_XL ;
  tU8 OUTZ_L_XL ;
  tU8 OUTZ_H_XL ;
  tU8 SENSORHUB1_REG ;
  tU8 SENSORHUB2_REG ;
  tU8 SENSORHUB3_REG ;
  tU8 SENSORHUB4_REG ;
  tU8 SENSORHUB5_REG ;
  tU8 SENSORHUB6_REG ;
  tU8 SENSORHUB7_REG ;
  tU8 SENSORHUB8_REG ;
  tU8 SENSORHUB9_REG ;
  tU8 SENSORHUB10_REG ;
  tU8 SENSORHUB11_REG ;
  tU8 SENSORHUB12_REG ;
  tU8 FIFO_STATUS1 ;
  tU8 FIFO_STATUS2 ;
  tU8 FIFO_STATUS3 ;
  tU8 FIFO_STATUS4 ;
  tU8 FIFO_DATA_OUT_L ;
  tU8 FIFO_DATA_OUT_H ;
  tU8 TIMESTAMP0_REG ;
  tU8 TIMESTAMP1_REG ;
  tU8 TIMESTAMP2_REG ;
  gap8 ( 6 ) ;
  tU8 STEP_TIMESTAMP_L ;
  tU8 STEP_TIMESTAMP_H ;
  tU8 STEP_COUNTER_L ;
  tU8 STEP_COUNTER_H ;
  tU8 SENSORHUB13_REG ;
  tU8 SENSORHUB14_REG ;
  tU8 SENSORHUB15_REG ;
  tU8 SENSORHUB16_REG ;
  tU8 SENSORHUB17_REG ;
  tU8 SENSORHUB18_REG ;
  tU8 FUNC_SRC ;
  gap8 ( 5 ) ;
  tU8 TAP_THS_6D ;
  tU8 INT_DUR2 ;
  tU8 WAKE_UP_THS ;
  tU8 WAKE_UP_DUR ;
  tU8 FREE_FALL ;
  tU8 MD1_CFG ;
  tU8 MD2_CFG ;
  gap8 ( 6 ) ;
  tU8 OUT_MAG_RAW_X_L ;
  tU8 OUT_MAG_RAW_X_H ;
  tU8 OUT_MAG_RAW_Y_L ;
  tU8 OUT_MAG_RAW_Y_H ;
  tU8 OUT_MAG_RAW_Z_L ;
  tU8 OUT_MAG_RAW_Z_H ;
} LSM6DS3_registersMappingTy ;

typedef enum
{
    LSM6DS3_STATUS_RESET        = 0,
    LSM6DS3_STATUS_ON           = 1,
    LSM6DS3_STATUS_FIFO_OVR     = 2,
    LSM6DS3_STATUS_FIFO_ERR     = 3,
    LSM6DS3_STATUS_FIFO_EMPTY   = 4
} LSM6DS3_statusEnumTy;

typedef enum
{
    LSM6DS3_FIFO_ODR_OFF        = 0 ,
    LSM6DS3_FIFO_ODR_13_Hz      = 1 ,
    LSM6DS3_FIFO_ODR_26_Hz      = 2 ,
    LSM6DS3_FIFO_ODR_52_Hz      = 3 ,
    LSM6DS3_FIFO_ODR_104_Hz     = 4 ,
    LSM6DS3_FIFO_ODR_208_Hz     = 5 ,
    LSM6DS3_FIFO_ODR_416_Hz     = 6 ,
    LSM6DS3_FIFO_ODR_833_Hz     = 7 ,
    LSM6DS3_FIFO_ODR_1660_Hz    = 8 ,
    LSM6DS3_FIFO_ODR_3333_Hz    = 9 ,
    LSM6DS3_FIFO_ODR_6666_Hz    = 10 ,
} LSM6DS3_OdrEnumTy;

typedef struct
{
    tS32 timeStampUs;
    tS16 acc_x;
    tS16 acc_y;
    tS16 acc_z;
} LSM6DS3_AccDataTy ;

typedef struct
{
    tS32 timeStampUs;
    tS16 pitch_x;
    tS16 roll_y;
    tS16 yaw_z;
} LSM6DS3_GyroDataTy ;

struct LSM6DS3_sensitivityLevelsTy
{
	tSInt fullScale;
	float sensitivity;
};

#define ACC_BUFFER_SIZE         0x1000
#define GYRO_BUFFER_SIZE        0x1000

static LSM6DS3_AccDataTy LSM6D3_accDataBuffer[ACC_BUFFER_SIZE];
static LSM6DS3_GyroDataTy LSM6D3_gyroDataBuffer[GYRO_BUFFER_SIZE];

static tU32 LSM6D3_accDataIdx = 0;
static tU32 LSM6D3_gyroDataIdx = 0;
static tU32 LSM6D3_accDataNum = ACC_BUFFER_SIZE;
static tU32 LSM6D3_gyroDataNum = ACC_BUFFER_SIZE;

///
// User selection values
///
static tU32 LSM6D3_accRng = 2; // +/- 2g
static tU32 LSM6D3_gyroRng = 125; // +/-125
static tU32 LSM6D3_Odr = 0;
static tU8  LSM6D3_Mask = LSM6DS3_FIFO_ODR_OFF;

// TODO: This must be corrected, see tables at the bottom of the file
static float LSM6D3_accLsb; // Internally calculated when acceleration range is programmed

static LSM6DS3_statusEnumTy LSM6DS3_status;

volatile static LSM6DS3_registersMappingTy LSM6DS3_registersMapping;
static struct timeval LSM6DS3_startTimeValue;

#if (defined WRITE_TO_FILE)
	static ofstream LSM6DS3_reportFile;
#endif  // #if (defined WRITE_TO_FILE)

#if (defined LSM6DS3_TEST_ENABLED)
	static tSInt LSM6DS3_TEST_LogData[128] = {0x00};
	static tSInt LSM6DS3_TEST_LogDataIndex = 0;
#endif // #if (defined LSM6DS3_TEST_ENABLED)

static MAYBE_UNUSED tS32 LSM6DS3_ReadAccelerationAndGyroscopeRegister (tSInt samplesToRead, LSM6DS3_AccDataTy *AccDataPnt, LSM6DS3_GyroDataTy *gyroDataPnt);
static tS32 LSM6DS3_WriteControlRegisters (void);
static tS32 LSM6DS3_ReadId (tPU8 idVal);
static tU16 LSM6DS3_ReadFIFOSamplesNum (tBool *fifoOverrun, tBool *fifoThrStatus, tBool *fifoFull, tBool *fifoEmpty);
static void LSM6DS3_FifoReset (void);
static tS32 LSM6DS3_StoreData (tSInt memoryPoolId, tSInt accStartIndex, tSInt accReadLen, tSInt gyroStartIndex, tSInt gyroReadLen);
static float LSM6DS3_CalculateAccelerationValue (tS16 accSample);
static MAYBE_UNUSED float LSM6DS3_CalculateAngularRateValue (tS16 gyroSample);

tS32 LSM6DS3_Open (void)
{
    // Start I2C mode
    bcm2835_i2c_begin ();

    // Setup I2C
    bcm2835_i2c_setSlaveAddress (LSM6DS3_I2C_ADD);
    bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_150); // (BCM2835_I2C_CLOCK_DIVIDER_626);

    return ERROR_CODE_NO_ERROR;
}

tS32 LSM6DS3_Close (void)
{
    // Close I2C mode
    bcm2835_i2c_end ();

    return ERROR_CODE_NO_ERROR;
}

tS32 LSM6DS3_Init (void)
{
    tS32 res = ERROR_CODE_NO_ERROR;
    tU8 deviceId;

    // Initialize globals
    LSM6D3_accDataIdx = 0;
    LSM6D3_gyroDataIdx = 0;
    LSM6D3_accDataNum = ACC_BUFFER_SIZE;
    LSM6D3_gyroDataNum = ACC_BUFFER_SIZE;

    LSM6DS3_status = LSM6DS3_STATUS_RESET;

    memset (&LSM6D3_accDataBuffer, 0x00, sizeof (LSM6D3_accDataBuffer));
    memset (&LSM6D3_gyroDataBuffer, 0x00, sizeof (LSM6D3_gyroDataBuffer));

    // Get start point for delta time
    gettimeofday (&LSM6DS3_startTimeValue, NULL);

    // Open LSM6DS3 I2C access
    LSM6DS3_Open();

    // Read ID to identify the proper device
    res = LSM6DS3_ReadId (&deviceId);

    if (ERROR_CODE_NO_ERROR != res || LSM6DS3_DEVICE_ID != deviceId)
    {
        // Close LSM6DS3 I2C access
        LSM6DS3_Close ();

        return ERROR_CODE_GENERIC;
    }

    // Open report file
#if (defined WRITE_TO_FILE)
    LSM6DS3_reportFile.open ("LSM6DS3_report.txt", ios::out | ios::trunc);

    if (false == LSM6DS3_reportFile.is_open ())
    {
        // Close LSM6DS3 I2C access
        LSM6DS3_Close ();

        return ERROR_CODE_GENERIC;
    }

    // Write file first line
    LSM6DS3_reportFile << "LSM6DS3 DATA REPORT" << '\n';
#endif  // #if (defined WRITE_TO_FILE)

//  // Get the shared memory area (from services)
//  LSM6DS3_sharedMemPtr = IPC_SharedMemoryGetDataPointer ();
//  LSM6DS3_sharedMemIndex = 0;

    // Write control registers
    LSM6DS3_WriteControlRegisters ();

    // Close report file
#if (defined WRITE_TO_FILE)
    LSM6DS3_reportFile.close ();
#endif // #if (defined WRITE_TO_FILE)

    // Close LSM6DS3 I2C access
    LSM6DS3_Close ();

    return res;
}

tS32 LSM6DS3_ReadAccelerationSequence (tSInt memoryPoolId)
{
    tU16 FIFOSamplesNum ;
    tS32 res = ERROR_CODE_NO_ERROR;
    tBool fifoverrunOccurred, fifoThrReached, fifoEmpty, fifoFull;
    tSInt samplesToRead;
    static tSInt maxSamplesReceived = 0;

    // Open LSM6DS3 I2C access
    LSM6DS3_Open ();

    // Read status register
    // LSM6DS0_ReadStatusRegister (&tempDataAvail, &accDataAvail, &gyroDataAvail);

    // Check if we have samples in the FIFO for acceleration
    FIFOSamplesNum = LSM6DS3_ReadFIFOSamplesNum (&fifoverrunOccurred, &fifoThrReached, &fifoEmpty, &fifoFull);

    // If we have an overrun then we clear the FIFO and go on,
    // otherwise we get all samples we have
    if (true == fifoverrunOccurred)
    {
        // Display error
        if (LSM6DS3_STATUS_FIFO_OVR != LSM6DS3_status)
        {
            printf ("FIFO overrun\n");
        }

        // Set device status to FIFO OVERRUN
        LSM6DS3_status = LSM6DS3_STATUS_FIFO_OVR;

        // Reset FIFO
        // LSM6DS0_FifoReset ();
    }

    // Check error: if status is used preferred is ' if (0 == FIFOSamplesNum && true == accDataAvail)'
    if (true == fifoEmpty && true == fifoFull)
    {
        // We have unread samples and FIFO is empty? Sounds like an error
        if (LSM6DS3_STATUS_FIFO_ERR != LSM6DS3_status)
        {
            printf ("ERROR: FIFO is empty but also full\n");
        }

        // Set device status to FIFO ERROR
        LSM6DS3_status = LSM6DS3_STATUS_FIFO_ERR;

        // Reset FIFO
        // LSM6DS0_FifoReset ();
    }
    else if (false == fifoThrReached)
    {
        // Return
        return res;
    }
    else
    {
        // Set device status to ON, device is working as expected
        LSM6DS3_status = LSM6DS3_STATUS_ON;

        // Save number of samples received
        if (maxSamplesReceived < FIFOSamplesNum)
        {
            maxSamplesReceived = FIFOSamplesNum;

            printf ("Max S: %d\n", maxSamplesReceived);
        }

#if (defined LSM6DS3_TEST_ENABLED)
        // Log data for TEST
        if (LSM6DS3_TEST_LogDataIndex < 128)
        {
        	LSM6DS3_TEST_LogData[LSM6DS3_TEST_LogDataIndex] = FIFOSamplesNum;
        	LSM6DS3_TEST_LogDataIndex++;
        }
#endif // #if (defined LSM6DS3_TEST_ENABLED)

        // Calculate samples to read
#if (defined LSM6DS3_GYRO_ENABLED)
        samplesToRead = FIFOSamplesNum - (FIFOSamplesNum % 6);
#else
        samplesToRead = FIFOSamplesNum - (FIFOSamplesNum % 3);
#endif // #if (defined LSM6DS3_GYRO_ENABLED)

        // Now read samples
        if (samplesToRead >= 0)
        {
            LSM6DS3_ReadAccelerationAndGyroscopeRegister (samplesToRead, &LSM6D3_accDataBuffer[0], &LSM6D3_gyroDataBuffer[0]);

            // Write data to file:
            // - Memory pool ID, acc start buffer pos, acc data len, gyro start buffer pos, gyro data len
#if (defined LSM6DS3_GYRO_ENABLED)
            LSM6DS3_StoreData (memoryPoolId, 0, (samplesToRead / 2), 0, (samplesToRead / 2));
#else
            LSM6DS3_StoreData (memoryPoolId, 0, samplesToRead, 0, 0);
#endif // #if (defined LSM6DS3_GYRO_ENABLED)
        }
    }

    // Close LSM6DS3 I2C access
    LSM6DS3_Close ();

    return res;
}

static tS32 MAYBE_UNUSED LSM6DS3_ReadAccelerationAndGyroscopeRegister (tSInt samplesToRead, LSM6DS3_AccDataTy *AccDataPnt, LSM6DS3_GyroDataTy *gyroDataPnt)
{
    tS32 readRes = 0;
    static tChar rdBuffer[1000];
    struct timeval CurrTaskTime;
    tSInt cnt;
    tSInt byteToRead = samplesToRead * 2;

    gettimeofday (&CurrTaskTime, NULL);

    ///
    // FIFO DISABLED MODE
    ///
#if (defined LSM6DS3_FIFO_DISABLED)
#if (defined LSM6DS3_GYRO_ENABLED)
    // Read from DATA REGs all accelerometer and gyroscope axis
    readRes = I2C_GetDataWithAutoIncrement (LSM6DS0_REG_OUT_X_L_G, 12, rdBuffer);

    gyroDataPnt->timeStampUs = CurrTaskTime.tv_usec;
    gyroDataPnt->pitch_x = (((int)rdBuffer[1]) << 8) + (int)rdBuffer[0];
    gyroDataPnt->roll_y = (((int)rdBuffer[3]) << 8) + (int)rdBuffer[2];
    gyroDataPnt->yaw_z = (((int)rdBuffer[5]) << 8) + (int)rdBuffer[4];

    AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;
    AccDataPnt->acc_x = (((int)rdBuffer[7]) << 8) + (int)rdBuffer[6];
    AccDataPnt->acc_y = (((int)rdBuffer[9]) << 8) + (int)rdBuffer[8];
    AccDataPnt->acc_z = (((int)rdBuffer[11]) << 8) + (int)rdBuffer[9];
#else
    // Read from DATA REGs all accelerometer and gyroscope axis
    readRes = I2C_GetDataWithAutoIncrement (LSM6DS0_REG_OUT_X_L_XL, 6, rdBuffer);

    AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;
    AccDataPnt->acc_x = (((int)rdBuffer[1]) << 8) + (int)rdBuffer[0];
    AccDataPnt->acc_y = (((int)rdBuffer[3]) << 8) + (int)rdBuffer[2];
    AccDataPnt->acc_z = (((int)rdBuffer[5]) << 8) + (int)rdBuffer[4];
#endif // #if (defined LSM6DS3_GYRO_ENABLED)

#else
    ///
    // FIFO ENABLED MODE
    ///
#if (defined LSM6DS3_GYRO_ENABLED)
    // Read from FIFO all accelerometer axis using the auto-increment wrap feature
    readRes = I2C_GetDataWithAutoIncrement (LSM6DS3_REG_FIFO_DATA_OUT_L, byteToRead, rdBuffer);

    for (cnt = 0; cnt < byteToRead; cnt += 12)
    {

		AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;

		AccDataPnt->acc_x = (((tS16)rdBuffer[7 + cnt]) << 8) + (tS16)rdBuffer[6 + cnt];
		AccDataPnt->acc_y = (((tS16)rdBuffer[9 + cnt]) << 8) + (tS16)rdBuffer[8 + cnt];
		AccDataPnt->acc_z = (((tS16)rdBuffer[11 + cnt]) << 8) + (tS16)rdBuffer[10 + cnt];

		AccDataPnt++;

		gyroDataPnt->timeStampUs = CurrTaskTime.tv_usec;

		gyroDataPnt->pitch_x = (((tS16)rdBuffer[1 + cnt]) << 8) + (tS16)rdBuffer[0 + cnt];
		gyroDataPnt->roll_y = (((tS16)rdBuffer[3 + cnt]) << 8) + (tS16)rdBuffer[2 + cnt];
		gyroDataPnt->yaw_z = (((tS16)rdBuffer[5 + cnt]) << 8) + (tS16)rdBuffer[4 + cnt];

		gyroDataPnt++;
    }
#else
    // Read from FIFO all accelerometer axis using the auto-increment wrap feature
    if (byteToRead <= 1000)
    {
		readRes = I2C_GetDataWithAutoIncrement (LSM6DS3_REG_FIFO_DATA_OUT_L, byteToRead, rdBuffer);

		for (cnt = 0; cnt < byteToRead; cnt += 6)
		{
			AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;

			AccDataPnt->acc_x = (((tS16)rdBuffer[1 + cnt]) << 8) + (tS16)rdBuffer[0 + cnt];
			AccDataPnt->acc_y = (((tS16)rdBuffer[3 + cnt]) << 8) + (tS16)rdBuffer[2 + cnt];
			AccDataPnt->acc_z = (((tS16)rdBuffer[5 + cnt]) << 8) + (tS16)rdBuffer[4 + cnt];

			AccDataPnt++;
		}
    }
#endif // #if (defined LSM6DS3_GYRO_ENABLED)
#endif // #if (defined LSM6DS3_FIFO_DISABLED)

    return readRes;
}

static tU16 LSM6DS3_ReadFIFOSamplesNum (tBool *fifoOverrun, tBool *fifoThrStatus, tBool *fifoFull, tBool *fifoEmpty)
{
    tS32 readRes;
    tChar rdStatusBfr[4] = {0x00, 0x00, 0x00, 0x00};
    tU16  FifoSamplesNum ;

    readRes = I2C_GetDataWithAutoIncrement (LSM6DS3_REG_FIFO_STATUS1, 4, &rdStatusBfr[0]);

    LSM6DS3_registersMapping.FIFO_STATUS1 = rdStatusBfr[0];
    LSM6DS3_registersMapping.FIFO_STATUS2 = rdStatusBfr[1];
    LSM6DS3_registersMapping.FIFO_STATUS3 = rdStatusBfr[2];
    LSM6DS3_registersMapping.FIFO_STATUS4 = rdStatusBfr[3];

    FifoSamplesNum = (((tU16)(rdStatusBfr[1] & 0x0F)) << 8) | (((tU16)rdStatusBfr[0]) & 0xFF);

    if (LSM6DS3_registersMapping.FIFO_STATUS2 & LSM6DS3_REG_FIFO_OVERRUN)
    {
        *fifoOverrun = true;
    }
    else
    {
        *fifoOverrun = false;
    }

    if (LSM6DS3_registersMapping.FIFO_STATUS2 & LSM6DS3_REG_FIFO_THR)
    {
        *fifoThrStatus = true;
    }
    else
    {
        *fifoThrStatus = false;
    }

    if (LSM6DS3_registersMapping.FIFO_STATUS2 & LSM6DS3_REG_FIFO_EMPTY)
    {
        *fifoEmpty = true;
    }
    else
    {
        *fifoEmpty = false;
    }

    if (LSM6DS3_registersMapping.FIFO_STATUS2 & LSM6DS3_REG_FIFO_FULL)
    {
        *fifoFull = true;
    }
    else
    {
        *fifoFull = false;
    }

    C_UNUSED(readRes);

    return FifoSamplesNum;
}

static tS32 LSM6DS3_WriteControlRegisters (void)
{
    tS32 res = ERROR_CODE_NO_ERROR;
    tChar wrBfr[10] = {0x00};
    tU8 TempByte;
    volatile tChar rdData;

    // Before to proceed we do a sw-reset to be sure to restart in default conditions
    wrBfr[0] = 0x01;
    I2C_WriteData (LSM6DS3_REG_CTRL3_C, 1, wrBfr);

    // Wait 200 ms before to proceed (20 ms is the device boot time)
    usleep (200);

    // Poll until the sw-reset flag become 0 again
    I2C_GetData (LSM6DS3_REG_CTRL3_C, 1, (char *)&rdData);

    while (rdData & (tChar)0x01)
    {
    	I2C_GetData (LSM6DS3_REG_CTRL3_C, 1, (char *)&rdData);
    }

    // Set device status to ON
    LSM6DS3_status = LSM6DS3_STATUS_ON;

    // Program Accelerometer
    TempByte = ( LSM6D3_Mask << 4 ) ;

    if ( LSM6D3_accRng >= 16 )
    {
         TempByte |= 0x04 ;
    }
    else if ( LSM6D3_accRng >= 8 )
    {
         TempByte |= 0x0C ;
    }
    else if ( LSM6D3_accRng >= 4 )
    {
         TempByte |= 0x08 ;
    }

    // Disable FIFO setting it in BYPASS mode
    LSM6DS3_registersMapping.FIFO_CTRL5 = 0x00;
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL5;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL5, 1, wrBfr);

    // Do not use internal pull-up for I2C
    LSM6DS3_registersMapping.MASTER_CONFIG = 0x00;
    wrBfr[0] = LSM6DS3_registersMapping.MASTER_CONFIG;
    I2C_WriteData (LSM6DS3_REG_MASTER_CONFIG, 1, wrBfr);

    // Set auto-increment mode ON and BDU OFF (it must be when FIFO is used)
    LSM6DS3_registersMapping.CTRL3_C = 0x04;
    wrBfr[0] = LSM6DS3_registersMapping.CTRL3_C;
    I2C_WriteData (LSM6DS3_REG_CTRL3_C, 1, wrBfr);

    // Data set 3 and 3 not in FIFO
    LSM6DS3_registersMapping.CTRL4_C = 0x00;
    wrBfr[0] = LSM6DS3_registersMapping.CTRL4_C;
    I2C_WriteData (LSM6DS3_REG_CTRL4_C, 1, wrBfr);

    // Enable ACC
    LSM6DS3_registersMapping.CTRL1_XL = TempByte; // Acc f @1.66kHz: 0x80
    wrBfr[0] = LSM6DS3_registersMapping.CTRL1_XL;
    I2C_WriteData (LSM6DS3_REG_CTRL1_XL, 1, wrBfr);

    // Enable GYRO
#if (defined LSM6DS3_GYRO_ENABLED)
    LSM6DS3_registersMapping.CTRL2_G = TempByte; // Gyro f @1.66kHz: 0x80
    wrBfr[0] = LSM6DS3_registersMapping.CTRL2_G;
    I2C_WriteData (LSM6DS3_REG_CTRL2_G, 1, wrBfr);
#endif // #if (defined LSM6DS3_GYRO_ENABLED)

    // Enable accelerometer axes
    LSM6DS3_registersMapping.CTRL9_XL = 0x38;
    wrBfr[0] = LSM6DS3_registersMapping.CTRL9_XL;
    I2C_WriteData (LSM6DS3_REG_CTRL9_XL, 1, wrBfr);

#if (defined LSM6DS3_GYRO_ENABLED)
    LSM6DS3_registersMapping.CTRL10_C = 0x38;
    wrBfr[0] = LSM6DS3_registersMapping.CTRL10_C;
    I2C_WriteData (LSM6DS3_REG_CTRL10_C, 1, wrBfr);
#else
	LSM6DS3_registersMapping.CTRL10_C = 0x00;
	wrBfr[0] = LSM6DS3_registersMapping.CTRL10_C;
	I2C_WriteData (LSM6DS3_REG_CTRL10_C, 1, wrBfr);
#endif // #if (defined LSM6DS3_GYRO_ENABLED)

    // Insert in FIFO only ACC or ACC+GYRO data depending on the settings
#if (defined LSM6DS3_GYRO_ENABLED)
    LSM6DS3_registersMapping.FIFO_CTRL3 = 0x09;
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL3;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL3, 1, wrBfr);
#else
    LSM6DS3_registersMapping.FIFO_CTRL3 = 0x01;
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL3;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL3, 1, wrBfr);
#endif // #if (defined LSM6DS3_GYRO_ENABLED)

    // Set FIFO thr level to 30 samples
    LSM6DS3_registersMapping.FIFO_CTRL1 = 30;
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL1;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL1, 1, wrBfr);

    // Do not limit FIFO to THR
    LSM6DS3_registersMapping.CTRL4_C = 0x00;
    wrBfr[0] = LSM6DS3_registersMapping.CTRL4_C;
    I2C_WriteData (LSM6DS3_REG_CTRL4_C, 1, wrBfr);

    // Set FIFO_CTRL2 (setting secret bits for SPI)
    LSM6DS3_registersMapping.FIFO_CTRL2 = 0x30;
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL2;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL2, 1, wrBfr);

    // Program FIFO ODR and FIFO MODE
    TempByte = ( LSM6D3_Mask << 3 ) | LSM6DS3_FIFO_CONT_MODE;
    LSM6DS3_registersMapping.FIFO_CTRL5 = TempByte;	// Use 0x46 to set f @1.66kHz and CONT mode
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL5;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL5, 1, wrBfr);

    return res;
}

static tS32 LSM6DS3_ReadId (tPU8 idVal)
{
    tU8 readRes;
    volatile tChar initRdBfr [5] = {0x00, 0x00, 0x00, 0x00, 0x00};

    readRes = I2C_GetData (LSM6DS3_REG_WHO_AM_I, 1, (char *)&initRdBfr[0]);
    printf ("LSM6DS3 Who Am I reg read result = %d : 0x%x\n", readRes, initRdBfr[0]);

    *idVal = initRdBfr[0];

    return ERROR_CODE_NO_ERROR;
}

void LSM6DS3_SetAccRange (int AccelerationRange)
{
	tU8 TempByte = 0;

    if (AccelerationRange >= 16)
    {
    	LSM6D3_accRng = 16;
    	TempByte |= 0x04;
    }
    else if (AccelerationRange >= 8)
    {
    	LSM6D3_accRng = 8;
    	TempByte |= 0x0C;
    }
    else if (AccelerationRange >= 4)
    {
    	LSM6D3_accRng = 4;
    	TempByte |= 0x08;
    }
    else // +- 2g
    {
    	LSM6D3_accRng = 2;
    	TempByte = 0;
    }

    LSM6D3_accLsb = (2.0 * LSM6D3_accRng) / 0x10000;
}

void LSM6DS3_SetOutputDataRate (int OutputDatarate)
{
    if (OutputDatarate <= 13)
    {
    	LSM6D3_Odr = 13;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_13_Hz;
    }
    else if (OutputDatarate <= 26)
    {
    	LSM6D3_Odr = 26;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_26_Hz;
    }
    else if (OutputDatarate <= 52)
    {
    	LSM6D3_Odr = 52;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_52_Hz;
    }
    else if (OutputDatarate <= 104)
    {
    	LSM6D3_Odr = 104;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_104_Hz;
    }
    else if (OutputDatarate <= 208)
    {
    	LSM6D3_Odr = 208;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_208_Hz;
    }
    else if (OutputDatarate <= 416)
    {
    	LSM6D3_Odr = 416;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_416_Hz;
    }
    else if (OutputDatarate <= 833)
    {
    	LSM6D3_Odr = 833;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_833_Hz;
    }
    else if (OutputDatarate <= 1660)
    {
    	LSM6D3_Odr = 1660;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_1660_Hz;
    }
    else if (OutputDatarate <= 3330)
    {
    	LSM6D3_Odr = 3330;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_3333_Hz;
    }
    else if (OutputDatarate <= 6660)
    {
    	LSM6D3_Odr = 6660;
    	LSM6D3_Mask = LSM6DS3_FIFO_ODR_6666_Hz;
    }
    else // OFF
    {
    	LSM6D3_Odr = 0;
        LSM6D3_Mask = LSM6DS3_FIFO_ODR_OFF;
    }
}

static tS32 LSM6DS3_StoreData (tSInt memoryPoolId, tSInt accStartIndex, tSInt accReadLen, tSInt gyroStartIndex, tSInt gyroReadLen)
{
	struct timeval curTimeValue;
    float mSec;
    tSInt cnt;
    tSInt axisSamples = accReadLen / 3; // We have 3-axis, so if we get 'x' samples we need to get them from 'x / 3' slots

    // Get current time
    gettimeofday (&curTimeValue, NULL);

    mSec = (curTimeValue.tv_sec - LSM6DS3_startTimeValue.tv_sec) * 1000 +
           (curTimeValue.tv_usec - LSM6DS3_startTimeValue.tv_usec) / (float)1000;

    // Depending on configuration we write data on file or on shared memory
#if (defined WRITE_TO_FILE)
    // Write data to file

    std::stringstream textStream;


    LSM6DS3_reportFile.open ("LSM6DS3_report.txt", ios::out | ios::app);

    if (false == LSM6DS3_reportFile.is_open ())
    {
        return ERROR_CODE_GENERIC;
    }

    // Write result to CSV file, format:
    // <time stamp> - <acceleration X Y Z> - <gyroscope X Y Z>
    textStream.str (std::string());
    textStream.clear();

    while (accReadLen > 0 || gyroReadLen > 0)
    {
        textStream << mSec << "," << "Acc X:  ," << LSM6D3_accDataBuffer[accStartIndex].acc_x << "," \
                                  << "Acc Y:  ," << LSM6D3_accDataBuffer[accStartIndex].acc_y << "," \
                                  << "Acc Z:  ," << LSM6D3_accDataBuffer[accStartIndex].acc_z << "," \
                                  << "Gyro X: ," << LSM6D3_gyroDataBuffer[gyroStartIndex].pitch_x << "," \
                                  << "Gyro Y: ," << LSM6D3_gyroDataBuffer[gyroStartIndex].roll_y << "," \
                                  << "Gyro Z: ," << LSM6D3_gyroDataBuffer[gyroStartIndex].yaw_z  << "," << '\n';

        accReadLen--;
        gyroReadLen--;

        if (accStartIndex < (ACC_BUFFER_SIZE - 1))
        {
            accStartIndex++;
        }
        else
        {
            accStartIndex = 0;
        }

        if (gyroStartIndex < (GYRO_BUFFER_SIZE - 1))
        {
            gyroStartIndex++;
        }
        else
        {
            gyroStartIndex = 0;
        }
    }

    std::string tmpStr = textStream.str();
    LSM6DS3_reportFile << tmpStr;

    // Close report file
    LSM6DS3_reportFile.close ();
#else
    ///
    // Write using async method: shared memory or memory pool
    ///
    if (accReadLen > 0 || gyroReadLen > 0)
    {
		float accValueX, accValueY, accValueZ;
#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
		float accValueG;
        tSInt slot = 0;
        IPC_sharedMemoryElemTy *sharedMemPtr;
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)

#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
		// Get the pointer to an available memory pointer
        sharedMemPtr = IPC_SharedMemoryGetWriterDataPointer ();
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)

#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
        while (accReadLen > 0 || gyroReadLen > 0)
        {
			///
			// Store data to the shared memory
			///
			// Store acceleration values
			//    We need to calculate the value of acceleration in 'g', doing this we
			//    can apply the current scale and there's no need to propagate this information
			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex].acc_x);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_X;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex].acc_y);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_Y;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex].acc_z);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_Z;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			// Store gyroscope values
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_X;
			sharedMemPtr->dataBfr[slot++].data = LSM6D3_gyroDataBuffer[gyroStartIndex].pitch_x;

			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_Y;
			sharedMemPtr->dataBfr[slot++].data = LSM6D3_gyroDataBuffer[gyroStartIndex].roll_y;

			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_Z;
			sharedMemPtr->dataBfr[slot++].data = LSM6D3_gyroDataBuffer[gyroStartIndex].yaw_z;

            sharedMemPtr->writerId = APP_THREAD_LSM6DS0_ID;
            sharedMemPtr->time = mSec;
            sharedMemPtr->len = slot;
        }
#else
		///
		// Write the data to the locked memory pool
		///
		for (cnt = 0; cnt < axisSamples; cnt++)
		{
			accValueX = LSM6DS3_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex + cnt].acc_x);
			accValueY = LSM6DS3_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex + cnt].acc_y);
			accValueZ = LSM6DS3_CalculateAccelerationValue (LSM6D3_accDataBuffer[accStartIndex + cnt].acc_z);

#if (defined APP_USE_MEMORYPOOL)
			IPC_MemoryPoolPush (memoryPoolId, mSec, accValueX, accValueY, accValueZ);
#else
			C_UNUSED (mSec);
			C_UNUSED (accValueX);
			C_UNUSED (accValueY);
			C_UNUSED (accValueZ);
#endif // #if (defined APP_USE_MEMORYPOOL)
		}
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
    }
#endif // #if (defined WRITE_TO_FILE)

    return ERROR_CODE_NO_ERROR;
}

void LSM6DS3_AccDataDisplay ( void )
{
    int DisplaySamplesIdx ;
    int BaseTimeStamp ;
    int PrevTimeStamp ;
    std::stringstream textStream;
    LSM6DS3_AccDataTy *AccDataPnt = LSM6D3_accDataBuffer ;
    float AccX_g ;
    float AccY_g ;
    float AccZ_g ;
    std::string tmpStr ;

    BaseTimeStamp = AccDataPnt -> timeStampUs ;
    PrevTimeStamp = BaseTimeStamp ;

#if (defined WRITE_TO_FILE)
    // Open report file
    LSM6DS3_reportFile.open ("LSM6DS3_report.txt", ios::out | ios::app);

    if (false == LSM6DS3_reportFile.is_open ())
    {
        // Close LSM6DS0 I2C access
        LSM6DS3_Close ();

        return ;
    }
#endif // #if (defined WRITE_TO_FILE)

    // Write file first line
    printf ( "Samples list\n" ) ;

    for ( DisplaySamplesIdx = 0 ; DisplaySamplesIdx < (tS32)LSM6D3_accDataNum ; DisplaySamplesIdx++ )
    {
        // Write result to CSV file
        AccX_g = LSM6D3_accLsb * ( ( (float) AccDataPnt -> acc_x ) ) ;
        AccY_g = LSM6D3_accLsb * ( ( (float) AccDataPnt -> acc_y ) ) ;
        AccZ_g = LSM6D3_accLsb * ( ( (float) AccDataPnt -> acc_z ) ) ;

        textStream.str (std::string());
        textStream.clear();
        textStream << AccDataPnt -> timeStampUs << "," << AccX_g << "," << AccY_g << "," << AccZ_g << '\n';
        tmpStr = textStream.str();
        cout << tmpStr;

#if (defined WRITE_TO_FILE)
        LSM6DS3_reportFile << tmpStr;
#endif // #if (defined WRITE_TO_FILE)

        PrevTimeStamp = AccDataPnt -> timeStampUs ;
        AccDataPnt++ ;
    } /* endif */

#if (defined WRITE_TO_FILE)
    // Close report file
    LSM6DS3_reportFile.close ();
#endif // #if (defined WRITE_TO_FILE)

    C_UNUSED(PrevTimeStamp);
}

static MAYBE_UNUSED void LSM6DS3_FifoReset (void)
{
	tChar wrBfr[10] = {0x00};

    // Disable FIFO setting it in BYPASS mode
	wrBfr[0] = (LSM6D3_Mask << 3) | LSM6DS3_FIFO_BYPASS_MODE;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL5, 1, wrBfr);

    // Program FIFO ODR and FIFO MODE
    LSM6DS3_registersMapping.FIFO_CTRL5 = ((LSM6D3_Mask << 3) | LSM6DS3_FIFO_FIFO_MODE);
    wrBfr[0] = LSM6DS3_registersMapping.FIFO_CTRL5;
    I2C_WriteData (LSM6DS3_REG_FIFO_CTRL5, 1, wrBfr);
}

static float LSM6DS3_CalculateAccelerationValue (tS16 accSample)
{
	const LSM6DS3_sensitivityLevelsTy LSM6DS3_sensitivityLevelsAccelerometer[] =
	{
			{2,  0.061E-3},
			{4,  0.122E-3},
			{8, 0.244E-3},
			{16, 0.488E-3}
	};

	// We set initial value to 0 to make sure ot return something valid if range was not properly set
	float accValueG = 0;

	if (2 == LSM6D3_accRng)
	{
		accValueG = (float)accSample * LSM6DS3_sensitivityLevelsAccelerometer[0].sensitivity;
	}
	else if (4 == LSM6D3_accRng)
	{
		accValueG = (float)accSample * LSM6DS3_sensitivityLevelsAccelerometer[1].sensitivity;
	}
	else if (8 == LSM6D3_accRng)
	{
		accValueG = (float)accSample * LSM6DS3_sensitivityLevelsAccelerometer[2].sensitivity;
	}
	else if (16 == LSM6D3_accRng)
	{
		accValueG = (float)accSample * LSM6DS3_sensitivityLevelsAccelerometer[3].sensitivity;
	}

	return accValueG;
}

static MAYBE_UNUSED float LSM6DS3_CalculateAngularRateValue (tS16 gyroSample)
{
	const LSM6DS3_sensitivityLevelsTy LSM6DS3_sensitivityLevelsGyroscope[] =
	{
			{125,  4.375E-3},
			{245,  8.75E-3},
			{500,  17.50E-3},
			{1000, 35E-3},
			{2000, 70E-3}
	};

	// We set initial value to 0 to make sure ot return something valid if range was not properly set
	float accValueG = 0;

	if (1255 == LSM6D3_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS3_sensitivityLevelsGyroscope[0].sensitivity;
	}
	else if (245 == LSM6D3_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS3_sensitivityLevelsGyroscope[0].sensitivity;
	}
	else if (500 == LSM6D3_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS3_sensitivityLevelsGyroscope[1].sensitivity;
	}
	else if (1000 == LSM6D3_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS3_sensitivityLevelsGyroscope[2].sensitivity;
	}
	else if (2000 == LSM6D3_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS3_sensitivityLevelsGyroscope[2].sensitivity;
	}

	return accValueG;
}


// End of file
