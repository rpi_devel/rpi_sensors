/*
 * led_common.h
 *
 *  Created on: Dec 2, 2014
 *      Author: alberto
 */

#ifndef LED_COMMON_H_
#define LED_COMMON_H_

#define GPIO_06					((char)6)
#define GPIO_11					((char)11)
#define GPIO_13					((char)13)
#define GPIO_17					((char)17)
#define GPIO_18					((char)18)
#define GPIO_19					((char)19)
#define GPIO_22					((char)22)
#define GPIO_23					((char)23)
#define GPIO_24					((char)24)
#define GPIO_25					((char)25)
#define GPIO_27					((char)27)

#define GPIO_INVALID			((char)0xFF)

typedef enum
{
	GPIODEV_DIRECTION_UNKNOWN 	= 0,
	GPIODEV_DIRECTION_OUT     	= 1,
	GPIODEV_DIRECTION_IN      	= 2
} GPIODEV_directionEnumTy;

typedef enum
{
	GPIODEV_VALUE_UNKNOWN		= 0,
	GPIODEV_VALUE_LOW 			= 1,
	GPIODEV_VALUE_HIGH			= 2
} GPIODEV_valEnumTy;

typedef enum
{
	GPIODEV_STATUS_CLOSED,
	GPIODEV_STATUS_OPEN,
	GPIODEV_STATUS_RESERVED,
	GPIODEV_STATUS_ERROR
} GPIODEV_statusEnumTy;

typedef enum
{
	GPIODEV_ALTFNCT_GPIO, // Alt fnct 0 is GPIO functionality
	GPIODEV_ALTFNCT_1,
	GPIODEV_ALTFNCT_2,
	GPIODEV_ALTFNCT_3,
	GPIODEV_ALTFNCT_4,
	GPIODEV_ALTFNCT_5,
	GPIODEV_ALTFNCT_UNKNOWN
} GPIODEV_alternateFnctEnumTy;

#endif // LED_COMMON_H_

// End of file
