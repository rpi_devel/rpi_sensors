/*
 * system_defines.h
 *
 *  Created on: May 14, 2015
 *      Author: alberto
 */

#ifndef SYSTEM_DEFINES_H_
#define SYSTEM_DEFINES_H_

// Application threads IDs
#define APP_THREAD_LSM6DS0_ID			0x01
#define APP_THREAD_HTS221_ID			0x02
#define APP_THREAD_LPS25H_ID			0x03
#define APP_THREAD_LIS3MDL_ID			0x04
#define APP_THREAD_LSM6DS3_ID			0x05

#endif /* SYSTEM_DEFINES_H_ */
