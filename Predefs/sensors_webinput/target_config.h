/*
 * target_config.h
 *
 *  Created on: Dec 7, 2014
 *      Author: alberto
 */

#ifndef TARGET_CONFIG_H_
#define TARGET_CONFIG_H_

///
// Define if file IO access or memory access is used for GPIOs
///
//#define GPIO_USE_IO_DEV
#undef GPIO_USE_IO_DEV

//#define GPIO_USE_MEM
#undef GPIO_USE_MEM

///
// Define if file some GPIO is accessed as a LED
///
//#define GPIO_USE_LEDS
#undef GPIO_USE_LEDS

///
// Define if you would like to use the SPI
///
//#define USE_SPI_DEV
#undef USE_SPI_DEV

///
// Define if you would like to use the I2C
///
//#define USE_I2C_DEV
#undef USE_I2C_DEV

///
// TEST: Define if you would like to use the I2C
///
//#define I2C_TEST_CODE
#undef I2C_TEST_CODE

///
// Define if you would like to use HTS221 relative humidity and temperature sensor
///
#define SENSOR_USE_HTS221
//#undef SENSOR_USE_HTS221

///
// Define if you would like to use LPS25H pressure (with device temperature) sensor
///
#define SENSOR_USE_LPS25H
//#undef SENSOR_USE_LPS25H

///
// Define if you would like to use LSM6DS0 accelerator and gyroscope (with device temperature) sensor
///
#define SENSOR_USE_LSM6DS0
//#undef SENSOR_USE_LSM6DS0

///
// Test mode for the LSM6DS0 sensor, must be defined together with 'SENSOR_USE_LSM6DS0'
///
//#define SENSOR_USE_LSM6DS0_TEST_MODE
#undef SENSOR_USE_LSM6DS0_TEST_MODE

///
// Define if you would like you use LIS3MDL magnetometer sensor
///
//#define SENSOR_USE_LIS3MDL
#undef SENSOR_USE_LIS3MDL

///
// Define to enable CONSOLE
///
//#define APP_USE_CONSOLE
#undef APP_USE_CONSOLE

///
// Define to redirect console to a file
///
#define APP_REDIRECT_STDOUT
//#undef APP_REDIRECT_STDOUT

///
// Define to enable PIPES
///
#define APP_USE_PIPES
//#undef APP_USE_PIPES

///
// Define to enable SHARED MEMORIES
///
#define APP_USE_SHAREDMEMS
//#undef APP_USE_SHAREDMEMS

///
// Define to enable SEMAPHORES: must be enabled when using sensors
///
#define APP_USE_SEMAPHORES
//#undef APP_USE_SEMAPHORES

///
// Define to use the memory pool
///
#define APP_USE_MEMORYPOOL
//#undef APP_USE_MEMORYPOOL

///
// Define to use the service thread
///
#define APP_USE_SERVICE_THREAD
//#undef APP_USE_SERVICE_THREAD

///
// Define to use the plots
///
#define APP_USE_PLOT_CAPABILITY
//#undef APP_USE_PLOT_CAPABILITY

///
// Sanity checks
///
#if (defined GPIO_USE_MEM) && (defined GPIO_USE_LEDS)
	#error "GPIO MEM and GPIO LED use the same GPIOs and cannot be enabled together"
#endif // #if (defined GPIO_USE_MEM) && (defined GPIO_USE_LEDS)

#if (defined SENSOR_USE_LSM6DS0_TEST_MODE) && (!defined SENSOR_USE_LSM6DS0)
	#error "Sensor LSM6DS0 defined for test mode without defining the usage of the sensor"
#endif // #if (defined GPIO_USE_MEM) && (defined GPIO_USE_LEDS)

#if (defined APP_USE_CONSOLE) && (defined APP_REDIRECT_STDOUT)
	#error "Console is used but the output is redirected to a file, this is an illegal combination"
#endif // #if (defined APP_USE_CONSOLE) && (defined APP_REDIRECT_STDOUT)

#endif /* TARGET_CONFIG_H_ */
