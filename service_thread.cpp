/*
 * service_thread.cpp
 *
 *  Created on: May 3, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>		// For 'sig_atomic_t'
#include <semaphore.h>  // For semaphores
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <string>       // std::string
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#include <boost/lexical_cast.hpp>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "system_defines.h"

#include "ipc.h"

#if (defined APP_USE_PLOT_CAPABILITY)
	#include "plot_data.h"
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

#include "service_thread.h"

using namespace std;

// Max dimension for a log file before to copy over (backup) and start from fresh
#define ST_LOG_FILE_MAX_DIM_MB 				((std::size_t)(100 * 1024 * 1024))  // 100MB

// Application logs
#define ST_LOG_FILE_APPLICATION				"/webroot/logs/application_rawdata.txt"
#define ST_LOG_FILE_APP_TEMP_HUMIDITY		"/webroot/logs/application_temperature.txt"
#define ST_LOG_FILE_APP_TEMP_HUMIDITY_LR	"/webroot/logs/application_temperature_lastread.txt"

#define ST_LOG_FILE_APP_ACC_GYRO			"/webroot/logs/application_accgyro.txt"
#define ST_LOG_FILE_APP_ACC_GYRO_NOEXT		"/webroot/logs/application_accgyro"

// Max value found inside a window for acceleration
#define ST_LOG_FILE_APP_MAX_ACC				"/webroot/logs/application_max_acc.txt"

// Session number file: unique number to identify the current run. It increments forever,
// until the user reset it or there's a wrap of the number contained into it
#define ST_FILE_SESSION_LOG					"./session_number.txt"

// Acceleration data type, contains:
// - R vector value
// - Angles between R vector and axis
struct ST_accDataTy
{
	float R;
	float Axr;
	float Ayr;
	float Azr;
};

static ST_accDataTy ST_greatestAcc;

volatile sig_atomic_t ST_ended = 0;
static pthread_t ST_threadId;
static tBool ST_exitThread = false;
static sem_t *ST_globalSemaphorePtr = NULL;
static ofstream ST_reportFile;
static ofstream ST_reportTemperatureHumidity;
static ofstream ST_reportTemperatureHumidityLastRead;
static ofstream ST_reportAccGyro;
static ofstream ST_reportMaxAcc;

static tSInt ST_sessionNumber = 0;

static tBool ST_accGyroLogFileTruncate = false;

static struct timeval ST_startTimeValue;

#if (defined APP_USE_SHAREDMEMS)
	static tSInt ST_GetDataFromSharedMemory (void);
#endif // #if (defined APP_USE_SHAREDMEMS)

static float ST_CalculateRVectorAndAngles (float accX, float accY, float accZ, ST_accDataTy *rAccVector);
static tSInt ST_CalculateMaxAccelerationValueInsideWindow (float accX, float accY, float accZ);
static tSInt ST_OutputMaxAccelerationValueInsideWindow (void);
static MAYBE_UNUSED tSInt ST_WriteApplicationLog (std::stringstream &textStream);
static MAYBE_UNUSED tSInt ST_WriteTemperatureHumidityLog (IPC_sharedMemoryElemTy *elemPtr);
static MAYBE_UNUSED tSInt ST_WriteAccGyroLog (IPC_sharedMemoryElemTy *elemPtr);
static MAYBE_UNUSED tSInt ST_WriteAccGyroLog (dataVectTy &dataVect);
static MAYBE_UNUSED tSInt ST_CallPlot (dataVectTy &pts);
static tBool ST_FileBackup (const string& filename, std::size_t dimension);
static tSInt ST_ManageSessionNumber (void);
static long int ST_CalculateDeltaTime (struct timeval *starttime, struct timeval *finishtime);

void ST_ExitThread (tBool exitThread)
{
	if (true == exitThread)
	{
		ST_exitThread = true;
	}
}

tS32 ST_SignalHandler (void)
{
	return ST_ended;
}

void* ST_Thread (void *arg)
{
#if (defined APP_USE_MEMORYPOOL)
	tSInt memoryPoolId;
	dataVectTy memPool;
#endif // #if (defined APP_USE_MEMORYPOOL)

	// Initialize globals
	ST_exitThread = false;
	ST_ended = 0;
	ST_accGyroLogFileTruncate = false;

	// Get start time
	gettimeofday (&ST_startTimeValue, NULL);

	// Get arguments
	ST_globalSemaphorePtr = (sem_t *)arg;

	// Get thread ID to return back at the end
	ST_threadId = pthread_self ();

	// Set session number. This number will be used to create unique file names
	ST_sessionNumber = ST_ManageSessionNumber ();

	// Open report file for RAW DATA log
	ST_reportFile.open (ST_LOG_FILE_APPLICATION, ios::out | ios::trunc);

	if (false == ST_reportFile.is_open ())
	{
		ST_exitThread = true;
	}

	// Open report file for TEXT DATA log for temperature and humidity
	ST_reportTemperatureHumidity.open (ST_LOG_FILE_APP_TEMP_HUMIDITY, ios::out | ios::trunc);

	if (false == ST_reportTemperatureHumidity.is_open ())
	{
		ST_exitThread = true;
	}

	// Open report for TEXT data last temperature and humidity read
	ST_reportTemperatureHumidityLastRead.open (ST_LOG_FILE_APP_TEMP_HUMIDITY_LR, ios::out | ios::trunc);

	if (false == ST_reportTemperatureHumidityLastRead.is_open ())
	{
		ST_exitThread = true;
	}

	// Open report file for TEXT DATA log for acc and gyro
	ST_reportAccGyro.open (ST_LOG_FILE_APP_ACC_GYRO, ios::out | ios::trunc);

	if (false == ST_reportAccGyro.is_open ())
	{
		ST_exitThread = true;
	}

	// Open report for max acceleration
	ST_reportMaxAcc.open (ST_LOG_FILE_APP_MAX_ACC, ios::out | ios::trunc);

	if (false == ST_reportMaxAcc.is_open ())
	{
		ST_exitThread = true;
	}

	// Enter critical section
	//sem_wait (ST_globalSemaphorePtr);

	// Exit critical section
	//sem_post (ST_globalSemaphorePtr);

    while (false == ST_exitThread)
    {
    	// Call services, check if someone wrote into shared memory
#if (defined APP_USE_SHAREDMEMS)
    	ST_GetDataFromSharedMemory ();
#endif // #if (defined APP_USE_SHAREDMEMS)

    	// Now check if someone prepared a memory pool for usage
#if (defined APP_USE_MEMORYPOOL)
    	memoryPoolId = IPC_MemoryPoolCheckForScheduled ();

    	if (memoryPoolId >= 0)
    	{
    		// Get the memory pool vector reference
    		memPool = IPC_MemoryPoolGetVectorReference (memoryPoolId);

    		// Save the data to a file log (the user must use MEMORY POOL to write)
    		ST_WriteAccGyroLog (memPool);

#if (defined APP_USE_PLOT_CAPABILITY)
    		// Prepare the data and call the function that draws the plot
    		ST_CallPlot (memPool);
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

    		// Free the resource
    		IPC_MemoryPoolUnLock (memoryPoolId);
    	}
#endif // #if (defined APP_USE_MEMORYPOOL)

    	// Wait 500 ms
    	usleep (500);
    }

    // Set signal for thread end to 1
    ST_ended = 1;

	return (void *)&ST_threadId;
}

static MAYBE_UNUSED tSInt ST_CallPlot (dataVectTy &dataVect)
{
	dataVectTy pts;
	tSInt cnt;
	tSInt vecSize;
	boost::tuple<float, float, float, float> tmpTuple;
	float tmpRx, tmpRy, tmpRz, tmpR;

	// Prepare the data
	// Get element
	vecSize = dataVect.size ();

	for (cnt = 0; cnt < vecSize; cnt++)
	{
		// Copy time
		boost::get<0>(tmpTuple) = boost::get<0>(dataVect[cnt]);

		// Get data
		tmpRx = boost::get<1>(dataVect[cnt]);
		tmpRy = boost::get<2>(dataVect[cnt]);
		tmpRz = boost::get<3>(dataVect[cnt]);

		// Calculate R
		tmpR = ST_CalculateRVectorAndAngles (tmpRx, tmpRy, tmpRz, NULL);

		// Put R in <1>
		boost::get<1>(tmpTuple) = tmpR;

		// Set <2> and <3> to 0 (not necessary)
		boost::get<2>(tmpTuple) = 0;
		boost::get<3>(tmpTuple) = 0;

		// Push tuple into vector
		pts.push_back (tmpTuple);
	}

	// Draw the plot
#if (defined APP_USE_PLOT_CAPABILITY)
	PLOT_Draw (pts);
#endif // #if (defined APP_USE_PLOT_CAPABILITY)

	return ERROR_CODE_NO_ERROR;
}

#if (defined APP_USE_SHAREDMEMS)
static tSInt ST_GetDataFromSharedMemory (void)
{
#if 0
	tU8 writeLen;
	std::stringstream textStream;
#endif // #if 0

	IPC_sharedMemoryElemTy *elemPtr;

#if 0
	// Clear stream content
	textStream.str (std::string());
	textStream.clear();
#endif // #if 0

	// Check if we have some data to dump
	elemPtr = IPC_SharedMemoryGetReaderDataPointer ();

	while ((IPC_sharedMemoryElemTy *)NULL != elemPtr)
	{
		if (elemPtr->len > 0)
		{
#if 0
			// Get header data
			textStream << elemPtr->writerId << ",";
			textStream << elemPtr->time << ",";
			textStream << elemPtr->len << ",";

			// Get data
			writeLen = elemPtr->len;

			for (int writeCnt = 0; writeCnt < writeLen; writeCnt++)
			{
				textStream << elemPtr->dataBfr[writeCnt].data << ",";
			}

			textStream << "\n";

			// Write data logs
			ST_WriteApplicationLog (textStream);
#endif // #if 0

			// Check specific logs
			if (APP_THREAD_HTS221_ID == elemPtr->writerId)
			{
				ST_WriteTemperatureHumidityLog (elemPtr);
			}
			else if (APP_THREAD_LSM6DS0_ID == elemPtr->writerId ||
					 APP_THREAD_LSM6DS3_ID == elemPtr->writerId)
			{
				// CHECKME: Uncomment this line if you would like to use SHARED MEMORIES for accelerometer/gyroscope
				ST_WriteAccGyroLog (elemPtr);
			}

			// Reset fields (not necessary, just for security)
			elemPtr->writerId = 0;
			elemPtr->time = 0;
			elemPtr->len = 0;
		}

		// Check if we have some more data to dump
		elemPtr = IPC_SharedMemoryGetReaderDataPointer ();
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined APP_USE_SHAREDMEMS)

static float ST_CalculateRVectorAndAngles (float accX, float accY, float accZ, ST_accDataTy *rAccVector)
{
	float r;

	// Calculate vector
	r = sqrt (pow (accX, 2) + pow (accY, 2) + pow (accZ, 2));

	if (NULL != rAccVector)
	{
		// Store R
		rAccVector->R = r;

		// Calculate angles
		rAccVector->Axr = acos (accX / r);
		rAccVector->Ayr = acos (accY / r);
		rAccVector->Azr = acos (accZ / r);
	}

	return r;
}

static tSInt ST_CalculateMaxAccelerationValueInsideWindow (float accX, float accY, float accZ)
{
	float r;
	float axr, ayr, azr;
//	std::stringstream textStream;

	// Calculate vector
	r = sqrt (pow (accX, 2) + pow (accY, 2) + pow (accZ, 2));

	if (r > ST_greatestAcc.R)
	{
		// Calculate angles
		axr = acos (accX / r);
		ayr = acos (accY / r);
		azr = acos (accZ / r);

		// Update greatest value
		ST_greatestAcc.R = r;
		ST_greatestAcc.Axr = axr;
		ST_greatestAcc.Ayr = ayr;
		ST_greatestAcc.Azr = azr;

#if 0
		// Open file for write
		ST_reportMaxAcc.open (ST_LOG_FILE_APP_MAX_ACC, ios::out | ios::trunc);

		if (false == ST_reportMaxAcc.is_open ())
		{
			return ERROR_CODE_GENERIC;
		}
		else
		{
		    textStream.str (std::string());
		    textStream.clear();

		    // Write acceleration max
		    textStream << "MAX: " << r << " g - ANGLES: " << axr << " - " << ayr << " - " << azr << " radiant";

			std::string tmpStr = textStream.str();
			ST_reportMaxAcc << tmpStr;

			// Close report file
			ST_reportMaxAcc.close ();
		}
#endif
	}

	return ERROR_CODE_NO_ERROR;
}

static tSInt ST_OutputMaxAccelerationValueInsideWindow (void)
{
	std::stringstream textStream;

	// Open file for write
	ST_reportMaxAcc.open (ST_LOG_FILE_APP_MAX_ACC, ios::out | ios::trunc);

	if (false == ST_reportMaxAcc.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		textStream.str (std::string());
		textStream.clear();

		// Write acceleration max
		textStream << "MAX: " << ST_greatestAcc.R << \
				      " g - ANGLES: " << ST_greatestAcc.Axr << " - " << ST_greatestAcc.Ayr << " - " << ST_greatestAcc.Azr << " radiant";

		std::string tmpStr = textStream.str();
		ST_reportMaxAcc << tmpStr;

		// Close report file
		ST_reportMaxAcc.close ();
	}

	return ERROR_CODE_NO_ERROR;
}


static MAYBE_UNUSED tSInt ST_WriteApplicationLog (std::stringstream &textStream)
{
	// Open report file
	ST_reportFile.open (ST_LOG_FILE_APPLICATION, ios::out | ios::app);

	if (false == ST_reportFile.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		std::string tmpStr = textStream.str();
		ST_reportFile << tmpStr;

		// Close report file
		ST_reportFile.close ();
	}

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt ST_WriteAccGyroLog (IPC_sharedMemoryElemTy *elemPtr)
{
	std::stringstream textStream;
	tSInt slot = 0;
	tSInt remainingData;

	// Open file for writing
	ST_reportAccGyro.open (ST_LOG_FILE_APP_ACC_GYRO, ios::out | ios::app);

	if (false == ST_reportAccGyro.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear max information for acceleration
		ST_greatestAcc.R = 0;

		// Write result to CSV file, format:
		//    <time stamp> - <acceleration X Y Z> - <gyroscope X Y Z>
		// or, if only acceleration is monitored:
		//    <time stamp> - <acceleration X Y Z>
	    textStream.str (std::string());
	    textStream.clear();

	    remainingData = elemPtr->len;
	    while (remainingData > 0)
	    {
#if 0
			textStream << elemPtr->time << "\t[Acc(X,Y,Z),Gyro(X,Y,Z)]\t" \
					   << elemPtr->dataBfr[slot++].data << "\t" \
					   << elemPtr->dataBfr[slot++].data << "\t" \
					   << elemPtr->dataBfr[slot++].data << "\t" \
					   << elemPtr->dataBfr[slot++].data << "\t" \
					   << elemPtr->dataBfr[slot++].data << "\t" \
					   << elemPtr->dataBfr[slot++].data << "\n";
#else
			textStream << elemPtr->time << "\t" << elemPtr->dataBfr[(slot + 0)].data << "\t" \
					                            << elemPtr->dataBfr[(slot + 1)].data << "\t" \
												<< elemPtr->dataBfr[(slot + 2)].data << "\n";
#endif // #if 0

			// Check if we have a new max
			ST_CalculateMaxAccelerationValueInsideWindow (elemPtr->dataBfr[(slot + 0)].data,
				 	 	 	 	 	 	 	 	 	      elemPtr->dataBfr[(slot + 1)].data,
														  elemPtr->dataBfr[(slot + 2)].data);

			remainingData -= 6;
	    }

		std::string tmpStr = textStream.str();
		ST_reportAccGyro << tmpStr;

		// Close report file
		ST_reportAccGyro.close ();

		// Write max value on file
		ST_OutputMaxAccelerationValueInsideWindow ();
	}

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED tSInt ST_WriteAccGyroLog (dataVectTy &dataVect)
{
	std::stringstream textStream;
	boost::tuple<float, float, float, float> dataTuple;

	// Open file for writing
	if (false == ST_accGyroLogFileTruncate)
	{
		ST_reportAccGyro.open (ST_LOG_FILE_APP_ACC_GYRO, ios::out | ios::app);
	}
	else
	{
		ST_reportAccGyro.open (ST_LOG_FILE_APP_ACC_GYRO, ios::out | ios::trunc);
	}

	if (false == ST_reportAccGyro.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear max information for acceleration
		ST_greatestAcc.R = 0;

		// Write result to CSV file, format:
		//    <time stamp> - <acceleration X Y Z> - <gyroscope X Y Z>
		// or, if only acceleration is monitored:
		//    <time stamp> - <acceleration X Y Z>
	    textStream.str (std::string());
	    textStream.clear();

	    for (tUInt cnt = 0; cnt < dataVect.size(); cnt++)
	    {
	        dataTuple = dataVect.at (cnt);

			textStream << dataTuple.get<0>() << "\t" << dataTuple.get<1>() << "\t" \
					                                 << dataTuple.get<2>() << "\t" \
											    	 << dataTuple.get<3>() << "\n";

			// Check if we have a new max
			ST_CalculateMaxAccelerationValueInsideWindow (dataTuple.get<1>(),
														  dataTuple.get<2>(),
														  dataTuple.get<3>());
	    }

		std::string tmpStr = textStream.str();
		ST_reportAccGyro << tmpStr;

		// Close report file
		ST_reportAccGyro.close ();

		// Write max value on file
		ST_OutputMaxAccelerationValueInsideWindow ();

		// Evaluate length for Acc/Gyro report file: if the file is too big then copy it to a new, numbered one
		ST_accGyroLogFileTruncate = ST_FileBackup (ST_LOG_FILE_APP_ACC_GYRO_NOEXT, ST_LOG_FILE_MAX_DIM_MB);
	}

	return ERROR_CODE_NO_ERROR;
}

static MAYBE_UNUSED long int ST_CalculateDeltaTime (struct timeval *starttime, struct timeval *finishtime)
{
	long int msec;

	msec = (finishtime->tv_sec - starttime->tv_sec) * 1000;
	msec += (finishtime->tv_usec - starttime->tv_usec) / 1000;

	return msec;
}

static tBool ST_FileBackup (const string& fileNameStr, std::size_t dimension)
{
	tBool res;
	struct timeval curTimeValue;
	long int  deltaTime;
	string oldFileName = fileNameStr + ".txt";

	// Open input file for reading
	std::ifstream fileToBackup (oldFileName, std::ios::in | std::ios::binary);

	// Get pointer to associated buffer object
	std::filebuf* pbuf = fileToBackup.rdbuf ();

	// Get file size using buffer's members
	std::size_t fileSize = pbuf->pubseekoff (0, fileToBackup.end, fileToBackup.in);

	// We do the backup only if the dimension is greater than the requested one
	if (fileSize >= dimension)
	{
		// Build output file name

		// Calculate delta time since the start of the process
		gettimeofday (&curTimeValue, NULL);

		deltaTime = ST_CalculateDeltaTime (&ST_startTimeValue, &curTimeValue);

		string newFileName = fileNameStr + "_" + boost::lexical_cast<string>(ST_sessionNumber) + "_" +
							 boost::lexical_cast<string>(deltaTime) + ".txt";

		// Open output file
	    std::ofstream backupFile (newFileName, std::ios::binary | std::ios::out | std::ios::trunc);

	    // Set position at the begin
		pbuf->pubseekoff (0, fileToBackup.beg, fileToBackup.in);

	    // Read data
	    backupFile << fileToBackup.rdbuf ();

	    fileToBackup.close (); // Not necessary to explicit close, anyway better to do it
	    backupFile.close (); // Not necessary to explicit close, anyway better to do it

		res = true;
	}
	else
	{
		fileToBackup.close (); // Not necessary to explicit close, anyway better to do it

		res = false;
	}

	return res;
}

static tSInt ST_ManageSessionNumber (void)
{
	string line;
	std::fstream fs;
	tSInt sessionNumber = 0;

	// Open file for reading
	fs.open (ST_FILE_SESSION_LOG, std::fstream::in);

    if (false == fs.is_open ())
    {
    	// File open failed, so use an arbitrary string, we use the 0 that will be incremented to 1 below
    	line = "0\n";
    }
    else
    {
    	// read data
    	getline (fs, line);
    }

    // Convert to number then increment
    try
    {
    	sessionNumber = boost::lexical_cast<tSInt>(line);

    	sessionNumber++;
    }
    catch (const boost::bad_lexical_cast &exc) // Conversion failed, exception thrown by lexical_cast and caught
    {
    	sessionNumber = 1; // Call 'exc.what ()' for the error details if needed
    }

    // Close file
    fs.close();

    // Open file for writing
    fs.open (ST_FILE_SESSION_LOG, std::fstream::out | std::fstream::trunc);

    if (false == fs.is_open ())
    {
    	// We should never fail here but if it happens we have our number, next time
    	// we probably have to restart from 0
    }
    else
    {
		// Convert number to string and write it to file
		fs << boost::lexical_cast<string>(sessionNumber) << endl;

		// Close file
		fs.close ();
    }

    return sessionNumber;
}

static MAYBE_UNUSED tSInt ST_WriteTemperatureHumidityLog (IPC_sharedMemoryElemTy *elemPtr)
{
	std::stringstream textStream;

	// Open LOG file for writing
	ST_reportTemperatureHumidity.open (ST_LOG_FILE_APP_TEMP_HUMIDITY, ios::out | ios::app);

	if (false == ST_reportTemperatureHumidity.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear string
	    textStream.str (std::string());
	    textStream.clear();

	    // Build string
		textStream << "Current temperature: " << elemPtr->temperature << ", " << "Current humidity: " << elemPtr->humidity << "\n";

		std::string tmpStr = textStream.str();

		ST_reportTemperatureHumidity << tmpStr;

		// Close report file
		ST_reportTemperatureHumidity.close ();
	}

	// Open LAST READ file for writing
	ST_reportTemperatureHumidityLastRead.open (ST_LOG_FILE_APP_TEMP_HUMIDITY_LR, ios::out | ios::trunc);

	if (false == ST_reportTemperatureHumidityLastRead.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear string
	    textStream.str (std::string());
	    textStream.clear();

	    // Build string
		textStream << "Current temperature: " << elemPtr->temperature << ", " << "Current humidity: " << elemPtr->humidity << "\n";

		std::string tmpStr = textStream.str();
		ST_reportTemperatureHumidityLastRead << tmpStr;

		// Close report file
		ST_reportTemperatureHumidityLastRead.close ();
	}

	return ERROR_CODE_NO_ERROR;
}

// End of file
