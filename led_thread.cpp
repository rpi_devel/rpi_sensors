/*
 * led_thread.cpp
 *
 *  Created on: Dec 2, 2014
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include "target_config.h"

#include "gpio_common.h"
#include "gpio_mem.h"
#include "gpio_dev.h"

static pthread_t id;

static bool LED_exitThread = false;

void LED_ExitThread (bool exitThread)
{
	if (true == exitThread)
	{
		LED_exitThread = true;
	}
}

volatile sig_atomic_t LED_ended = 0;

int LED_SignalHandler (void)
{
	return LED_ended;
}

void* LED_Thread (void *arg)
{
	GPIODEV_valEnumTy val0, val1;
	int cnt;
	char gpio[] = {GPIO_23, GPIO_24};
	int gpioNum = sizeof (gpio);

	// Be sure to stay here
	LED_exitThread = false;
	LED_ended = 0;

	// Get thread ID to return back at the end
	id = pthread_self ();

///
// USe peripherals GPIOs
//
    // Call GPIO open function
    for (cnt = 0; cnt < gpioNum; cnt++)
    {
		if (0 != gpio_dev_open (gpio[cnt]))
		{
			printf ("Failed to open GPIO%d\n", gpio[cnt]);
		}

		if (0 != gpio_dev_set_direction (gpio[cnt], GPIODEV_DIRECTION_OUT))
		{
			printf ("Failed to set GPIO%d as %d\n", gpio[cnt], (int)GPIODEV_DIRECTION_OUT);
		}
    }

    while (false == LED_exitThread)
    {
    	// Set GPIOs 0 = HIGH, 1 = LOW
    	gpio_dev_set_value (gpio[0], GPIODEV_VALUE_HIGH);
    	gpio_dev_set_value (gpio[1], GPIODEV_VALUE_LOW);

    	// Check if we set value correctly
    	val0 = gpio_dev_get_value (gpio[0]);
    	val1 = gpio_dev_get_value (gpio[1]);

    	if (GPIODEV_VALUE_HIGH != val0)
    	{
    		printf ("Failed to set GPIO%d to %d\n", gpio[0],(int)val0);
    	}

    	if (GPIODEV_VALUE_LOW != val1)
    	{
    		printf ("Failed to set GPIO%d to %d\n", gpio[1],(int)val1);
    	}

    	// Wait 1 ms
    	sleep (1);

    	// Set GPIOs 0 = LOW, 1 = HIGH
    	gpio_dev_set_value (gpio[0], GPIODEV_VALUE_LOW);
    	gpio_dev_set_value (gpio[1], GPIODEV_VALUE_HIGH);

    	// Check if we set value correctly
    	val0 = gpio_dev_get_value (gpio[0]);
    	val1 = gpio_dev_get_value (gpio[1]);

    	if (GPIODEV_VALUE_LOW != val0)
    	{
    		printf ("Failed to set GPIO%d to %d\n", gpio[0],(int)val0);
    	}

    	if (GPIODEV_VALUE_HIGH != val1)
    	{
    		printf ("Failed to set GPIO%d to %d\n", gpio[1],(int)val1);
    	}

    	// Wait 1 s
    	sleep (1);
    }

    // Call GPIO close function
    for (cnt = 0; cnt < gpioNum; cnt++)
    {
		if (0 != gpio_dev_close (gpio[cnt]))
		{
			printf ("Failed to close GPIO%d\n", gpio[cnt]);
		}
    }

    // Signal thread ended
    LED_ended = 1;

    return (void *)&id;
}

// End of file
