/*
 * plot_data.cpp
 *
 *  Created on: May 24, 2015
 *      Author: alberto
 */

#include "target_config.h"

#if (defined APP_USE_PLOT_CAPABILITY)

#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>     // std::cout
#include <string>

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "ipc.h"

#include "gnuplot-iostream.h"

using namespace std;

static tSInt PLOT_fileIncrementalCounter = 0;
static std::string PLOT_scale;
static std::string PLOT_yTicks;

void PLOT_Init (void)
{
	PLOT_scale = "set yrange [-2:2]\n";

	PLOT_yTicks = "set ytics -2,1,2\n";

	PLOT_fileIncrementalCounter = 0;
}

void PLOT_setScale (tSInt scale)
{
	PLOT_scale = "set yrange [-" + boost::lexical_cast<string>(scale) + ":" + boost::lexical_cast<string>(scale) + "]\n";

	PLOT_yTicks = "set ytics -" + boost::lexical_cast<string>(scale) + ",1," + boost::lexical_cast<string>(scale) + "\n";
}

void PLOT_Draw (dataVectTy &pts)
{
	Gnuplot gp;

	// Set gnuplot parameters and call the program
	std::string tmp = "set output '/webroot/images/gnuplot_output.png'\n";

	// Set y range and y tickmarks
	gp << PLOT_scale;
	gp << PLOT_yTicks;

	// Set output to be png
	gp << "set terminal png\n";

	// "set output '/webroot/images/gnuplot_output.png'\n";
	gp << tmp;

	// Set plot type
	gp << "plot '-' with lines\n";

	// Call the gnuplot program
	gp.send1d(pts);

	PLOT_fileIncrementalCounter++;
}

void PLOT_Draw (void)
{
#if 1
	Gnuplot gp;

	gp << "set terminal png\n";

	std::vector<double> y_pts;

	for(int i=0; i<1000; i++)
	{
	    double y = (i/500.0-1) * (i/500.0-1);
	    y_pts.push_back(y);
	}

	gp << "set ytics -16,1,16\n";
	gp << "set yrange [-16:16]\n";
	gp << "set output 'my_graph_1.png'\n";
	gp << "plot '-' with lines\n";
	gp.send1d(y_pts);
#else
    Gnuplot gp;

    // Gnuplot vectors (i.e. arrows) require four columns: (x,y,dx,dy)
    std::vector<boost::tuple<double, double, double, double> > pts_A;

    // You can also use a separate container for each column, like so:
    std::vector<double> pts_B_x;
    std::vector<double> pts_B_y;
    std::vector<double> pts_B_dx;
    std::vector<double> pts_B_dy;

    // Make the data
    for(double alpha = 0; alpha < 1; alpha += 1.0 / 24.0)
    {
        double theta = alpha * 2.0 * 3.14159;

        pts_A.push_back (boost::make_tuple (cos (theta), sin (theta), -cos (theta) * 0.1, -sin (theta) * 0.1));

        pts_B_x.push_back (cos (theta) * 0.8);
        pts_B_y.push_back (sin (theta) * 0.8);
        pts_B_dx.push_back (sin (theta) * 0.1);
        pts_B_dy.push_back (-cos (theta) * 0.1);
    }

    // Set terminal to PNG
    gp << "set term png\n";
    gp << "set output 'output.png'\n";

    // Each line shall terminate with '\n'
    gp << "set xrange [-2:2]\nset yrange [-2:2]\n";

    // '-' means read from stdin
    gp << "plot '-' with vectors title 'pts_A', " << "'-' with vectors title 'pts_B'\n";

    // The send1d() function sends data to gnuplot's stdin
    gp.send1d(pts_A);
    gp.send1d(boost::make_tuple(pts_B_x, pts_B_y, pts_B_dx, pts_B_dy));
#endif
}

#else

void PLOT_Init (void)
{

}

void PLOT_Draw (void)
{

}

#endif // #if (defined APP_USE_PLOT_CAPABILITY)

// End of file
