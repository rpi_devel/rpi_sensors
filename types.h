/*
 * types.h
 *
 *  Created on: Mar 21, 2015
 *      Author: alberto
 */

#ifndef TYPES_H_
#define TYPES_H_

///
// Typedefs
///
typedef unsigned char           tBool;

typedef unsigned char           tU8;
typedef signed char         	tS8;
typedef char             		tChar;
typedef unsigned char           tUChar;
typedef signed char             tSChar;

typedef unsigned short          tU16;
typedef signed short            tS16;
typedef unsigned short          tUShort;
typedef signed short            tShort;

typedef unsigned int            tU32;
typedef int                     tS32;
typedef unsigned int            tUInt;
typedef signed int              tSInt;
typedef float                   tFloat;

typedef unsigned long long      tU64;
typedef long long               tS64;
typedef unsigned long           tULong;
typedef long                    tSLong;
typedef double                  tDouble;

typedef long double             tSLDouble;

typedef tU8*                    tPU8;
typedef tU16*                   tPU16;
typedef tU32*                   tPU32;
typedef char*                   tPChar;


#endif // TYPES_H_

// End of file

