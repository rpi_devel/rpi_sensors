/*
 * spi_thread.cpp
 *
 *  Created on: Jan 31, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include <bcm2835.h>

#include "target_config.h"

#include "spi_thread.h"

#if (defined USE_SPI_DEV)

static pthread_t id;

static bool SPI_exitThread = false;

volatile sig_atomic_t SPI_ended = 0;

int SPI_SignalHandler (void)
{
	return SPI_ended;
}

void SPI_ExitThread (bool exitThread)
{
	if (true == exitThread)
	{
		SPI_exitThread = true;
	}
}

void* SPI_Thread (void *arg)
{
	bool res;
	char mosi[] = {0xAA, 0x55, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x55, 0xAA};
	char miso[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	unsigned int cnt;

	// Be sure to stay here
	SPI_exitThread = false;
	SPI_ended = 0;

	// Get thread ID to return back at the end
	id = pthread_self ();

///
// USe SPI
//
	res = bcm2835_init ();

	if (false == res)
    {
		printf ("Failed to init SPI\n");

		return NULL;
    }

    while (false == SPI_exitThread)
    {
    	// Set GPIO in alternate mode so they can be used for SPI
    	bcm2835_spi_begin ();

    	// Set transfer mode
    	bcm2835_spi_setBitOrder (BCM2835_SPI_BIT_ORDER_MSBFIRST);   // Default
    	bcm2835_spi_setDataMode (BCM2835_SPI_MODE0);                // Default
    	bcm2835_spi_setClockDivider (BCM2835_SPI_CLOCK_DIVIDER_64); // 4MHz
    	bcm2835_spi_chipSelect (BCM2835_SPI_CS0);                   // Default
    	bcm2835_spi_setChipSelectPolarity (BCM2835_SPI_CS0, LOW);   // Default

    	// Send and read data
    	bcm2835_spi_transfernb (mosi, miso, sizeof (mosi));

    	// Print data read back
    	printf ("Read from SPI:\n");

    	for (cnt = 0; cnt < sizeof (miso); cnt++)
    	{
    		printf ("%04x\n", miso[cnt]);
    	}

    	// Reconfigure GPIOs for GPIO usage
    	bcm2835_spi_end ();

    	// Wait 1s before to start another transmission
    	sleep (1);
    }

	// Thread ended
	SPI_ended = 1;

	return (void *)&id;
}

#endif // #if (defined USE_SPI_DEV)

// End of file




