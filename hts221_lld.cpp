/*
 * hts221_lld.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: alberto
 */

#include <string>       // std::string
#include <iostream>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#include <bcm2835.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "system_defines.h"

#include "ipc.h"

#include "i2c_lld.h"

#include "hts221_lld.h"

using namespace std;

///
// Define to directly write data to file
///
//#define HTS221_WRITE_TO_FILE
#undef HTS221_WRITE_TO_FILE

#define HTS221_I2C_ADD						0x5F

#define HTS221_DEVICE_ID					((tU8)0xBC)

#define HTS221_DATA_BUFFER_SIZE 			0x1000

// Device registers
#define HTS221_REG_WHO_AM_I					0x0F
#define HTS221_REG_AV_CONF					0x10
#define HTS221_REG_CTRL_REG1				0x20
#define HTS221_REG_CTRL_REG2				0x21
#define HTS221_REG_CTRL_REG3				0x22
#define HTS221_REG_STATUS_REG				0x27
#define HTS221_REG_HUMIDITY_OUT_L			0x28
#define HTS221_REG_HUMIDITY_OUT_H			0x29
#define HTS221_REG_TEMP_OUT_L				0x2A
#define HTS221_REG_TEMP_OUT_H				0x2B
#define HTS221_REG_H0_rH_x2					0x30
#define HTS221_REG_H1_rH_x2					0x31
#define HTS221_REG_T0_degC_x8				0x32
#define HTS221_REG_T1_degC_x8				0x33
#define HTS221_REG_T1_T0_msb				0x35
#define HTS221_REG_H0_T0_OUT_LSB			0x36
#define HTS221_REG_H0_T0_OUT_MSB			0x37
#define HTS221_REG_H1_T0_OUT_LSB			0x3A
#define HTS221_REG_H1_T0_OUT_MSB			0x3B
#define HTS221_REG_T0_OUT_LSB				0x3C
#define HTS221_REG_T0_OUT_MSB				0x3D
#define HTS221_REG_T1_OUT_LSB				0x3E
#define HTS221_REG_T1_OUT_MSB				0x3F

#define HTS221_REG_CTRL_REG1_ENABLE_BIT		0x80
#define HTS221_REG_CTRL_REG1_BDU_BIT		0x04
#define HTS221_REG_CTRL_REG1_ONE_SHOT_VAL	0x00
#define HTS221_REG_CTRL_REG1_1HZ_VAL		0x01
#define HTS221_REG_CTRL_REG1_7HZ_VAL		0x02
#define HTS221_REG_CTRL_REG1_12_5HZ_VAL		0x03

#define HTS221_REG_CTRL_REG2_BOOT_BIT		0x80
#define HTS221_REG_CTRL_REG2_ONE_SHOT_BIT	0x01

#define HTS221_REG_STATUS_REG_T_RDY_BIT		0x01
#define HTS221_REG_STATUS_REG_H_RDY_BIT		0x02
#define HTS221_REG_STATUS_REG_H_RDY_MASK	(HTS221_REG_STATUS_REG_T_RDY_BIT | HTS221_REG_STATUS_REG_H_RDY_BIT)

// Fields
#define HTS221_AUTO_INCREMENT_BIT			((tU8)0x80)

#define HTS221_MODE_POWERDOWN               ((tU8)0x00)
#define HTS221_MODE_ACTIVE                  ((tU8)0x80)

#define HTS221_MODE_MASK                    ((tU8)0x80)

// Data resolution
#define HTS221_HUM_DECIMAL_DIGITS           2
#define HTS221_TEMP_DECIMAL_DIGITS          2

typedef struct
{
	gap8 (15);

	tU8 WHO_AM_I;
	tU8 AV_CONF;

	gap8 (12);

	tU8 CTRL_REG1;
	tU8 CTRL_REG2;
	tU8 CTRL_REG3;

	gap8 (4);

	tU8 STATUS_REG;
	tU8 HUMIDITY_OUT_L;
	tU8 HUMIDITY_OUT_H;
	tU8 TEMP_OUT_L;
	tU8 TEMP_OUT_H;

	gap8 (4);

	tU8 H0_rH_x2;
	tU8 H1_rH_x2;
	tU8 T0_degC_x8;
	tU8 T1_degC_x8;

	gap (1);

	tU8 T1_T0_msb;

	tU8 H0_T0_OUT_LSB;
	tU8 H0_T0_OUT_MSB;

	gap (2);

	tU8 H1_T0_OUT_LSB;
	tU8 H1_T0_OUT_MSB;

	tU8 T0_OUT_LSB;
	tU8 T0_OUT_MSB;

	tU8 T1_OUT_LSB;
	tU8 T1_OUT_MSB;
} HTS221_registersMappingTy;

typedef struct
{
    pointTy calibrationHumidity1;
    pointTy calibrationHumidity2;
    pointTy calibrationTemperature1;
    pointTy calibrationTemperature2;
} HTS221_calibrationRegTy;

typedef struct
{
	tS32 timeStampUs;
	tS16 temperature;
	tS16 humidity;
} HTS221_dataReadTy;

volatile static HTS221_registersMappingTy HTS221_registersMapping;
static HTS221_calibrationRegTy HTS221_calibrationReg;
static HTS221_dataReadTy HTS221_dataRead[HTS221_DATA_BUFFER_SIZE];
static struct timeval HTS221_startTimeValue;

// Temperature in degree for calibration
static float HTS221_T0_degC, HTS221_T1_degC;

// Output temperature value for calibration
static tS16 HTS221_T0_out, HTS221_T1_out;

// Humidity for calibration
static float HTS221_H0_rh, HTS221_H1_rh;

// Output Humidity value for calibration
static tS16 HTS221_H0_T0_out, HTS221_H1_T0_out;

#if (defined HTS221_WRITE_TO_FILE)
	static ofstream HTS221_reportFile;
#endif // #if (defined HTS221_WRITE_TO_FILE)

static void HTS221_PowerOff (void);
static void HTS221_PowerOn (void);
static void HTS221_Calibration (void);
static tS32 HTS221_ReadControlRegisters (void);
static tS32 HTS221_ReadConfigurationRegisters (void);
static tS32 HTS221_ReadStatusRegister (void);
static tS32 HTS221_ReadTempRegister (void);
static tS32 HTS221_ReadHumidityRegister (void);
static tS32 HTS221_WriteControlRegisters (void);
static tS32 HTS221_StartMeasurement (tBool automaticMode);
static float HTS221_PerformTemperatureCalculation (void);
static float HTS221_PerformHumidityCalculation (void);
static tS32 HTS221_ReadId (tPU8 idVal);
static tSInt HTS221_WriteDataToFile (float temperature, float humidity);

tS32 HTS221_Open (void)
{
	// Start I2C mode
	bcm2835_i2c_begin ();

	// Setup I2C
	bcm2835_i2c_setSlaveAddress (HTS221_I2C_ADD);
	bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_626);

	return ERROR_CODE_NO_ERROR;
}

tS32 HTS221_Close (void)
{
	// Close I2C mode
	bcm2835_i2c_end ();

	return ERROR_CODE_NO_ERROR;
}

tS32 HTS221_Init (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	tU8 deviceId;

	// Initialize globals
	memset (&HTS221_dataRead, 0x00, sizeof (HTS221_dataRead));
	memset (&HTS221_calibrationReg, 0x00, sizeof (HTS221_calibrationRegTy));

	// Get start point for delta time
	gettimeofday (&HTS221_startTimeValue, NULL);

	// Open HTS221 I2C access
	HTS221_Open ();

	// Read ID to identify the proper device
	res = HTS221_ReadId (&deviceId);

	if (ERROR_CODE_NO_ERROR != res || HTS221_DEVICE_ID != deviceId)
	{
		// Close HTS221 I2C access
		HTS221_Close ();

		return ERROR_CODE_GENERIC;
	}

#if (defined HTS221_WRITE_TO_FILE)
	// Open report file
	HTS221_reportFile.open ("hts221_report.txt", ios::out | ios::trunc);

	if (false == HTS221_reportFile.is_open ())
	{
		// Close HTS221 I2C access
		HTS221_Close ();

		return ERROR_CODE_GENERIC;
	}

	// Write file first line
	HTS221_reportFile << "HTS221 DATA REPORT" << '\n';
#endif // #if (defined HTS221_WRITE_TO_FILE)

	// Write control registers
	HTS221_WriteControlRegisters ();

	// Check if the write was fine
	HTS221_ReadControlRegisters ();

	// Read configuration register (just for test)
	HTS221_ReadConfigurationRegisters ();

	// Read calibration registers
	HTS221_Calibration ();

	// Start measurements in automatic mode
	HTS221_StartMeasurement (true);

#if (defined HTS221_WRITE_TO_FILE)
	// Close report file
	HTS221_reportFile.close ();
#endif // #if (defined HTS221_WRITE_TO_FILE)

	// Close HTS221 I2C access
	HTS221_Close ();

	return res;
}

tS32 HTS221_ReadTemperatureAndHumiditySequence (void)
{
	float outTemperature;
	float outHumidity;
	tS32 res = ERROR_CODE_NO_ERROR;

	// Open HTS221 I2C access
	HTS221_Open ();

	// Wait the actual value to be read:
	// - RD: STATUS_REG
	//       - b0: temperature ready
	//       - b1: humidity ready
	HTS221_ReadStatusRegister ();

	if (!(HTS221_registersMapping.STATUS_REG & HTS221_REG_STATUS_REG_T_RDY_BIT) ||
		!(HTS221_registersMapping.STATUS_REG & HTS221_REG_STATUS_REG_H_RDY_BIT))
	{
		// We have no data so we return back to the caller without blocking other processes
		printf ( "HTS221 STATUS REGISTER not ready value : 0x%x\n", HTS221_registersMapping.STATUS_REG);
	}

	// Read temperature
	HTS221_ReadTempRegister ();

	// Read humidity
	HTS221_ReadHumidityRegister ();

	// Perform actual temperature calculation
	outTemperature = HTS221_PerformTemperatureCalculation ();

	// Perform actual humidity calculation
	outHumidity = HTS221_PerformHumidityCalculation ();

	// Output acquired data
	HTS221_WriteDataToFile (outTemperature, outHumidity);

	// Close HTS221 I2C access
	HTS221_Close ();

	return res;
}

tS32 HTS221_ReadHumidityAndTemperature (tBool readTemperature, tBool readHumidity)
{
	tS32 res = ERROR_CODE_NO_ERROR;

	return res;
}

static tS32 HTS221_ReadControlRegisters (void)
{
	tS32 readRes;
	tChar rdCtrlBfr[5] = {0x00, 0x00, 0x00, 0x00, 0x00};

	// Read CTRL registers
	readRes = I2C_GetData (HTS221_REG_CTRL_REG1, 1, &rdCtrlBfr[0]);
	printf ( "HTS221 CTRL1 REGISTER read result = %d : 0x%x\n", readRes, rdCtrlBfr[0]);
	HTS221_registersMapping.CTRL_REG1 = rdCtrlBfr[0];

	readRes = I2C_GetData (HTS221_REG_CTRL_REG2, 1, &rdCtrlBfr[0]);
	printf ( "HTS221 CTRL2 REGISTER read result = %d : 0x%x\n", readRes, rdCtrlBfr[0]);
	HTS221_registersMapping.CTRL_REG2 = rdCtrlBfr[0];

	readRes = I2C_GetData (HTS221_REG_CTRL_REG3, 1, &rdCtrlBfr[0]);
	printf ( "HTS221 CTRL3 REGISTER read result = %d : 0x%x\n", readRes, rdCtrlBfr[0]);
	HTS221_registersMapping.CTRL_REG3 = rdCtrlBfr[0];

	return readRes;
}

static tS32 HTS221_ReadConfigurationRegisters (void)
{
	tS32 res;
	tChar rdConfBfr [3] = {0x00, 0x00, 0x00};

	// Read CONF register
	res = I2C_GetData (HTS221_REG_AV_CONF, 1, rdConfBfr);

	HTS221_registersMapping.AV_CONF = rdConfBfr[0];

	return res;
}

static tS32 HTS221_ReadStatusRegister (void)
{
	tS32 readRes;
	tChar rdStatusBfr[3] = {0x00, 0x00, 0x00};

	// Read CONF register
	readRes = I2C_GetData (HTS221_REG_STATUS_REG, 1, rdStatusBfr);

	if (rdStatusBfr[0] ^ HTS221_REG_STATUS_REG_H_RDY_MASK)
	{
		printf ( "HTS221 STATUS REGISTER read result = %d : 0x%x\n", readRes, rdStatusBfr[0]);
	}

	HTS221_registersMapping.STATUS_REG = rdStatusBfr[0];

	return readRes;
}

static tS32 HTS221_ReadTempRegister (void)
{
	tS32 readTempRes;
	tChar rdTempBfr[3];

	// Read temperature data
	readTempRes = I2C_GetData (HTS221_REG_TEMP_OUT_L, 1, &rdTempBfr[0]);
	readTempRes = I2C_GetData (HTS221_REG_TEMP_OUT_H, 1, &rdTempBfr[1]);

	HTS221_registersMapping.TEMP_OUT_L = rdTempBfr[0];
	HTS221_registersMapping.TEMP_OUT_H = rdTempBfr[1];

	return readTempRes;
}

static tS32 HTS221_ReadHumidityRegister (void)
{
	tS32 readTempRes;
	tChar rdTempBfr[3];

	// Read humidity data
	readTempRes = I2C_GetData (HTS221_REG_HUMIDITY_OUT_L, 1, &rdTempBfr[0]);
	readTempRes = I2C_GetData (HTS221_REG_HUMIDITY_OUT_H, 1, &rdTempBfr[1]);

	HTS221_registersMapping.HUMIDITY_OUT_L = rdTempBfr[0];
	HTS221_registersMapping.HUMIDITY_OUT_H = rdTempBfr[1];

	return readTempRes;
}

static tS32 HTS221_WriteControlRegisters (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	tChar wrBfr[10] = {0x00};
	tChar rdBfr[10] = {0x00};

	// Turn OFF device (Write CNTRL reg 1)
	HTS221_PowerOff ();

	// Wait here
	usleep (100);

	// Turn ON device (Write CNTRL reg 1)
	HTS221_PowerOn ();

	// Wait here
	usleep (100);

    // Configure the device
	HTS221_registersMapping.CTRL_REG1 = (HTS221_REG_CTRL_REG1_ENABLE_BIT | HTS221_REG_CTRL_REG1_BDU_BIT | HTS221_REG_CTRL_REG1_12_5HZ_VAL);
	wrBfr[0] = HTS221_registersMapping.CTRL_REG1;
	I2C_WriteData (HTS221_REG_CTRL_REG1, 1, wrBfr);

	// Write CNTRL reg 2 (load calibration registers)
	HTS221_registersMapping.CTRL_REG2 |= HTS221_REG_CTRL_REG2_BOOT_BIT;
	wrBfr[0] = HTS221_registersMapping.CTRL_REG2;
	I2C_WriteData (HTS221_REG_CTRL_REG2, 1, wrBfr);

	// Wait boot to complete
	I2C_GetData (HTS221_REG_CTRL_REG2, 1, &rdBfr[0]);

	while (rdBfr[0] & HTS221_REG_CTRL_REG2_BOOT_BIT)
	{
		I2C_GetData (HTS221_REG_CTRL_REG2, 1, &rdBfr[0]);
	}

	// Write CNTRL reg 3
	//wrBfr[0] = HTS221_registersMapping.CTRL_REG3;
	//I2C_WriteData (HTS221_REG_CTRL_REG3, 1, wrBfr);

	return res;
}

static tS32 HTS221_StartMeasurement (tBool automaticMode)
{
	tS32 res;
	tChar wrBfr[5] = {0x00, 0x00, 0x00, 0x00, 0x00};

	// Write CNTRL reg 2
	if (true == automaticMode)
	{
		HTS221_registersMapping.CTRL_REG2 |= HTS221_REG_CTRL_REG2_ONE_SHOT_BIT;
		wrBfr[0] = HTS221_registersMapping.CTRL_REG2;
		I2C_WriteData (HTS221_REG_CTRL_REG2, 1, wrBfr);
	}

	return res = ERROR_CODE_NO_ERROR;
}

static float HTS221_PerformTemperatureCalculation (void)
{
	float temperature;
    tS16 tempOut, temperature_t;
    float tempDegC;

    tempOut = ((((tS16)HTS221_registersMapping.TEMP_OUT_H) << 8) | (tS16)HTS221_registersMapping.TEMP_OUT_L );

    tempDegC = ((float)(tempOut - HTS221_T0_out)) / (HTS221_T1_out - HTS221_T0_out) * (HTS221_T1_degC - HTS221_T0_degC) + HTS221_T0_degC;

    temperature_t = (int16_t)(tempDegC * pow (10, HTS221_TEMP_DECIMAL_DIGITS));

    temperature = ((float)temperature_t) /pow (10, HTS221_TEMP_DECIMAL_DIGITS);

	return temperature;
}

static float HTS221_PerformHumidityCalculation (void)
{
	float humidity;
    tS16 humidityOut, humidity_t;
    float H_rh;

    humidityOut = ((((tS16)HTS221_registersMapping.HUMIDITY_OUT_H) << 8) | (tS16)HTS221_registersMapping.HUMIDITY_OUT_L);

    H_rh = ((float)(humidityOut - HTS221_H0_T0_out)) / (HTS221_H1_T0_out - HTS221_H0_T0_out) * (HTS221_H1_rh - HTS221_H0_rh) + HTS221_H0_rh;

    humidity_t = (uint16_t)(H_rh * pow(10, HTS221_HUM_DECIMAL_DIGITS));

    humidity = ((float)humidity_t)/pow(10, HTS221_HUM_DECIMAL_DIGITS);

    return humidity;
}

static tS32 HTS221_ReadId (tPU8 idVal)
{
	tU8 readRes;
	volatile tChar initRdBfr [5] = {0x00, 0x00, 0x00, 0x00, 0x00};
	volatile tChar initRdAutoIncBfr [5] = {0x00, 0x00, 0x00, 0x00, 0x00};

	// Get device ID for identification and AV CONF
	readRes = I2C_GetData (HTS221_REG_AV_CONF, 1, (char *)&initRdBfr[0]);
	printf ("HTS221 AV CONF reg read result = %d : 0x%x\n", readRes, initRdBfr[0]);

	readRes = I2C_GetData (HTS221_REG_WHO_AM_I, 1, (char *)&initRdBfr[0]);
	printf ("HTS221 Who Am I reg read result = %d : 0x%x\n", readRes, initRdBfr[0]);

	// Get the device ID and the AV CONF using auto-increment
	readRes = I2C_GetDataWithAutoIncrement ((HTS221_REG_WHO_AM_I | HTS221_AUTO_INCREMENT_BIT), 2, (char *)&initRdAutoIncBfr[0]);
	printf ("HTS221 WHO AMI/AV CONF with auto increment read result = %d : 0x%x - 0x%x\n",
			readRes, initRdAutoIncBfr[0], initRdAutoIncBfr[1]);

	*idVal = initRdBfr[0];

	return ERROR_CODE_NO_ERROR;
}

static tSInt HTS221_WriteDataToFile (float temperature, float humidity)
{
	// Depending on configuration we write data on file or on shared memory
#if (defined HTS221_WRITE_TO_FILE)
	struct timeval curTimeValue;
	std::stringstream textStream;
	float mSec;

	// Open report file
	HTS221_reportFile.open ("hts221_report.txt", ios::out | ios::app);

	if (false == HTS221_reportFile.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}

	// Print data
	printf ("HTS221 T --- H : T=0x%x-0x%x, %.3f --- H=0x%x-0x%x, %.3f\n",
			HTS221_registersMapping.TEMP_OUT_H, HTS221_registersMapping.TEMP_OUT_L, temperature,
			HTS221_registersMapping.HUMIDITY_OUT_H, HTS221_registersMapping.HUMIDITY_OUT_L, humidity);

	// Get current time
	gettimeofday (&curTimeValue, NULL);

    mSec = (curTimeValue.tv_sec - HTS221_startTimeValue.tv_sec) * 1000 +
    	   (curTimeValue.tv_usec - HTS221_startTimeValue.tv_usec) / (float)1000;

	// Write result to CSV file, format:
	// <time stamp> - <temperature> - <humidity>
    textStream.str (std::string());
    textStream.clear();
	textStream << mSec << "," << temperature << "," << humidity << '\n';
	std::string tmpStr = textStream.str();
	HTS221_reportFile << tmpStr;

	// Close report file
	HTS221_reportFile.close ();
#elif (defined APP_USE_SHAREDMEMS)
	// Write data on shared memory
	struct timeval curTimeValue;
	float mSec;
	IPC_sharedMemoryElemTy *sharedMemPtr;

	// Print data
	printf ("HTS221 T --- H : T=0x%x-0x%x, %.3f --- H=0x%x-0x%x, %.3f\n",
			HTS221_registersMapping.TEMP_OUT_H, HTS221_registersMapping.TEMP_OUT_L, temperature,
			HTS221_registersMapping.HUMIDITY_OUT_H, HTS221_registersMapping.HUMIDITY_OUT_L, humidity);

	// Get current time
	gettimeofday (&curTimeValue, NULL);

	mSec = (curTimeValue.tv_sec - HTS221_startTimeValue.tv_sec) * 1000 +
		   (curTimeValue.tv_usec - HTS221_startTimeValue.tv_usec) / (float)1000;

	sharedMemPtr = IPC_SharedMemoryGetWriterDataPointer ();

	sharedMemPtr->dataBfr[0].type = IPC_DATA_IS_TEMP;
	sharedMemPtr->dataBfr[0].data = HTS221_registersMapping.TEMP_OUT_H;

	sharedMemPtr->dataBfr[1].type = IPC_DATA_IS_TEMP;
	sharedMemPtr->dataBfr[1].data = HTS221_registersMapping.TEMP_OUT_L;

	sharedMemPtr->dataBfr[2].type = IPC_DATA_IS_HUMIDITY;
	sharedMemPtr->dataBfr[2].data = HTS221_registersMapping.HUMIDITY_OUT_H;

	sharedMemPtr->dataBfr[3].type = IPC_DATA_IS_HUMIDITY;
	sharedMemPtr->dataBfr[3].data = HTS221_registersMapping.HUMIDITY_OUT_L;

	sharedMemPtr->temperature = temperature;
	sharedMemPtr->humidity = humidity;

	sharedMemPtr->writerId = APP_THREAD_HTS221_ID;
	sharedMemPtr->time = mSec;
	sharedMemPtr->len = 2;
#else
	// No output method defined
#endif // #if (defined WRITE_TO_FILE)

	return ERROR_CODE_NO_ERROR;
}

static void HTS221_PowerOn (void)
{
    tChar tmpReg;

    // Read the register content
    I2C_GetData (HTS221_REG_CTRL_REG1, 1, &tmpReg);

    // Set the power down bit
    tmpReg |= HTS221_MODE_ACTIVE;

    // Write register
    I2C_WriteData (HTS221_REG_CTRL_REG1, 1, &tmpReg);
}

static void HTS221_PowerOff (void)
{
	tChar tmpReg;

    /* Read the register content */
    I2C_GetData (HTS221_REG_CTRL_REG1, 1, &tmpReg);

    /* Reset the power down bit */
    tmpReg &= ~(HTS221_MODE_ACTIVE);

    /* Write register */
    I2C_WriteData (HTS221_REG_CTRL_REG1, 1, &tmpReg);
}

static void HTS221_Calibration (void)
{
    tU16 T0_degC_x8_L, T0_degC_x8_H, T1_degC_x8_L, T1_degC_x8_H;
    tU8 H0_rh_x2, H1_rh_x2;
    tChar rdBfr[2] = {0,0};

    ///
    // Temperature calibration. Temperature in degree for calibration ( "/8" to obtain float)
    ///
    I2C_GetData (HTS221_REG_T0_degC_x8, 1, &rdBfr[0]);
    HTS221_registersMapping.T0_degC_x8 = rdBfr[0];
    T0_degC_x8_L = (tU16)rdBfr[0];

    I2C_GetData (HTS221_REG_T1_T0_msb, 1, &rdBfr[0]);
    HTS221_registersMapping.T1_T0_msb = rdBfr[0];
    T0_degC_x8_H = (tU16) (rdBfr[0] & 0x03);

    HTS221_T0_degC = ((float)((T0_degC_x8_H << 8) | (T0_degC_x8_L))) / 8;

    I2C_GetData (HTS221_REG_T1_degC_x8, 1, &rdBfr[0]);
    HTS221_registersMapping.T1_degC_x8 = rdBfr[0];
    T1_degC_x8_L = (tU16)rdBfr[0];

    I2C_GetData (HTS221_REG_T1_T0_msb, 1, &rdBfr[0]);
    HTS221_registersMapping.T1_T0_msb = rdBfr[0];
    T1_degC_x8_H = (tU16) (rdBfr[0] & 0x0C);
    T1_degC_x8_H = T1_degC_x8_H >> 2;

    HTS221_T1_degC = ((float)((T1_degC_x8_H << 8) | (T1_degC_x8_L))) / 8;

    I2C_GetDataWithAutoIncrement ((HTS221_REG_T0_OUT_LSB | HTS221_AUTO_INCREMENT_BIT), 2, (char *)&rdBfr[0]); // Read 0x3C and 0x3D registers
	HTS221_registersMapping.T0_OUT_LSB = rdBfr[0];
	HTS221_registersMapping.T0_OUT_MSB = rdBfr[1];
	HTS221_T0_out = ((((tS16)rdBfr[1]) << 8) | (tS16)rdBfr[0]);

    I2C_GetDataWithAutoIncrement ((HTS221_REG_T1_OUT_LSB | HTS221_AUTO_INCREMENT_BIT), 2, (char *)&rdBfr[0]); // Read 0x3E and 0x3F registers
	HTS221_registersMapping.T1_OUT_LSB = rdBfr[0];
	HTS221_registersMapping.T1_OUT_MSB = rdBfr[1];
    HTS221_T1_out = ((((tS16)rdBfr[1]) << 8) | (tS16)rdBfr[0]);

    // Output temperature calibration registers
    printf ("HTS221 Temperature Calibration: HTS221_T0_degC = %.2f, HTS221_T1_degC = %.2f, HTS221_T0_out = %ih, HTS221_T1_out = %ih\n",
    		HTS221_T0_degC, HTS221_T1_degC, HTS221_T0_out, HTS221_T1_out);

    ///
    // Humidity Calibration
    ///
    I2C_GetData (HTS221_REG_H0_rH_x2, 1, &rdBfr[0]);
	HTS221_registersMapping.H0_rH_x2 = rdBfr[0];
    H0_rh_x2 = (tU8)rdBfr[0];

    I2C_GetData (HTS221_REG_H1_rH_x2, 1, &rdBfr[0]);
    HTS221_registersMapping.H1_rH_x2 = rdBfr[0];
    H1_rh_x2 = (tU8)rdBfr[0];

    I2C_GetDataWithAutoIncrement ((HTS221_REG_H0_T0_OUT_LSB | HTS221_AUTO_INCREMENT_BIT), 2, (char *)&rdBfr[0]); // Read 0x2C and 0x3D registers
	HTS221_registersMapping.H0_T0_OUT_LSB = rdBfr[0];
	HTS221_registersMapping.H0_T0_OUT_MSB = rdBfr[1];
    HTS221_H0_T0_out = ((((tS16)rdBfr[1]) << 8) | (tS16)rdBfr[0]);

    I2C_GetDataWithAutoIncrement ((HTS221_REG_H1_T0_OUT_LSB | HTS221_AUTO_INCREMENT_BIT), 2, (char *)&rdBfr[0]); // Read 0x3A and 0x3B registers
	HTS221_registersMapping.H1_T0_OUT_LSB = rdBfr[0];
	HTS221_registersMapping.H1_T0_OUT_MSB = rdBfr[1];
    HTS221_H1_T0_out = ((((tS16)rdBfr[1]) << 8) | (tS16)rdBfr[0]);

    HTS221_H0_rh = ((float)H0_rh_x2) / 2;
    HTS221_H1_rh = ((float)H1_rh_x2) / 2;

    // Output humidity calibration registers
    printf ("HTS221 Humidity Calibration: H0_T0_out = %ih, HTS221_H0_rh = %.2f, HTS221_H1_rh = %.2f\n",
    		HTS221_H0_T0_out, HTS221_H0_rh, HTS221_H1_rh);
}
#if 0
float LinearInterpolation (linearPointsTy *points, float point0Read)
{
    float point0Deg;

    point0Deg = points->point1.y + (points->point2.y - points->point1.y) * ((point0Read - points->point1.x) / (points->point2.x - points->point1.x));

    return point0Deg;
}
static int test_interpolation (void)
{
	int res = ERROR_CODE_NO_ERROR;
    linearPointsTy inputPoints;
    pointTy outputPoint;

    // Test
    inputPoints.point1.x = 300;
    inputPoints.point1.y = 10;

    inputPoints.point2.x = 500;
    inputPoints.point2.y = 20;

    outputPoint.x = 400;

    outputPoint.y = LinearInterpolation (&inputPoints, outputPoint.x);

    printf ("\nTemperature is: %3.1f degree\n", outputPoint.y);

	return res;
}
#endif // #if 0

// End of file
