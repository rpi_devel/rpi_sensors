/*
 * i2c_thread.h
 *
 *  Created on: Feb 7, 2015
 *      Author: alberto
 */

#ifndef I2C_THREAD_H_
#define I2C_THREAD_H_

#define I2C_BUF_MAX_LEN				8

typedef enum
{
	I2C_MODE_NONE  = 0,
	I2C_MODE_INIT  = 1,
	I2C_MODE_READ  = 2,
	I2C_MODE_WRITE = 3,
	I2C_MODE_CLOSE = 4,
	I2C_MODE_EXIT  = 5
} I2C_modeEnumTy;

typedef struct
{
	I2C_modeEnumTy mode;
	bcm2835I2CClockDivider clkDiv;
	unsigned char slave_address;
	unsigned int rdLen;
	unsigned int wrLen;
	char rdBuf[I2C_BUF_MAX_LEN];
	char wrBuf[I2C_BUF_MAX_LEN];
} I2C_paramsTy;

extern void* I2C_Thread (void *arg);

extern void I2C_ExitThread (bool exitThread);

extern int I2C_SignalHandler (void);

extern bool I2C_GetParameters (I2C_modeEnumTy mode, bcm2835I2CClockDivider clockDiv, unsigned char slaveAddress,
		                       char *dataBfr, int dataLen);

#endif /* I2C_THREAD_H_ */
