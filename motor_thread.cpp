/*
 * motor_thread.cpp
 *
 *  Created on: Oct 22, 2015
 *      Author: alberto
 */

#include <iostream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <time.h>
#include <sys/time.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "gpio_common.h"
#include "gpio_mem.h"

#include "motor_thread.h"

using namespace std;

// 28YBJ-48 Stepper Motor
// It has good torque for its size but relatively slow motion.
// Motor details:
//    4 Phase 5 Wire Connection
//    StepperMotor5V-2.jpg100% Brand New
//    Phase: 4
//    Current: 160 mA per winding (320 mA in 4-step mode) Measured: 250mA stopped, 200 mA running fast
//    Resistance: 31 Ω per coil winding (from Red wire to any coil)
//    Voltage: 5V DC
//    Step Angle (8-Step sequence: Internal Motor alone): 5.625° (64 steps per revolution)
//    Step Angle (4-Step sequence: Internal Motor alone): 11.25° (32 steps per revolution)
//    Gear Reduction ratio: 1 / 64 (Not really exact: probably 63.68395.:1 )
//       It takes 64*64 = 4096 steps per output shaft revolution in 8-step sequence
//       It takes 32*64 = 2048 steps per output shaft revolution in 4-step sequence
//    No-Load Pull-Out Frequency: 800pps
//    No-Load Pull-In Frequency: 500pps
//    Pull-In Torque : ≥ 78.4mN.m
//    Wiring Instruction : A (Blue), B (Pink), C (Yellow), D (Orange), E (Red, Mid-Point)
//    Weight : 30g

///
// Compiler defines:
//    - 8-steps or 4-steps sequence can be selected (not both)
//    - Direction change: if defined the motor change direction
///
#define MOTOR_STEPPER_USES_8_STEP_SEQUENCE
#undef MOTOR_STEPPER_USES_8_STEP_SEQUENCE

#define MOTOR_STEPPER_USES_4_STEP_SEQUENCE
//#undef MOTOR_STEPPER_USES_4_STEP_SEQUENCE

#define MOTOR_STEPPER_CHANGE_DIRECTION
#undef MOTOR_STEPPER_CHANGE_DIRECTION

///
// Defines
///
#define ST_LOG_FILE_APP_MOTOR_STATUS		"/webroot/logs/motor_status.txt"

#define MOTOR_CMD_QUEUE_LEN					10

#define MOTOR_STEP_ARRAY_COILS_NUMBER		4 // This is the number of GPIOs to use

///
// 8-steps sequence
///
// Step Angle (8-Step sequence: Internal Motor alone): 5.625° (64 steps per revolution)
// Gear Reduction ratio: 1 / 64
// 64*64 = 4096 steps per output shaft revolution
#define MOTOR_8_STEP_ARRAY_STEPS_NUMBER		8
#define MOTOR_8_STEP_TOTAL_STEPS			4096 // 4096 steps per output shaft revolution

///
// 4-steps sequence
///
// Step Angle (4-Step sequence: Internal Motor alone): 11.25° (32 steps per revolution)
// Gear Reduction ratio: 1 / 64
// 32*64 = 2048 steps per output shaft revolution
#define MOTOR_4_STEP_ARRAY_STEPS_NUMBER		4
#define MOTOR_4_STEP_TOTAL_STEPS			2048 // 2048 steps per output shaft revolution

#if (defined MOTOR_STEPPER_USES_8_STEP_SEQUENCE)
	#define MOTOR_DELTA_TIME_BETWEEN_STEPS		700 // Minimum value is 700, below this vale the stepper motor shaft is not moving
	#define MOTOR_DELTA_TIME_AFTER_SHAFT_REVO	700 // Minimum value is 700

	#define MOTOR_STEP_ARRAY_STEPS_NUMBER		MOTOR_8_STEP_ARRAY_STEPS_NUMBER
	#define MOTOR_STEP_TOTAL_STEPS				MOTOR_8_STEP_TOTAL_STEPS
#elif (defined MOTOR_STEPPER_USES_4_STEP_SEQUENCE)
	#define MOTOR_DELTA_TIME_BETWEEN_STEPS		1400 // Minimum value is 1400, below this vale the stepper motor shaft is not moving
	#define MOTOR_DELTA_TIME_AFTER_SHAFT_REVO	1400 // Minimum value is 1400

	#define MOTOR_STEP_ARRAY_STEPS_NUMBER		MOTOR_4_STEP_ARRAY_STEPS_NUMBER
	#define MOTOR_STEP_TOTAL_STEPS				MOTOR_4_STEP_TOTAL_STEPS
#else
	#error "A step sequence shall be defined"
#endif // #if (defined MOTOR_STEPPER_USES_8_STEP_SEQUENCE)

///
// Types
///
typedef struct
{
	// GPIO number
	tU8 gpioNum;

	// GPIO status
	tBool opened;
	GPIODEV_directionEnumTy dir;
	GPIODEV_valEnumTy value;

	// 8-step sequence for this GPIO
	GPIODEV_valEnumTy sequence8Steps[MOTOR_8_STEP_ARRAY_STEPS_NUMBER];

	// 4-step sequence for this GPIO
	GPIODEV_valEnumTy sequence4Steps[MOTOR_4_STEP_ARRAY_STEPS_NUMBER];
} MOTOR_gpioTableTy;

typedef struct
{
	MOTOR_CommandEnumTy cmdToSend;

	tSInt internalCmdGpioNum;
} MOTOR_cmdTy;

///
// Local variables
///
static pthread_t MOTOR_threadId;
static MOTOR_gpioTableTy MOTOR_gpioTable[MOTOR_STEP_ARRAY_COILS_NUMBER];
static MOTOR_cmdTy MOTOR_cmdQueue[MOTOR_CMD_QUEUE_LEN];
static tSInt MOTOR_cmdQueueIndex = 0;
static bool MOTOR_exitThread = false;
static ofstream MOTOR_logFile;
volatile sig_atomic_t MOTOR_ended = 0;
static MOTOR_directionTy MOTOR_direction = MOTOR_DIRECTION_NONE;
static tSInt MOTOR_currentStep = 0;
static tSInt MOTOR_stepsLeft = (MOTOR_STEP_TOTAL_STEPS - 1);

///
// Local functions declarations
///
static tSInt MOTOR_Init (void);
static tSInt MOTOR_InitLogFile (void);
static tSInt MOTOR_UpdateLogFile (void);
static tSInt MOTOR_Stepper (tSInt step);
static tSInt MOTOR_StepperInit (void);
static tSInt MOTOR_StepperIdle (void);

///
// Global functions definitions
///
int MOTOR_SignalHandler (void)
{
	return MOTOR_ended;
}

void MOTOR_ExitThread (bool exitThread)
{
	if (true == exitThread)
	{
		MOTOR_exitThread = true;
	}
}

void* MOTOR_Thread (void *arg)
{
	// Get thread ID to return back at the end
	MOTOR_threadId = pthread_self ();

	// Call initialize function
	if (ERROR_CODE_NO_ERROR != MOTOR_Init ())
	{
        // Signal thread end
        MOTOR_ended = 1;

        // Return thread id
        return (void *)&MOTOR_threadId;
	}

	// Initialize stepper in idle status (all GPIOs set to LOW)
	if (ERROR_CODE_NO_ERROR != MOTOR_StepperIdle ())
	{
        // Signal thread end
        MOTOR_ended = 1;

        // Return thread id
        return (void *)&MOTOR_threadId;
	}

	// Thread loop
	while (false == MOTOR_exitThread)
	{
		while (MOTOR_stepsLeft > 0)
		{
			// Motor stepper
			if (ERROR_CODE_NO_ERROR != MOTOR_Stepper (MOTOR_currentStep))
			{
				printf ("Failed to execute motor step\n");
			}

			// Decrement the number of total steps
			MOTOR_stepsLeft--;

			// Increment step depending on current direction
			if (MOTOR_DIRECTION_FORWARD == MOTOR_direction)
			{
				(MOTOR_currentStep < MOTOR_STEP_ARRAY_STEPS_NUMBER) ? (MOTOR_currentStep++) : (MOTOR_currentStep = 0);
			}
			else if (MOTOR_DIRECTION_BACKWARD == MOTOR_direction)
			{
				(MOTOR_currentStep > 0) ? (MOTOR_currentStep--) : (MOTOR_currentStep = MOTOR_STEP_ARRAY_STEPS_NUMBER - 1);
			}
			else
			{
				printf ("Motor in execution without any direction defined\n");
			}

			// Suspend execution for microsecond intervals
			usleep (MOTOR_DELTA_TIME_BETWEEN_STEPS);
		}

		// Wait
		usleep (MOTOR_DELTA_TIME_AFTER_SHAFT_REVO);

		// Restart inner loop
		MOTOR_stepsLeft = (MOTOR_STEP_TOTAL_STEPS - 1);

		// Change direction
#if (defined MOTOR_STEPPER_CHANGE_DIRECTION)
		// Change direction
		(MOTOR_direction == MOTOR_DIRECTION_FORWARD) ? (MOTOR_direction = MOTOR_DIRECTION_BACKWARD) : (MOTOR_direction = MOTOR_DIRECTION_FORWARD);
#endif // #if (defined MOTOR_STEPPER_CHANGE_DIRECTION)

		// Depending on the direction set the current step
		if (MOTOR_DIRECTION_FORWARD == MOTOR_direction)
		{
			MOTOR_currentStep = 0;
		}
		else
		{
			MOTOR_currentStep = 7;
		}
	}

    // Signal thread end
    MOTOR_ended = 1;

    return (void *)&MOTOR_threadId;
}

void* MOTOR_ServiceThread (void *arg)
{
	// Get thread ID to return back at the end
	MOTOR_threadId = pthread_self ();

	// Call initialize function
	if (ERROR_CODE_NO_ERROR != MOTOR_Init ())
	{
        // Signal thread end
        MOTOR_ended = 1;

        // Return thread id
        return (void *)&MOTOR_threadId;
	}

	// Init log file
	MOTOR_InitLogFile ();

	// Init the GPIOs status to ensure their status can be read from external application
	MOTOR_UpdateLogFile ();

	// Thread main loop
    while (false == MOTOR_exitThread)
    {
    	// Check if some command has been issued
    	if (MOTOR_cmdQueueIndex >= 0)
    	{
			if (MOTOR_CMD_NONE != MOTOR_cmdQueue[MOTOR_cmdQueueIndex].cmdToSend)
			{
				// TODO: Manage the received command

				// Update the GPIOs status to ensure their status can be read from external application
				MOTOR_UpdateLogFile ();

				// Acquire command execution
				MOTOR_cmdQueue[MOTOR_cmdQueueIndex].cmdToSend = MOTOR_CMD_NONE;

				// Signal one command has been served
				MOTOR_cmdQueueIndex--;
			}
    	}

		// Suspend execution for microsecond intervals (0.5 sec) waiting for a new command
		usleep (500000);
    }

    return (void *)&MOTOR_threadId;
}

tSInt MOTOR_Command (MOTOR_CommandEnumTy cmd)
{
	tSInt res = ERROR_CODE_GENERIC;

	// Validate the command
	if (cmd < MOTOR_CMD_START && cmd > MOTOR_CMD_DIR_DECELERATE)
	{
		return res;
	}

	if (MOTOR_cmdQueueIndex < MOTOR_CMD_QUEUE_LEN)
	{
		// Increment command queue index
		MOTOR_cmdQueueIndex++;

		// Save the command received
		MOTOR_cmdQueue[MOTOR_cmdQueueIndex].cmdToSend = cmd;

		// Set return code
		res = ERROR_CODE_NO_ERROR;
	}

	// The command is not executed here, it has been planned
	return res;
}

///
// Local functions definitions
///
static tSInt MOTOR_StepperIdle (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tSInt cnt;
	tSInt resLevel;

	for (cnt = 0; cnt < MOTOR_STEP_ARRAY_COILS_NUMBER; cnt++)
	{
		if (true == MOTOR_gpioTable[cnt].opened  && GPIODEV_DIRECTION_OUT == MOTOR_gpioTable[cnt].dir)
		{
			// Set GPIO's value
			resLevel = gpio_mem_set_value (MOTOR_gpioTable[cnt].gpioNum, GPIODEV_VALUE_LOW);

			if (0 == resLevel)
			{
				MOTOR_gpioTable[cnt].value = GPIODEV_VALUE_LOW;
			}
			else
			{
				res = ERROR_CODE_GPIO_ACCESS_ERR;
			}
		}
		else
		{
			res = ERROR_CODE_GPIO_ACCESS_ERR;
		}
	}

	return res;
}

static tSInt MOTOR_StepperInit (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tSInt cnt;
	tSInt resDir, resLevel;

    // GPIO initialization phase: set the GPIO as output and put in the default status (LOW)
	for (cnt = 0; cnt < MOTOR_STEP_ARRAY_COILS_NUMBER; cnt++)
	{
		// Set GPIO's direction
		resDir = gpio_mem_set_direction (MOTOR_gpioTable[cnt].gpioNum, GPIODEV_DIRECTION_OUT);

		if (0 == resDir)
		{
			// Set GPIO as opened
			MOTOR_gpioTable[cnt].opened = true;
			MOTOR_gpioTable[cnt].dir = GPIODEV_DIRECTION_OUT;

			// Set GPIO's value
			resLevel = gpio_mem_set_value (MOTOR_gpioTable[cnt].gpioNum, GPIODEV_VALUE_LOW);

			if (0 == resLevel)
			{
				MOTOR_gpioTable[cnt].value = GPIODEV_VALUE_LOW;
			}
		}

    	if (0 != resDir || 0 != resLevel)
    	{
    		res = ERROR_CODE_GPIO_ACCESS_ERR;

    		printf ("Failed to set GPIO%d, set direction=%d, set LOW value=%d\n",
    				MOTOR_gpioTable[cnt].gpioNum, resDir, resLevel);
    	}
	}

	return res;
}

///
// Init functions
///
static tSInt MOTOR_Init (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	GPIODEV_valEnumTy sequence8Steps[MOTOR_8_STEP_ARRAY_STEPS_NUMBER][MOTOR_STEP_ARRAY_COILS_NUMBER] =
	{
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH },
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH },
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH }
	};
	GPIODEV_valEnumTy sequence4Steps[MOTOR_4_STEP_ARRAY_STEPS_NUMBER][MOTOR_STEP_ARRAY_COILS_NUMBER] =
	{
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH },
		{ GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_HIGH, GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW  },
		{ GPIODEV_VALUE_HIGH, GPIODEV_VALUE_LOW,  GPIODEV_VALUE_LOW,  GPIODEV_VALUE_HIGH }
	};
	tSInt gpioCnt, stepCnt;
	tChar gpio[MOTOR_STEP_ARRAY_COILS_NUMBER] = {GPIO_22, GPIO_27, GPIO_17, GPIO_25};

	// Initialize globals
	MOTOR_ended = 0;
	MOTOR_exitThread = false;
    MOTOR_direction = MOTOR_DIRECTION_FORWARD;
	MOTOR_currentStep = 0;
	MOTOR_stepsLeft = (MOTOR_STEP_TOTAL_STEPS - 1);
	memset ((void *)&MOTOR_cmdQueue[0], 0, sizeof (MOTOR_cmdQueue));
	MOTOR_cmdQueueIndex = -1; // Set to -1 meaning queue empty

	// GPIO table
	for (gpioCnt = 0; gpioCnt < MOTOR_STEP_ARRAY_COILS_NUMBER; gpioCnt++)
	{
		MOTOR_gpioTable[gpioCnt].gpioNum = gpio[gpioCnt];
		MOTOR_gpioTable[gpioCnt].opened = false;
		MOTOR_gpioTable[gpioCnt].dir = GPIODEV_DIRECTION_UNKNOWN;
		MOTOR_gpioTable[gpioCnt].value = GPIODEV_VALUE_UNKNOWN;

		for (stepCnt = 0; stepCnt < MOTOR_8_STEP_ARRAY_STEPS_NUMBER; stepCnt++)
		{
			MOTOR_gpioTable[gpioCnt].sequence8Steps[stepCnt] = sequence8Steps[stepCnt][gpioCnt];
		}

		for (stepCnt = 0; stepCnt < MOTOR_4_STEP_ARRAY_STEPS_NUMBER; stepCnt++)
		{
			MOTOR_gpioTable[gpioCnt].sequence4Steps[stepCnt] = sequence4Steps[stepCnt][gpioCnt];
		}
	}

    // Open GPIOs
    if (0 != gpio_mem_open ())
	{
    	// Error opening GPIO memory access
    	printf ("Failed to open GPIOs for direct memory access\n");

		// Return error
		res = ERROR_CODE_GPIO_ACCESS_ERR;
	}
    else
    {
		// Initialize motor stepper
		if (ERROR_CODE_NO_ERROR != MOTOR_StepperInit ())
		{
			// Failed to init motor stepper
			printf ("Failed to initialize motor stepper\n");

			// Return error
			res = ERROR_CODE_GPIO_ACCESS_ERR;
		}
    }

    return res;
}

///
// MOTOR functions
///
static tSInt MOTOR_Stepper (tSInt step)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tSInt cnt;

#if (defined MOTOR_STEPPER_USES_8_STEP_SEQUENCE)
	if (step < MOTOR_STEP_ARRAY_STEPS_NUMBER)
	{
		for (cnt = 0; cnt < MOTOR_STEP_ARRAY_COILS_NUMBER; cnt++)
		{
			if (0 != gpio_mem_set_value (MOTOR_gpioTable[cnt].gpioNum, MOTOR_gpioTable[cnt].sequence8Steps[step]))
			{
				// Set error
				res = ERROR_CODE_GPIO_ACCESS_ERR;

				// Error message
				printf ("Failed to set GPIO%d to value %d\n", MOTOR_gpioTable[cnt].gpioNum, (tSInt)MOTOR_gpioTable[cnt].sequence8Steps[step]);
			}
		}
	}
#else
	if (step < MOTOR_STEP_ARRAY_STEPS_NUMBER)
	{
		for (cnt = 0; cnt < MOTOR_STEP_ARRAY_COILS_NUMBER; cnt++)
		{
			if (0 != gpio_mem_set_value (MOTOR_gpioTable[cnt].gpioNum, MOTOR_gpioTable[cnt].sequence4Steps[step]))
			{
				// Set error
				res = ERROR_CODE_GPIO_ACCESS_ERR;

				// Error message
				printf ("Failed to set GPIO%d to value %d\n", MOTOR_gpioTable[cnt].gpioNum, (tSInt)MOTOR_gpioTable[cnt].sequence4Steps[step]);
			}
		}
	}
#endif

	return res;
}

///
// LOG file functions
///
static tSInt MOTOR_InitLogFile (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;

	// Open report for TEXT data last temperature and humidity read
	MOTOR_logFile.open (ST_LOG_FILE_APP_MOTOR_STATUS, ios::out | ios::trunc);

	if (false == MOTOR_logFile.is_open ())
	{
		res = ERROR_CODE_GENERIC;
	}

	// Close report file
	MOTOR_logFile.close ();

	return res;
}

static tSInt MOTOR_UpdateLogFile (void)
{
	std::stringstream textStream;
	tSInt cnt;
	tSInt channelNum = 1;

	// Open LAST READ file for writing
	MOTOR_logFile.open (ST_LOG_FILE_APP_MOTOR_STATUS, ios::out | ios::trunc);

	if (false == MOTOR_logFile.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Clear string
	    textStream.str (std::string());
	    textStream.clear();

	    // Build string for all channels
	    for (cnt = 0; cnt < MOTOR_STEP_ARRAY_COILS_NUMBER; cnt += 2)
	    {
	    	// Set channel number
	    	textStream << channelNum << " ";
	    	channelNum++;

	    	// Set associated GPIO
	    	textStream << (tSInt)MOTOR_gpioTable[cnt].gpioNum <<"_" << (tSInt)MOTOR_gpioTable[(cnt + 1)].gpioNum << " ";

	    	if ((true == MOTOR_gpioTable[cnt].opened && GPIODEV_DIRECTION_OUT == MOTOR_gpioTable[cnt].dir) &&
	    		(true == MOTOR_gpioTable[(cnt + 1)].opened && GPIODEV_DIRECTION_OUT == MOTOR_gpioTable[(cnt + 1)].dir))
	    	{
    			// Set status
	    		if ((GPIODEV_VALUE_HIGH == MOTOR_gpioTable[cnt].value) || (GPIODEV_VALUE_HIGH == MOTOR_gpioTable[(cnt + 1)].value))
	    		{
	    			textStream << "OFF" << endl;
	    		}
	    		else
	    		{
	    			textStream << "ON" << endl;
	    		}
	    	}
	    	else
	    	{
    			textStream << "DISABLED" << endl;
	    	}
	    }

	    // Save text stream on a file
		std::string tmpStr = textStream.str();
		MOTOR_logFile << tmpStr;

		// Close report file
		MOTOR_logFile.close ();
	}

	return ERROR_CODE_NO_ERROR;
}

// End of file




