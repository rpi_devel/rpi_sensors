/*
 * gpio_thread.h
 *
 *  Created on: Dec 2, 2014
 *      Author: alberto
 */

#ifndef GPIO_THREAD_H_
#define GPIO_THREAD_H_

typedef enum
{
	GPIO_CMD_NONE = 0,
	GPIO_CMD_SET_VALUE_LOW = 1,
	GPIO_CMD_SET_VALUE_HIGH = 2
} GPIO_CommandEnumTy;

extern tSInt GPIO_Init (tPU8 gpioListPtr, tSInt gpioNum);

extern GPIODEV_directionEnumTy GPIO_SetDirection (tSInt gpioNum, GPIODEV_directionEnumTy direction);

extern GPIODEV_valEnumTy GPIO_SetValue (tSInt gpioNum, GPIODEV_valEnumTy value);

extern GPIODEV_valEnumTy GPIO_GetValue (tSInt gpioNum);

extern tSInt GPIO_Command (tSInt gpioNum, GPIO_CommandEnumTy cmd);

extern void* GPIO_Thread (void *arg);

extern void* GPIO_ServiceThread (void *arg);

extern void GPIO_ExitThread (bool exitThread);

extern int GPIO_SignalHandler (void);

#endif /* GPIO_THREAD_H_ */
