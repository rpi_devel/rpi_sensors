/*
 * lsm6ds0_thread.h
 *
 *  Created on: Apr 12, 2015
 *      Author: sandro
 */

#ifndef LSM6DS0_THREAD_H_
#define LSM6DS0_THREAD_H_

namespace LSM6DS0_data
{
	const int MAX = 30;

	class device
	{
		public:
	    	device () {}
	};

	enum AccGyro_DeviceEnumTy
	{
	    LSM6DS0_DEVICE        = 0,
	    LSM6DS3_DEVICE        = 1,
	    UNKNOWN_DEVICE        = 0xFF
	};
};

extern void* AccGyro_Thread (void *arg);

extern void AccGyro_ExitThread (tBool exitThread);

extern tS32 LSM6DS0_ReadTemperatureAndHumiditySequence (void);

extern tS32 AccGyro_SignalHandler (void);

extern void AccGyroSelect ( int SelectedDeviceId ) ;

#endif // LSM6DS0_THREAD_H_

// End of file


