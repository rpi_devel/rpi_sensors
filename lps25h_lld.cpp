/*
 * lps25h_lld.cpp
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#include <string>       // std::string
#include <iostream>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#include <bcm2835.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "i2c_lld.h"

#include "lps25h_lld.h"

#define LPS25H_I2C_ADD						0x5D

#define LPS25H_DEVICE_ID					((tU8)0xBD)

// Register map
#define LPS25H_REG_REF_P_XL					0x08
#define LPS25H_REG_REF_P_L  				0x09
#define LPS25H_REG_REF_P_H  				0x0A
#define LPS25H_REG_WHO_AM_I					0x0F
#define LPS25H_REG_RES_CONF					0x10
#define LPS25H_REG_CTRL_REG1				0x20
#define LPS25H_REG_CTRL_REG2				0x21
#define LPS25H_REG_CTRL_REG3				0x22
#define LPS25H_REG_CTRL_REG4				0x23
#define LPS25H_REG_INT_CFG  				0x24
#define LPS25H_REG_INT_SOURCE 				0x25
#define LPS25H_REG_STATUS_REG				0x27
#define LPS25H_REG_PRESS_OUT_XL  			0x28
#define LPS25H_REG_PRESS_OUT_L   			0x29
#define LPS25H_REG_PRESS_OUT_H				0x2A
#define LPS25H_REG_TEMP_OUT_L				0x2B
#define LPS25H_REG_TEMP_OUT_H				0x2C
#define LPS25H_REG_FIFO_CTRL				0x2E
#define LPS25H_REG_FIFO_STATUS				0x2F
#define LPS25H_REG_THS_P_L   				0x30
#define LPS25H_REG_THS_P_H  				0x31
#define LPS25H_REG_RPDS_L    				0x39
#define LPS25H_REG_RPDS_H    				0x3A

// Resolution configuration register fields
#define LPS25H_CTRL_RES_CONF_P_AVRG_8		0x00
#define LPS25H_CTRL_RES_CONF_P_AVRG_32		0x01
#define LPS25H_CTRL_RES_CONF_P_AVRG_128		0x02
#define LPS25H_CTRL_RES_CONF_P_AVRG_512		0x03

#define LPS25H_CTRL_RES_CONF_T_AVRG_8		0x00
#define LPS25H_CTRL_RES_CONF_T_AVRG_16		0x04
#define LPS25H_CTRL_RES_CONF_T_AVRG_32		0x08
#define LPS25H_CTRL_RES_CONF_T_AVRG_64		0x0C

// Control register 1 fields
#define LPS25H_CTRL_REG1_PD_ENABLE			0x80

#define LPS25H_CTRL_REG1_ODR_ONE_SHOT		0x00
#define LPS25H_CTRL_REG1_ODR_1HZ			0x10
#define LPS25H_CTRL_REG1_ODR_7HZ			0x20
#define LPS25H_CTRL_REG1_ODR_12_5HZ			0x30
#define LPS25H_CTRL_REG1_ODR_25HZ			0x40

#define LPS25H_CTRL_REG1_DIFF_EN			0x08

#define LPS25H_CTRL_REG1_BDU_WAIT_LSB		0x04
#define LPS25H_CTRL_REG1_BDU_CONT_UPDATE	0x00

#define LPS25H_CTRL_REG1_RESET_AZ			0x02

#define LPS25H_CTRL_REG1_SPI_4WIRE			0x00
#define LPS25H_CTRL_REG1_SPI_3WIRE			0x01

// Control register 2 fields
#define LPS25H_CTRL_REG2_BOOT				0x80
#define LPS25H_CTRL_REG2_FIFO_ENABLE		0x40
#define LPS25H_CTRL_REG2_WTM_ENABLE			0x20
#define LPS25H_CTRL_REG2_FIFO_MEAN_DEC		0x10
#define LPS25H_CTRL_REG2_SW_RESET			0x04
#define LPS25H_CTRL_REG2_AUTO_ZERO			0x02
#define LPS25H_CTRL_REG2_ONE_SHOT			0x01

// Control register 3 fields
#define LPS25H_CTRL_REG3_INT_ACTIVE_HIGH	0x80
#define LPS25H_CTRL_REG3_INT_ACTIVE_LOW 	0x00

#define LPS25H_CTRL_REG3_INT_PUSH_PULL		0x40
#define LPS25H_CTRL_REG3_INT_OPEN_DRAIN		0x00

#define LPS25H_CTRL_REG3_DATA_SIGNAL		0x00
#define LPS25H_CTRL_REG3_PRESSURE_HIGH		0x01
#define LPS25H_CTRL_REG3_PRESSURE_LOW		0x02
#define LPS25H_CTRL_REG3_PRESSURE_LOWORHIGH	0x03

// Control register 4 fields
#define LPS25H_CTRL_REG4_DATA_READY_ON_INT1	0x00
#define LPS25H_CTRL_REG4_OVERRUN_ON_INT1	0x01
#define LPS25H_CTRL_REG4_WATERMARK_ON_INT1	0x02
#define LPS25H_CTRL_REG4_EMPTY_ON_INT1		0x03

// Status register fields
#define LPS25H_STATUS_REG_TEMP_AVAIL		0x01
#define LPS25H_STATUS_REG_PRESSURE_AVAIL	0x02
#define LPS25H_STATUS_REG_TEMP_OVERRUN		0x10
#define LPS25H_STATUS_REG_PRESSURE_OVERRUN	0x20

#define LPS25H_AUTO_INCREMENT_BIT			0x80

typedef struct
{
	gap8 (8);

	tU8 REF_P_XL;
	tU8 REF_P_L;
	tU8 REF_P_H;

	gap8 (4);

	tU8 WHO_AM_I;
	tU8 RES_CONF;

	gap8 (15);

	tU8 CTRL_REG1;
	tU8 CTRL_REG2;
	tU8 CTRL_REG3;
	tU8 CTRL_REG4;
	tU8 INT_CFG;
	tU8 INT_SOURCE;

	gap8 (1);

	tU8 STATUS_REG;
	tU8 PRESSURE_OUT_XL;
	tU8 PRESSURE_OUT_L;
	tU8 PRESSURE_OUT_H;
	tU8 TEMP_OUT_L;
	tU8 TEMP_OUT_H;

	gap8 (1);

	tU8 FIFO_CTRL;
	tU8 FIFO_STATUS;
	tU8 THS_P_L;
	tU8 THS_P_H;

	gap (7);

	tU8 RPDS_L;
	tU8 RPDS_H;
} LPS25H_registersMappingTy;

typedef struct
{
	tBool tempReady;
	tBool pressureReady;
	tBool tempOverrun;
	tBool pressureOverrun;
} LPS25H_statusTy;

typedef struct
{
	tS32 referencePressure;
	float pressureOutHPa;
	float temperatureDegree;
} LPS25H_measureDataTy;

volatile static LPS25H_registersMappingTy LPS25H_registersMapping;
static LPS25H_measureDataTy LPS25H_measureData;

static tS32 LPS25H_ReadId (tPU8 idVal);
static tS32 LPS25H_ConfigureDevice (void);
static tS32 LPS25H_ReadStatusRegister (LPS25H_statusTy *LPS25H_statusPtr);
static tS32 LPS25H_ReadPressureRegisters (LPS25H_measureDataTy *LPS25H_measureDataPtr);
static tS32 LPS25H_ReadTemperatureRegisters (LPS25H_measureDataTy *LPS25H_measureDataPtr);

tS32 LPS25H_Init (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	tU8 deviceId;

	// Open LPS25H I2C access
	LPS25H_Open ();

	// Read ID to identify the proper device
	res = LPS25H_ReadId (&deviceId);

	if (ERROR_CODE_NO_ERROR != res || LPS25H_DEVICE_ID != deviceId)
	{
		// Close LPS25H I2C access
		LPS25H_Close ();

		return ERROR_CODE_GENERIC;
	}

	// Configure device
	res = LPS25H_ConfigureDevice ();

	if (ERROR_CODE_NO_ERROR != res)
	{
		res = ERROR_CODE_GENERIC;
	}

	// Close LPS25H I2C access
	LPS25H_Close ();

	return res;
}

tS32 LPS25H_Open (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;

	// Start I2C mode
	bcm2835_i2c_begin ();

	// Setup I2C
	bcm2835_i2c_setSlaveAddress (LPS25H_I2C_ADD);
	bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_626);

	return res;
}

tS32 LPS25H_Close (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;

	// Close I2C mode
	bcm2835_i2c_end ();

	return res;
}

tS32 LPS25H_ReadPressureAndTemperatureSequence (void)
{
	tS32 readPRes, readTRes;
	tS32 res = ERROR_CODE_NO_ERROR;
	LPS25H_statusTy LPS25H_status;

	// Set I2C for LPS25H access
	LPS25H_Open ();

	// Read status register
	LPS25H_ReadStatusRegister (&LPS25H_status);

	if (true == LPS25H_status.tempOverrun || true == LPS25H_status.pressureOverrun)
	{
		// We have data overrun
		printf ("LPS25H STATUS REGISTER overrun value : 0x%x\n", LPS25H_registersMapping.STATUS_REG);
	}

	if (false == LPS25H_status.tempReady || false == LPS25H_status.pressureReady)
	{
		// We have no data so we return back to the caller without blocking other processes
		printf ("LPS25H STATUS REGISTER not ready value : 0x%x\n", LPS25H_registersMapping.STATUS_REG);
	}

	// Perform data read
	readPRes = LPS25H_ReadPressureRegisters (&LPS25H_measureData);
	readTRes = LPS25H_ReadTemperatureRegisters (&LPS25H_measureData);

	if (BCM2835_I2C_REASON_OK == readPRes && BCM2835_I2C_REASON_OK == readTRes)
	{
		// Print pressure data
		printf ("LPS25H P --- T : 0x%x-0x%x-0x%x, P=%.3f, REF P=%d --- 0x%x-0x%x, T=%.3f\n",
				LPS25H_registersMapping.PRESSURE_OUT_H, LPS25H_registersMapping.PRESSURE_OUT_L, LPS25H_registersMapping.PRESSURE_OUT_XL,
				LPS25H_measureData.pressureOutHPa, LPS25H_measureData.referencePressure,
				LPS25H_registersMapping.TEMP_OUT_H, LPS25H_registersMapping.TEMP_OUT_L, LPS25H_measureData.temperatureDegree);
	}
	else
	{
		// Print error
		printf ("LPS25H ERROR pressure/temperature data read error : %d, %d\n", readPRes, readTRes);
	}

	// Free I2C access
	LPS25H_Close ();

	return res;
}

static tS32 LPS25H_ReadId (tPU8 idVal)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	volatile tChar initRdBfr[5] = {0x00};

	// Get device ID for identification and AV CONF
	res = I2C_GetData (LPS25H_REG_WHO_AM_I, 1, (char *)&initRdBfr[0]);
	printf ("LPS25H WHO AM I register read result = %d : 0x%x\n", res, initRdBfr[0]);

	*idVal = initRdBfr[0];

	return res;
}

static tS32 LPS25H_ConfigureDevice (void)
{
	tS32 writeRes;

	// Turn OFF device (Write CNTRL reg 1)
	LPS25H_registersMapping.CTRL_REG1 = 0;
	writeRes = I2C_WriteData (LPS25H_REG_CTRL_REG1, 1, (char *)&LPS25H_registersMapping.CTRL_REG1);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CTRL_REG1 write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	// Wait here
	usleep (1000);

	// Turn ON device (Write CNTRL reg 1)
	LPS25H_registersMapping.CTRL_REG1 = (LPS25H_CTRL_REG1_PD_ENABLE | LPS25H_CTRL_REG1_ODR_1HZ | LPS25H_CTRL_REG1_BDU_WAIT_LSB);
	writeRes = I2C_WriteData (LPS25H_REG_CTRL_REG1, 1, (char *)&LPS25H_registersMapping.CTRL_REG1);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CTRL_REG1 write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	// Set CNTRL REG 2
	LPS25H_registersMapping.CTRL_REG2 = LPS25H_CTRL_REG2_FIFO_ENABLE;
	writeRes = I2C_WriteData (LPS25H_REG_CTRL_REG2, 1, (char *)&LPS25H_registersMapping.CTRL_REG2);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CTRL_REG2 write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	// Set CNTRL REG 3
	LPS25H_registersMapping.CTRL_REG3 = 0;
	writeRes = I2C_WriteData (LPS25H_REG_CTRL_REG3, 1, (char *)&LPS25H_registersMapping.CTRL_REG3);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CTRL_REG3 write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	// Set CNTRL REG 4
	LPS25H_registersMapping.CTRL_REG4 = 0;
	writeRes = I2C_WriteData (LPS25H_REG_CTRL_REG4, 1, (char *)&LPS25H_registersMapping.CTRL_REG4);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CTRL_REG4 write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	// Set RES CONF REG
	LPS25H_registersMapping.RES_CONF = (LPS25H_CTRL_RES_CONF_P_AVRG_32 | LPS25H_CTRL_RES_CONF_T_AVRG_16);
	writeRes = I2C_WriteData (LPS25H_REG_RES_CONF, 1, (char *)&LPS25H_registersMapping.RES_CONF);
	if (BCM2835_I2C_REASON_OK != writeRes)
	{
		printf ("LPS25H ERROR CONF REG write result = %d\n", writeRes);

		return ERROR_CODE_I2C_ERROR;
	}

	return ERROR_CODE_NO_ERROR;
}

static tS32 LPS25H_ReadStatusRegister (LPS25H_statusTy *LPS25H_statusPtr)
{
	tS32 readRes;

	// Read CONF register
	readRes = I2C_GetData (LPS25H_REG_STATUS_REG, 1, (char *)&LPS25H_registersMapping.STATUS_REG);

	if (LPS25H_registersMapping.STATUS_REG & LPS25H_STATUS_REG_TEMP_AVAIL)
	{
		LPS25H_statusPtr->tempReady = true;
	}
	else
	{
		LPS25H_statusPtr->tempReady = false;
	}

	if (LPS25H_registersMapping.STATUS_REG & LPS25H_STATUS_REG_PRESSURE_AVAIL)
	{
		LPS25H_statusPtr->pressureReady = true;
	}
	else
	{
		LPS25H_statusPtr->pressureReady = false;
	}

	if (LPS25H_registersMapping.STATUS_REG & LPS25H_STATUS_REG_TEMP_OVERRUN)
	{
		LPS25H_statusPtr->tempOverrun = true;
	}
	else
	{
		LPS25H_statusPtr->tempOverrun = false;
	}

	if (LPS25H_registersMapping.STATUS_REG & LPS25H_STATUS_REG_PRESSURE_OVERRUN)
	{
		LPS25H_statusPtr->pressureOverrun = true;
	}
	else
	{
		LPS25H_statusPtr->pressureOverrun = false;
	}

	return readRes;
}

static tS32 LPS25H_ReadPressureRegisters (LPS25H_measureDataTy *LPS25H_measureDataPtr)
{
	tS32 readTempRes;
	tChar rdTempBfr[3];
	tS32 pressureOut;

	// Read pressure data
	readTempRes = I2C_GetDataWithAutoIncrement ((LPS25H_REG_PRESS_OUT_XL | LPS25H_AUTO_INCREMENT_BIT), 3, &rdTempBfr[0]);
	if (BCM2835_I2C_REASON_OK != readTempRes)
	{
		printf ("LPS25H ERROR get pressure data result = %d\n", readTempRes);
	}

	LPS25H_registersMapping.PRESSURE_OUT_XL = rdTempBfr[0];
	LPS25H_registersMapping.PRESSURE_OUT_L = rdTempBfr[1];
	LPS25H_registersMapping.PRESSURE_OUT_H = rdTempBfr[2];

	// Store data
	if (LPS25H_registersMapping.PRESSURE_OUT_H & 0x80)
	{
		// Negative number
		pressureOut = ((((tS32)0xFF                                    << 24) & (tS32)0xFF000000) |
					   (((tS32)LPS25H_registersMapping.PRESSURE_OUT_H  << 16) & (tS32)0x00FF0000) |
					   (((tS32)LPS25H_registersMapping.PRESSURE_OUT_L  <<  8) & (tS32)0x0000FF00) |
					   (((tS32)LPS25H_registersMapping.PRESSURE_OUT_XL <<  0) & (tS32)0x000000FF));

		LPS25H_measureDataPtr->pressureOutHPa = (float)pressureOut / 4096;
	}
	else
	{
		// Positive number
		pressureOut= ((((tS32)0x00                                    << 24) & (tS32)0xFF000000) |
					  (((tS32)LPS25H_registersMapping.PRESSURE_OUT_H  << 16) & (tS32)0x00FF0000) |
					  (((tS32)LPS25H_registersMapping.PRESSURE_OUT_L  <<  8) & (tS32)0x0000FF00) |
		              (((tS32)LPS25H_registersMapping.PRESSURE_OUT_XL <<  0) & (tS32)0x000000FF));

		LPS25H_measureDataPtr->pressureOutHPa = (float)pressureOut / 4096;
	}


	// Read reference pressure data
	readTempRes = I2C_GetDataWithAutoIncrement ((LPS25H_REG_REF_P_XL | LPS25H_AUTO_INCREMENT_BIT), 3, &rdTempBfr[0]);
	if (BCM2835_I2C_REASON_OK != readTempRes)
	{
		printf ("LPS25H ERROR get reference data result = %d\n", readTempRes);
	}

	LPS25H_registersMapping.REF_P_XL = rdTempBfr[0];
	LPS25H_registersMapping.REF_P_L = rdTempBfr[1];
	LPS25H_registersMapping.REF_P_H = rdTempBfr[2];

	// Store data
	if (LPS25H_registersMapping.REF_P_H & 0x80)
	{
		// Negative number
		LPS25H_measureDataPtr->referencePressure = ((((tS32)0xFF                             << 24) & (tS32)0xFF000000) |
									   	       	    (((tS32)LPS25H_registersMapping.REF_P_H  << 16) & (tS32)0x00FF0000) |
												    (((tS32)LPS25H_registersMapping.REF_P_L  <<  8) & (tS32)0x0000FF00) |
												    (((tS32)LPS25H_registersMapping.REF_P_XL <<  0) & (tS32)0x000000FF));
	}
	else
	{
		// Positive number
		LPS25H_measureDataPtr->referencePressure = ((((tS32)0x00                             << 24) & (tS32)0xFF000000) |
									   	   	        (((tS32)LPS25H_registersMapping.REF_P_H  << 16) & (tS32)0x00FF0000) |
												    (((tS32)LPS25H_registersMapping.REF_P_L  <<  8) & (tS32)0x0000FF00) |
												    (((tS32)LPS25H_registersMapping.REF_P_XL <<  0) & (tS32)0x000000FF));
	}

	return readTempRes;
}

static tS32 LPS25H_ReadTemperatureRegisters (LPS25H_measureDataTy *LPS25H_measureDataPtr)
{
	tS32 readTempRes;
	tChar rdTempBfr[3];
	tS16 temperature;

	// Read temperature data
	readTempRes = I2C_GetDataWithAutoIncrement ((LPS25H_REG_TEMP_OUT_L | LPS25H_AUTO_INCREMENT_BIT), 2, &rdTempBfr[0]);
	if (BCM2835_I2C_REASON_OK != readTempRes)
	{
		printf ("LPS25H ERROR get temperature data result = %d\n", readTempRes);
	}

	LPS25H_registersMapping.TEMP_OUT_L = rdTempBfr[0];
	LPS25H_registersMapping.TEMP_OUT_H = rdTempBfr[1];

	// Store data
	temperature = ((((tS16)LPS25H_registersMapping.TEMP_OUT_H << 8) & (tS16)0xFF00) |
				   (((tS16)LPS25H_registersMapping.TEMP_OUT_L << 8) & (tS16)0x00FF));

	LPS25H_measureDataPtr->temperatureDegree = (float)42.5 + ((float)temperature / 480);

	return readTempRes;
}

// End of file
