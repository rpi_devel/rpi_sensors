/*
 * lsm6ds0_lld.cpp
 *
 *  Created on: Apr 12, 2015
 *      Author: sandro
 */

#include <string>       // std::string
#include <iostream>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>

#include <bcm2835.h>

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "system_defines.h"

#include "ipc.h"

#include "i2c_lld.h"

#include "lsm6ds0_lld.h"

using namespace std;

///
// Define to directly write data to file without using service thread.
// If direct write is used it could be difficult to avoid sample loosing
///
//#define WRITE_TO_FILE
#undef WRITE_TO_FILE

///
// Define to use SHARED MEMORIES instead of MEMORY POOL
///
//#define WRITE_DELAYED_USING_SHARED_MEMORIES
#undef WRITE_DELAYED_USING_SHARED_MEMORIES

#define LSM6DS0_I2C_ADD                     0x6B

#define LSM6DS0_DEVICE_ID                   ((tU8)0x68)

#define LSM6DS0_REG_ACT_THS                 0x04
#define LSM6DS0_REG_ACT_DUR                 0x05
#define LSM6DS0_REG_INT_GEN_CFG_XL          0x06
#define LSM6DS0_REG_INT_GEN_THS_X_XL        0x07
#define LSM6DS0_REG_INT_GEN_THS_Y_XL        0x08
#define LSM6DS0_REG_INT_GEN_THS_Z_XL        0x09
#define LSM6DS0_REG_INT_GEN_DUR_XL          0x0A
#define LSM6DS0_REG_REFERENCE_G             0x0B
#define LSM6DS0_REG_INT_CTRL                0x0C
#define LSM6DS0_REG_WHO_AM_I                0x0F
#define LSM6DS0_REG_CTRL_REG1_G             0x10
#define LSM6DS0_REG_CTRL_REG2_G             0x11
#define LSM6DS0_REG_CTRL_REG3_G             0x12
#define LSM6DS0_REG_ORIENT_G                0x13
#define LSM6DS0_REG_INT_GEN_SRC_G           0x14
#define LSM6DS0_REG_OUT_TEMP_L              0x15
#define LSM6DS0_REG_OUT_TEMP_H              0x16
#define LSM6DS0_REG_STATUS_REG_G            0x17
#define LSM6DS0_REG_OUT_X_L_G               0x18
#define LSM6DS0_REG_OUT_X_H_G               0x19
#define LSM6DS0_REG_OUT_Y_L_G               0x1A
#define LSM6DS0_REG_OUT_Y_H_G               0x1B
#define LSM6DS0_REG_OUT_Z_L_G               0x1C
#define LSM6DS0_REG_OUT_Z_H_G               0x1D
#define LSM6DS0_REG_CTRL_REG4               0x1E
#define LSM6DS0_REG_CTRL_REG5_XL            0x1F
#define LSM6DS0_REG_CTRL_REG6_XL            0x20
#define LSM6DS0_REG_CTRL_REG7_XL            0x21
#define LSM6DS0_REG_CTRL_REG8               0x22
#define LSM6DS0_REG_CTRL_REG9               0x23
#define LSM6DS0_REG_CTRL_REG10              0x24
#define LSM6DS0_REG_INT_GEN_SRC_XL          0x26
#define LSM6DS0_REG_STATUS_REG              0x27
#define LSM6DS0_REG_OUT_X_L_XL              0x28
#define LSM6DS0_REG_OUT_X_H_XL              0x29
#define LSM6DS0_REG_OUT_Y_L_XL              0x2A
#define LSM6DS0_REG_OUT_Y_H_XL              0x2B
#define LSM6DS0_REG_OUT_Z_L_XL              0x2C
#define LSM6DS0_REG_OUT_Z_H_XL              0x2D
#define LSM6DS0_REG_FIFO_CTRL               0x2E
#define LSM6DS0_REG_FIFO_SRC                0x2F
#define LSM6DS0_REG_INT_GEN_CFG_G           0x30
#define LSM6DS0_REG_INT_THS_XH_G            0x31
#define LSM6DS0_REG_INT_THS_XL_G            0x32
#define LSM6DS0_REG_INT_THS_YH_G            0x33
#define LSM6DS0_REG_INT_THS_YL_G            0x34
#define LSM6DS0_REG_INT_THS_ZH_G            0x35
#define LSM6DS0_REG_INT_THS_ZL_G            0x36
#define LSM6DS0_REG_DUR_G                   0x37

// Status register masks
#define LSM6DS0_REG_STATUS_NEW_ACC_DATA     0x01
#define LSM6DS0_REG_STATUS_NEW_GYRO_DATA    0x02
#define LSM6DS0_REG_STATUS_NEW_TEMP_DATA    0x04

// FIFO SRC register mask
#define LSM6DS0_REG_FIFO_SRC_THR_STATUS     0x80
#define LSM6DS0_REG_FIFO_SRC_OVERRUN        0x40

#define LSM6DS0_AUTO_INCREMENT_BIT          0x80

// Buffers sizes
#define LSM6DS0_FIFO_SLOT_NUMBER		32
#define ACC_BUFFER_SIZE     			LSM6DS0_FIFO_SLOT_NUMBER
#define GYRO_BUFFER_SIZE     			LSM6DS0_FIFO_SLOT_NUMBER

typedef enum
{
	LSM6DS0_STATUS_RESET		= 0,
	LSM6DS0_STATUS_ON			= 1,
	LSM6DS0_STATUS_FIFO_OVR		= 2,
	LSM6DS0_STATUS_FIFO_ERR		= 3,
	LSM6DS0_STATUS_FIFO_EMPTY   = 4
} LSM6DS0_statusEnumTy;

typedef struct
{
    gap8 (4);

    tU8 ACT_THS ;
    tU8 ACT_DUR ;
    tU8 INT_GEN_CFG_XL ;
    tU8 INT_GEN_THS_X_XL ;
    tU8 INT_GEN_THS_Y_XL ;
    tU8 INT_GEN_THS_Z_XL ;
    tU8 INT_GEN_DUR_XL ;
    tU8 REFERENCE_G ;
    tU8 INT_CTRL ;

    gap8 (2);

    tU8 WHO_AM_I ;
    tU8 CTRL_REG1_G ;
    tU8 CTRL_REG2_G ;
    tU8 CTRL_REG3_G ;
    tU8 ORIENT_G ;
    tU8 INT_GEN_SRC_G ;
    tU8 OUT_TEMP_L ;
    tU8 OUT_TEMP_H ;
    tU8 STATUS_REG_G ;
    tU8 OUT_X_L_G ;
    tU8 OUT_X_H_G ;
    tU8 OUT_Y_L_G ;
    tU8 OUT_Y_H_G ;
    tU8 OUT_Z_L_G ;
    tU8 OUT_Z_H_G ;
    tU8 CTRL_REG4 ;
    tU8 CTRL_REG5_XL ;
    tU8 CTRL_REG6_XL ;
    tU8 CTRL_REG7_XL ;
    tU8 CTRL_REG8 ;
    tU8 CTRL_REG9 ;
    tU8 CTRL_REG10 ;

    gap8 (1);

    tU8 INT_GEN_SRC_XL ;
    tU8 STATUS_REG ;
    tU8 OUT_X_L_XL ;
    tU8 OUT_X_H_XL ;
    tU8 OUT_Y_L_XL ;
    tU8 OUT_Y_H_XL ;
    tU8 OUT_Z_L_XL ;
    tU8 OUT_Z_H_XL ;
    tU8 FIFO_CTRL ;
    tU8 FIFO_SRC ;
    tU8 INT_GEN_CFG_G ;
    tU8 INT_THS_XH_G ;
    tU8 INT_THS_XL_G ;
    tU8 INT_THS_YH_G ;
    tU8 INT_THS_YL_G ;
    tU8 INT_THS_ZH_G ;
    tU8 INT_THS_ZL_G ;
    tU8 DUR_G ;
} LSM6DS0_registersMappingTy;

typedef struct
{
    tS32 timeStampUs;
    tS16 acc_x;
    tS16 acc_y;
    tS16 acc_z;
} LSM6DS0_AccDataTy ;

typedef struct
{
    tS32 timeStampUs;
    tS16 pitch_x;
    tS16 roll_y;
    tS16 yaw_z;
} LSM6DS0_GyroDataTy ;

typedef struct
{
    float x;
    float y;
} pointTy;

typedef struct
{
    pointTy point1;
    pointTy point2;
} linearPointsTy;

struct LSM6DS0_sensitivityLevelsTy
{
	tSInt fullScale;
	float sensitivity;
};

static LSM6DS0_AccDataTy LSM6D0_accDataBuffer[ACC_BUFFER_SIZE];
static LSM6DS0_GyroDataTy LSM6D0_gyroDataBuffer[GYRO_BUFFER_SIZE];
static tU32 LSM6D0_accDataIdx = 0;
static tU32 LSM6D0_gyroDataIdx = 0;
static tU32 LSM6D0_accDataNum = ACC_BUFFER_SIZE;
static tU32 LSM6D0_gyroDataNum = ACC_BUFFER_SIZE;
static tU32 LSM6D0_accRng = 2; // +/- 2g
static tU32 LSM6D0_gyroRng = 245; // +/- 245dps
static tU32 LSM6D0_Odr = 0;
static float LSM6D0_accLsb;
static LSM6DS0_statusEnumTy LSM6DS0_status;

volatile static LSM6DS0_registersMappingTy LSM6DS0_registersMapping;
static struct timeval LSM6DS0_startTimeValue;
static tBool LSM6DS0_displayOnce = true;

#if (defined WRITE_TO_FILE)
	static ofstream LSM6DS0_reportFile;
#endif // #if (defined WRITE_TO_FILE)

static tS32 LSM6DS0_ReadStatusRegister (tBool *tempDataAvail, tBool *accDataAvail, tBool *gyroDataAvail);
static MAYBE_UNUSED tS32 LSM6DS0_ReadAccelerationRegister (LSM6DS0_AccDataTy *AccDataPnt);
static MAYBE_UNUSED tS32 LSM6DS0_ReadGyroscopeRegister (LSM6DS0_GyroDataTy *gyroDataPnt);
static MAYBE_UNUSED tS32 LSM6DS0_ReadAccelerationAndGyroscopeRegister (LSM6DS0_AccDataTy *AccDataPnt, LSM6DS0_GyroDataTy *gyroDataPnt);
static tS32 LSM6DS0_WriteControlRegisters (void);
static tS32 LSM6DS0_ReadId (tPU8 idVal);
static tU8 LSM6DS0_ReadFIFOSamplesNum (tBool *fifoOverrun, tBool *fifoThrStatus);
static void LSM6DS0_FifoReset (void);
static tS32 LSM6DS0_StoreData (tSInt memoryPoolId, tSInt accStartIndex, tSInt accReadLen, tSInt gyroStartIndex, tSInt gyroReadLen);
static MAYBE_UNUSED float LSM6DS0_CalculateAccelerationValue (tS16 accSample);
static MAYBE_UNUSED float LSM6DS0_CalculateAngularRateValue (tS16 gyroSample);
static long int LSM6DS0_CalculateDeltaTime (struct timeval *starttime, struct timeval *finishtime);

tS32 LSM6DS0_Open (void)
{
	// Start I2C mode
	bcm2835_i2c_begin ();

	// Setup I2C
	bcm2835_i2c_setSlaveAddress (LSM6DS0_I2C_ADD);
	bcm2835_i2c_setClockDivider (BCM2835_I2C_CLOCK_DIVIDER_626);

	return ERROR_CODE_NO_ERROR;
}

tS32 LSM6DS0_Close (void)
{
	// Close I2C mode
	bcm2835_i2c_end ();

	return ERROR_CODE_NO_ERROR;
}

tS32 LSM6DS0_Init (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	tU8 deviceId;

	// Initialize globals
	LSM6D0_accDataIdx = 0;
	LSM6D0_gyroDataIdx = 0;
	LSM6D0_accDataNum = ACC_BUFFER_SIZE;
	LSM6D0_gyroDataNum = ACC_BUFFER_SIZE;

	LSM6DS0_status = LSM6DS0_STATUS_RESET;

	LSM6DS0_displayOnce = true;

	memset (&LSM6D0_accDataBuffer, 0x00, sizeof (LSM6D0_accDataBuffer));
	memset (&LSM6D0_gyroDataBuffer, 0x00, sizeof (LSM6D0_gyroDataBuffer));

	// Get start point for delta time
	gettimeofday (&LSM6DS0_startTimeValue, NULL);

	// Open LSM6DS0 I2C access
	LSM6DS0_Open();

	// Read ID to identify the proper device
	res = LSM6DS0_ReadId (&deviceId);

	if (ERROR_CODE_NO_ERROR != res || LSM6DS0_DEVICE_ID != deviceId)
	{
		// Close LSM6DS0 I2C access
		LSM6DS0_Close ();

		return ERROR_CODE_GENERIC;
	}

	// Open report file
#if (defined WRITE_TO_FILE)
	LSM6DS0_reportFile.open ("lsm6ds0_report.txt", ios::out | ios::trunc);

	if (false == LSM6DS0_reportFile.is_open ())
	{
		// Close LSM6DS0 I2C access
		LSM6DS0_Close ();

		return ERROR_CODE_GENERIC;
	}

	// Write file first line
	LSM6DS0_reportFile << "LSM6DS0 DATA REPORT" << '\n';
#endif // #if (defined WRITE_TO_FILE)

//	// Get the shared memory area (from services)
//	LSM6DS0_sharedMemPtr = IPC_SharedMemoryGetDataPointer ();
//	LSM6DS0_sharedMemIndex = 0;

	// Write control registers
	LSM6DS0_WriteControlRegisters ();

	// Close report file
#if (defined WRITE_TO_FILE)
	LSM6DS0_reportFile.close ();
#endif // #if (defined WRITE_TO_FILE)

	// Close LSM6DS0 I2C access
	LSM6DS0_Close ();

	return res;
}

tS32 LSM6DS0_ReadAccelerationSequence (tSInt memoryPoolId)
{
	tU8 FIFOSamplesNum ;
	tS32 res = ERROR_CODE_NO_ERROR;
	tBool tempDataAvail, accDataAvail, gyroDataAvail;
	tBool fifoverrunOccurred, fifoThrReached;
	tSInt accStartIndex, gyroStartIndex;
	tSInt accReadLen, gyroReadLen;
	struct timeval CurrTaskTime;
	struct timeval startTime, endTime;
	long int deltaInMicroSec;
	static long int biggerTime = 0;
	static tS32 numberOfTimeAcq = 0;
	static tS32 maxSamplesReceived = 0;

	// Initialize temporary index to the global ones for file write
	accStartIndex = LSM6D0_accDataIdx;
	accReadLen = 0;

	gyroStartIndex = LSM6D0_gyroDataIdx;
	gyroReadLen = 0;

	// Open LSM6DS0 I2C access
	LSM6DS0_Open ();

	// Read status register
	LSM6DS0_ReadStatusRegister (&tempDataAvail, &accDataAvail, &gyroDataAvail);

	// Check if we have samples in the FIFO for acceleration
	FIFOSamplesNum = LSM6DS0_ReadFIFOSamplesNum (&fifoverrunOccurred, &fifoThrReached);

	//printf ("Acc avail: %d, Gyro avail: %d, Temp avail: %d, Samples #: %d\n", accDataAvail, gyroDataAvail, tempDataAvail, FIFOSamplesNum);

	// If we have an overrun then we clear the FIFO and go on,
	// otherwise we get all samples we have
	if (true == fifoverrunOccurred)
	{
		// Display error
		if (LSM6DS0_STATUS_FIFO_OVR != LSM6DS0_status)
		{
			printf ("FIFO overrun\n");
		}

		// Set device status to FIFO OVERRUN
		LSM6DS0_status = LSM6DS0_STATUS_FIFO_OVR;

		// Reset FIFO
		//LSM6DS0_FifoReset ();

		// Reset max samples
		maxSamplesReceived = 0;

		// Reset bigger time
		biggerTime = 0;
	}

	if (0 == FIFOSamplesNum && true == accDataAvail)
	{
		// We have unread samples and FIFO is empty? Sounds like an error
		if (LSM6DS0_STATUS_FIFO_ERR != LSM6DS0_status)
		{
			printf ("ERROR: FIFO is empty but we have samples\n");
		}

		// Set device status to FIFO ERROR
		LSM6DS0_status = LSM6DS0_STATUS_FIFO_ERR;

		// Reset FIFO
		LSM6DS0_FifoReset ();
	}
	else if (0 == FIFOSamplesNum)
	{
		// No samples in the FIFO, we are too fast
		if (LSM6DS0_STATUS_FIFO_EMPTY != LSM6DS0_status)
		{
			// We do nto take action: it could happen
		}

		// Set device status to FIFO ERROR
		LSM6DS0_status = LSM6DS0_STATUS_FIFO_EMPTY;
	}
	else
	{
		// Get operation start time
		gettimeofday ( &CurrTaskTime , NULL );
		startTime.tv_sec = CurrTaskTime.tv_sec;
		startTime.tv_usec = CurrTaskTime.tv_usec;

		// Display new status
		if (true == LSM6DS0_displayOnce)
		{
			//printf ("Samples received\n");

			LSM6DS0_displayOnce = false;
		}

		// Set device status to ON, device is working as expected
		LSM6DS0_status = LSM6DS0_STATUS_ON;

		// Save number of samples received
		if (maxSamplesReceived < FIFOSamplesNum)
		{
			maxSamplesReceived = FIFOSamplesNum;

			//printf ("Max S: %d\n", maxSamplesReceived);
		}

		// Now read samples
		while (0 != FIFOSamplesNum--)
		{
#if 1
			LSM6DS0_ReadAccelerationAndGyroscopeRegister (&LSM6D0_accDataBuffer[LSM6D0_accDataIdx],
													      &LSM6D0_gyroDataBuffer[LSM6D0_gyroDataIdx]);

			accReadLen++;
			gyroReadLen++;

			if (LSM6D0_accDataIdx < (LSM6D0_accDataNum - 1))
			{
				LSM6D0_accDataIdx++;
			}
			else
			{
				LSM6D0_accDataIdx = 0;
			}

			if (LSM6D0_gyroDataIdx < (LSM6D0_gyroDataNum - 1))
			{
				LSM6D0_gyroDataIdx++;
			}
			else
			{
				LSM6D0_gyroDataIdx = 0;
			}
#else
			// Acceleration data
			if (LSM6D0_accDataIdx < LSM6D0_accDataNum)
			{
				accReadLen++;
				LSM6DS0_ReadAccelerationRegister (&LSM6D0_accDataBuffer[LSM6D0_accDataIdx++]); // Read acceleration data
			}
			else if (LSM6D0_accDataIdx == LSM6D0_accDataNum)
			{
				// We would like to restart in a circular way for now
				LSM6D0_accDataIdx = 0;
			}

			// Gyroscope data
			if (LSM6D0_gyroDataIdx < LSM6D0_gyroDataNum)
			{
				gyroReadLen++;
				LSM6DS0_ReadGyroscopeRegister (&LSM6D0_gyroDataBuffer[LSM6D0_gyroDataIdx++]);  // Read Gyroscope data
			}
			else if (LSM6D0_gyroDataIdx == LSM6D0_gyroDataNum)
			{
				// We would like to restart in a circular way for now
				LSM6D0_gyroDataIdx = 0;
			}
#endif // #if 1
		}

		// Write data to file
		LSM6DS0_StoreData (memoryPoolId, accStartIndex, accReadLen, gyroStartIndex, gyroReadLen);

		// Debug stuff: monitor delta time between acquisitions
		gettimeofday ( &CurrTaskTime , NULL );
		endTime.tv_sec = CurrTaskTime.tv_sec;
		endTime.tv_usec = CurrTaskTime.tv_usec;

		deltaInMicroSec = LSM6DS0_CalculateDeltaTime (&startTime, &endTime);

		numberOfTimeAcq++;

		if (deltaInMicroSec > biggerTime)
		{
			//printf ("Max T: %ld ms\n", deltaInMicroSec);

			biggerTime = deltaInMicroSec;
		}
	}

	// Close LSM6DS0 I2C access
	LSM6DS0_Close ();

	return res;
}

static tS32 LSM6DS0_StoreData (tSInt memoryPoolId, tSInt accStartIndex, tSInt accReadLen, tSInt gyroStartIndex, tSInt gyroReadLen)
{
	// Depending on configuration we write data on file or on shared memory
	// for delayed write by a service thread
#if (defined WRITE_TO_FILE)
	// Write data to file
	struct timeval curTimeValue;
	std::stringstream textStream;
	float mSec;

	LSM6DS0_reportFile.open ("lsm6ds0_report.txt", ios::out | ios::app);

	if (false == LSM6DS0_reportFile.is_open ())
	{
		return ERROR_CODE_GENERIC;
	}

	// Get current time
	gettimeofday (&curTimeValue, NULL);

    mSec = (curTimeValue.tv_sec - LSM6DS0_startTimeValue.tv_sec) * 1000 +
    	   (curTimeValue.tv_usec - LSM6DS0_startTimeValue.tv_usec) / (float)1000;

	// Write result to CSV file, format:
	// <time stamp> - <acceleration X Y Z> - <gyroscope X Y Z>
    textStream.str (std::string());
    textStream.clear();

    while (accReadLen > 0 || gyroReadLen > 0)
    {
		textStream << mSec << "," << "Acc X:  ," << LSM6D0_accDataBuffer[accStartIndex].acc_x << "," \
								  << "Acc Y:  ," << LSM6D0_accDataBuffer[accStartIndex].acc_y << "," \
								  << "Acc Z:  ," << LSM6D0_accDataBuffer[accStartIndex].acc_z << "," \
								  << "Gyro X: ," << LSM6D0_gyroDataBuffer[gyroStartIndex].pitch_x << "," \
								  << "Gyro Y: ," << LSM6D0_gyroDataBuffer[gyroStartIndex].roll_y << "," \
								  << "Gyro Z: ," << LSM6D0_gyroDataBuffer[gyroStartIndex].yaw_z  << "," << '\n';

		accReadLen--;
		gyroReadLen--;

		if (accStartIndex < (ACC_BUFFER_SIZE - 1))
		{
			accStartIndex++;
		}
		else
		{
			accStartIndex = 0;
		}

		if (gyroStartIndex < (GYRO_BUFFER_SIZE - 1))
		{
			gyroStartIndex++;
		}
		else
		{
			gyroStartIndex = 0;
		}
    }

	std::string tmpStr = textStream.str();
	LSM6DS0_reportFile << tmpStr;

	// Close report file
	LSM6DS0_reportFile.close ();
#else
	if (accReadLen > 0 || gyroReadLen > 0)
	{
		// Write data on shared memory
		struct timeval curTimeValue;
		float mSec, accValueX, accValueY, accValueZ;
#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
		float accValueG;
		tSInt slot = 0;
		IPC_sharedMemoryElemTy *sharedMemPtr;
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)

		// Get current time
		gettimeofday (&curTimeValue, NULL);

		mSec = (curTimeValue.tv_sec - LSM6DS0_startTimeValue.tv_sec) * 1000 +
			   (curTimeValue.tv_usec - LSM6DS0_startTimeValue.tv_usec) / (float)1000;

#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
		// Get the pointer to an available memory pointer
		sharedMemPtr = IPC_SharedMemoryGetWriterDataPointer ();
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)

		// Save the data
		while (accReadLen > 0 || gyroReadLen > 0)
		{
#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
			///
			// Store data to the shared memory
			///
			// Store acceleration values
			//    We need to calculate the value of acceleration in 'g', doing this we
			//    can apply the current scale and there's no need to propagate this information
			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_x);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_X;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_y);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_Y;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			accValueG = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_z);
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_ACC_Z;
			sharedMemPtr->dataBfr[slot++].data = accValueG;

			// Store gyroscope values
			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_X;
			sharedMemPtr->dataBfr[slot++].data = LSM6D0_gyroDataBuffer[gyroStartIndex].pitch_x;

			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_Y;
			sharedMemPtr->dataBfr[slot++].data = LSM6D0_gyroDataBuffer[gyroStartIndex].roll_y;

			sharedMemPtr->dataBfr[slot].type = IPC_DATA_IS_GYRO_Z;
			sharedMemPtr->dataBfr[slot++].data = LSM6D0_gyroDataBuffer[gyroStartIndex].yaw_z;
#elif (defined APP_USE_MEMORYPOOL)
			///
			// Write the data to the locked memory pool
			///
			accValueX = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_x);
			accValueY = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_y);
			accValueZ = LSM6DS0_CalculateAccelerationValue (LSM6D0_accDataBuffer[accStartIndex].acc_z);

			IPC_MemoryPoolPush (memoryPoolId, mSec, accValueX, accValueY, accValueZ);
#else
			// No ouptut method is used, data is not saved
			C_UNUSED (mSec);
			C_UNUSED (accValueX);
			C_UNUSED (accValueY);
			C_UNUSED (accValueZ);
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)

			accReadLen--;
			gyroReadLen--;

			if (accStartIndex < (ACC_BUFFER_SIZE - 1))
			{
				accStartIndex++;
			}
			else
			{
				accStartIndex = 0;
			}

			if (gyroStartIndex < (GYRO_BUFFER_SIZE - 1))
			{
				gyroStartIndex++;
			}
			else
			{
				gyroStartIndex = 0;
			}
		}

#if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
		sharedMemPtr->writerId = APP_THREAD_LSM6DS0_ID;
		sharedMemPtr->time = mSec;
		sharedMemPtr->len = slot;
#endif // #if (defined WRITE_DELAYED_USING_SHARED_MEMORIES)
	}
#endif // #if (defined WRITE_TO_FILE)

	return ERROR_CODE_NO_ERROR;
}

static tS32 LSM6DS0_ReadStatusRegister (tBool *tempDataAvail, tBool *accDataAvail, tBool *gyroDataAvail)
{
  tS32 readRes;
  tChar rdStatusBfr[3] = {0x00, 0x00, 0x00};

  readRes = I2C_GetData (LSM6DS0_REG_STATUS_REG, 1, rdStatusBfr);
  LSM6DS0_registersMapping.STATUS_REG = rdStatusBfr[0];

  if (LSM6DS0_registersMapping.STATUS_REG & LSM6DS0_REG_STATUS_NEW_ACC_DATA)
  {
	  *accDataAvail = true;
  }
  else
  {
	  *accDataAvail = false;
  }

  if (LSM6DS0_registersMapping.STATUS_REG & LSM6DS0_REG_STATUS_NEW_GYRO_DATA)
  {
	  *gyroDataAvail = true;
  }
  else
  {
	  *gyroDataAvail = false;
  }

  if (LSM6DS0_registersMapping.STATUS_REG & LSM6DS0_REG_STATUS_NEW_TEMP_DATA)
  {
	  *tempDataAvail = true;
  }
  else
  {
	  *tempDataAvail = false;
  }

  return readRes;
}

static tU8 LSM6DS0_ReadFIFOSamplesNum (tBool *fifoOverrun, tBool *fifoThrStatus)
{
	tS32 readRes;
	tChar rdStatusBfr[3] = {0x00, 0x00, 0x00};

	readRes = I2C_GetData (LSM6DS0_REG_FIFO_SRC, 1, rdStatusBfr);
	LSM6DS0_registersMapping.FIFO_SRC = rdStatusBfr[0];

	if (LSM6DS0_registersMapping.FIFO_SRC & LSM6DS0_REG_FIFO_SRC_OVERRUN)
	{
		*fifoOverrun = true;
	}
	else
	{
		*fifoOverrun = false;
	}

	if (LSM6DS0_registersMapping.FIFO_SRC & LSM6DS0_REG_FIFO_SRC_THR_STATUS)
	{
		*fifoThrStatus = true;
	}
	else
	{
		*fifoThrStatus = false;
	}

	C_UNUSED(readRes);

	return ( (tU8) ( rdStatusBfr[0] & 0x3F ) ) ;
}

static void LSM6DS0_FifoReset (void)
{
	tChar wrBfr[10] = {0x00};

    // FIFO clear
    LSM6DS0_registersMapping.FIFO_CTRL = 0x00;
    wrBfr[0] = LSM6DS0_registersMapping.FIFO_CTRL;
    I2C_WriteData (LSM6DS0_REG_FIFO_CTRL, 1, wrBfr);
}

static tS32 MAYBE_UNUSED LSM6DS0_ReadAccelerationAndGyroscopeRegister (LSM6DS0_AccDataTy *AccDataPnt, LSM6DS0_GyroDataTy *gyroDataPnt)
{
	tS32 readRes;
	tChar rdBuffer[12];
	struct timeval CurrTaskTime ;

	gettimeofday ( &CurrTaskTime , NULL ) ;

	// We start reading from 0x28 so we use the automatic wrap of the auto-increment read
#if 0
	// Starting resgister: LSM6DS0_REG_OUT_X_L_XL or LSM6DS0_REG_OUT_X_L_G
	readRes = I2C_GetDataWithAutoIncrement (LSM6DS0_REG_OUT_X_L_G, 12, rdBuffer);

	gyroDataPnt->timeStampUs = CurrTaskTime.tv_usec;
	gyroDataPnt->pitch_x = (((int)rdBuffer[1]) << 8) + (int)rdBuffer[0];
	gyroDataPnt->roll_y = (((int)rdBuffer[3]) << 8) + (int)rdBuffer[2];
	gyroDataPnt->yaw_z = (((int)rdBuffer[5]) << 8) + (int)rdBuffer[4];

	AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;
	AccDataPnt->acc_x = (((int)rdBuffer[7]) << 8) + (int)rdBuffer[6];
	AccDataPnt->acc_y = (((int)rdBuffer[9]) << 8) + (int)rdBuffer[8];
	AccDataPnt->acc_z = (((int)rdBuffer[11]) << 8) + (int)rdBuffer[9];
#else
	// Starting resgister: LSM6DS0_REG_OUT_X_L_XL or LSM6DS0_REG_OUT_X_L_G
	readRes = I2C_GetDataWithAutoIncrement (LSM6DS0_REG_OUT_X_L_XL, 12, rdBuffer);

	AccDataPnt->timeStampUs = CurrTaskTime.tv_usec;
	AccDataPnt->acc_x = (((int)rdBuffer[1]) << 8) + (int)rdBuffer[0];
	AccDataPnt->acc_y = (((int)rdBuffer[3]) << 8) + (int)rdBuffer[2];
	AccDataPnt->acc_z = (((int)rdBuffer[5]) << 8) + (int)rdBuffer[4];

	gyroDataPnt->timeStampUs = CurrTaskTime.tv_usec;
	gyroDataPnt->pitch_x = (((int)rdBuffer[7]) << 8) + (int)rdBuffer[6];
	gyroDataPnt->roll_y = (((int)rdBuffer[9]) << 8) + (int)rdBuffer[8];
	gyroDataPnt->yaw_z = (((int)rdBuffer[11]) << 8) + (int)rdBuffer[9];
#endif // # if 0

	return readRes;
}

static MAYBE_UNUSED tS32 LSM6DS0_ReadAccelerationRegister (LSM6DS0_AccDataTy *AccDataPnt)
{
	tS32 readRes;
	tChar rdAccBuffer[6];
	struct timeval CurrTaskTime ;

	gettimeofday ( &CurrTaskTime , NULL ) ;
	readRes = I2C_GetDataWithAutoIncrement (LSM6DS0_REG_OUT_X_L_XL , 6 , rdAccBuffer); // Read status register

	AccDataPnt->timeStampUs = CurrTaskTime . tv_usec ;
	AccDataPnt->acc_x = ( ( (int)rdAccBuffer[1] ) << 8 ) + (int)rdAccBuffer[0] ;
	AccDataPnt->acc_y = ( ( (int)rdAccBuffer[3] ) << 8 ) + (int)rdAccBuffer[2] ;
	AccDataPnt->acc_z = ( ( (int)rdAccBuffer[5] ) << 8 ) + (int)rdAccBuffer[4] ;

	return readRes;
}

static MAYBE_UNUSED tS32 LSM6DS0_ReadGyroscopeRegister (LSM6DS0_GyroDataTy *gyroDataPnt)
{
	tS32 readRes;
	tChar rdGyroBuffer[6];
	struct timeval CurrTaskTime ;

	gettimeofday ( &CurrTaskTime , NULL ) ;
	readRes = I2C_GetDataWithAutoIncrement ( LSM6DS0_REG_OUT_X_L_G , 6 , rdGyroBuffer ) ; // Read status register

	gyroDataPnt->timeStampUs = CurrTaskTime . tv_usec ;
	gyroDataPnt->pitch_x = ( ( (int)rdGyroBuffer[1] ) << 8 ) + (int)rdGyroBuffer[0] ;
	gyroDataPnt->roll_y = ( ( (int)rdGyroBuffer[3] ) << 8 ) + (int)rdGyroBuffer[2] ;
	gyroDataPnt->yaw_z = ( ( (int)rdGyroBuffer[5] ) << 8 ) + (int)rdGyroBuffer[4] ;

	return readRes;
}

static tS32 LSM6DS0_WriteControlRegisters (void)
{
	tS32 res = ERROR_CODE_NO_ERROR;
	tChar wrBfr[10] = {0x00};
	tU8 Reg20val;
	tChar feedbackBuffer;
	tChar readRes;

	// Accelerometer disable
	LSM6DS0_registersMapping.CTRL_REG5_XL = 0x00;
	wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG5_XL;
	I2C_WriteData (LSM6DS0_REG_CTRL_REG5_XL, 1, wrBfr);

	// Gyroscope disable
	LSM6DS0_registersMapping.CTRL_REG4 = 0x00;
	wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG4;
	I2C_WriteData (LSM6DS0_REG_CTRL_REG4, 1, wrBfr);

	// ODR (OutputDataRate) = 110 -> 952 Hz
	switch (LSM6D0_accRng)
	{
		 case 16:
			  Reg20val = 0xC8;
		 break;

		 case 8:
			  Reg20val = 0xD8;
		 break;

		 case 4:
			  Reg20val = 0xD0;
		 break;

		 default:
			  Reg20val = 0xC0;
		 break;
	}

    LSM6D0_accLsb = (2.0 * LSM6D0_accRng) / 0x10000;

	// Accelerometer bandwidth selection (when only accelerometer is enabled):
	//         Sampling rate [Hz]    Anti-aliasing [Hz]
	// - 0xC0: 952                   408
	// - 0x40: 50                    408
	LSM6DS0_registersMapping.CTRL_REG6_XL = Reg20val; // 0x40
	wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG6_XL;
	I2C_WriteData (LSM6DS0_REG_CTRL_REG6_XL, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG6_XL, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG6_XL feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG6_XL)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG6_XL");
	}

	// Gyroscope bandwidth selection:
	//         Sampling rate [Hz]    Cutoff [Hz]
	// - 0xC3: 952                   33
	// - 0xC0: 952                   100
	// - 0x60: 119					 14
	// - 0xA0: 476					 21
	// - 0x40: 59.5                  16
    LSM6DS0_registersMapping.CTRL_REG1_G = 0xC0; // 0xA0;
    wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG1_G;
    I2C_WriteData (LSM6DS0_REG_CTRL_REG1_G, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG1_G, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG1_G feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG1_G)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG1_G");
	}

    // Avoid incrementing in the middle of the read and let auto-increment
    LSM6DS0_registersMapping.CTRL_REG8 = 0x44;
    wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG8;
    I2C_WriteData (LSM6DS0_REG_CTRL_REG8, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG8, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG8 feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG8)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG8\n");
	}

    // FIFO enable
    LSM6DS0_registersMapping.CTRL_REG9 = 0x02;
    wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG9;
    I2C_WriteData (LSM6DS0_REG_CTRL_REG9, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG9, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG9 feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG9)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG9\n");
	}

    // FIFO clear
    LSM6DS0_registersMapping.FIFO_CTRL = 0x00;
    wrBfr[0] = LSM6DS0_registersMapping.FIFO_CTRL;
    I2C_WriteData (LSM6DS0_REG_FIFO_CTRL, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_FIFO_CTRL, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_FIFO_CTRL feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.FIFO_CTRL)
	{
		printf ("Error writing LSM6DS0_REG_FIFO_CTRL\n");
	}

    // FIFO mode (set FIFO level to 24)
	// Select mode:
	// - 0x38: FIFO mode with threshold 24 samples
	// - 0xC8: continuous mode with threshold 24 samples
    LSM6DS0_registersMapping.FIFO_CTRL = 0xC8; // 0x38;
    wrBfr[0] = LSM6DS0_registersMapping.FIFO_CTRL;
    I2C_WriteData (LSM6DS0_REG_FIFO_CTRL, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_FIFO_CTRL, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_FIFO_CTRL feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.FIFO_CTRL)
	{
		printf ("Error writing LSM6DS0_REG_FIFO_CTRL\n");
	}

    // Gyroscope enable
    LSM6DS0_registersMapping.CTRL_REG4 = 0x38;
    wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG4;
    I2C_WriteData (LSM6DS0_REG_CTRL_REG4, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG4, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG4 feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG4)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG4\n");
	}

    // Accelerometer enable, no decimation
    LSM6DS0_registersMapping.CTRL_REG5_XL = 0x38;
    wrBfr[0] = LSM6DS0_registersMapping.CTRL_REG5_XL;
    I2C_WriteData (LSM6DS0_REG_CTRL_REG5_XL, 1, wrBfr);

    readRes = I2C_GetData (LSM6DS0_REG_CTRL_REG5_XL, 1, (char *)&feedbackBuffer);
	printf ("LSM6DS0_REG_CTRL_REG5_XL feedback = %d : 0x%x\n", readRes, feedbackBuffer);

	if (feedbackBuffer != LSM6DS0_registersMapping.CTRL_REG5_XL)
	{
		printf ("Error writing LSM6DS0_REG_CTRL_REG5_XL\n");
	}

	// Set device status to ON
	LSM6DS0_status = LSM6DS0_STATUS_ON;

	return res;
}

static tS32 LSM6DS0_ReadId (tPU8 idVal)
{
	tU8 readRes;
	volatile tChar initRdBfr [5] = {0x00, 0x00, 0x00, 0x00, 0x00};

    readRes = I2C_GetData (LSM6DS0_REG_WHO_AM_I, 1, (char *)&initRdBfr[0]);
	printf ("LSM6DS0 Who Am I reg read result = %d : 0x%x\n", readRes, initRdBfr[0]);

	*idVal = initRdBfr[0];

	return ERROR_CODE_NO_ERROR;
}

void LSM6DS0_AccDataDisplay ( void )
{
	int DisplaySamplesIdx ;
	int BaseTimeStamp ;
	int PrevTimeStamp ;
	std::stringstream textStream;
	LSM6DS0_AccDataTy *AccDataPnt = LSM6D0_accDataBuffer ;
	float AccX_g ;
	float AccY_g ;
	float AccZ_g ;
	std::string tmpStr ;

	BaseTimeStamp = AccDataPnt -> timeStampUs ;
	PrevTimeStamp = BaseTimeStamp ;

#if (defined WRITE_TO_FILE)
    // Open report file
    LSM6DS0_reportFile.open ("lsm6ds0_report.txt", ios::out | ios::app);

    if (false == LSM6DS0_reportFile.is_open ())
    {
        // Close LSM6DS0 I2C access
        LSM6DS0_Close ();

        return ;
    }
#endif // #if (defined WRITE_TO_FILE)

    // Write file first line
	printf ( "Samples list\n" ) ;

	for ( DisplaySamplesIdx = 0 ; DisplaySamplesIdx < (tS32)LSM6D0_accDataNum ; DisplaySamplesIdx++ )
	{
		// Write result to CSV file
        AccX_g = LSM6D0_accLsb * ( ( (float) AccDataPnt -> acc_x ) ) ;
        AccY_g = LSM6D0_accLsb * ( ( (float) AccDataPnt -> acc_y ) ) ;
        AccZ_g = LSM6D0_accLsb * ( ( (float) AccDataPnt -> acc_z ) ) ;

        textStream.str (std::string());
        textStream.clear();
        textStream << AccDataPnt -> timeStampUs << "," << AccX_g << "," << AccY_g << "," << AccZ_g << '\n';
        tmpStr = textStream.str();
        cout << tmpStr;

#if (defined WRITE_TO_FILE)
        LSM6DS0_reportFile << tmpStr;
#endif // #if (defined WRITE_TO_FILE)

        PrevTimeStamp = AccDataPnt -> timeStampUs ;
        AccDataPnt++ ;
	} /* endif */

#if (defined WRITE_TO_FILE)
	// Close report file
	LSM6DS0_reportFile.close ();
#endif // #if (defined WRITE_TO_FILE)

	C_UNUSED(PrevTimeStamp);
}

void LSM6DS0_SetAccSamplesNum ( int AccSamplesNum )
{
	if ( AccSamplesNum >= ACC_BUFFER_SIZE )
	{
	   AccSamplesNum = ACC_BUFFER_SIZE ;
	} /* endif */

	LSM6D0_accDataNum = AccSamplesNum ;
}

void LSM6DS0_SetAccRange ( int AccelerationRange )
{
	LSM6D0_accRng = AccelerationRange ;
}

void LSM6DS0_SetOutputDataRate ( int OutputDatarate )
{
	// TODO: fix with proper values, the passed could be not precise
	// TODO: implement this, the set value is not used at all
    LSM6D0_Odr = OutputDatarate ;
}

static long int LSM6DS0_CalculateDeltaTime (struct timeval *starttime, struct timeval *finishtime)
{
  long int msec;

  msec = (finishtime->tv_sec - starttime->tv_sec) * 1000;
  msec += (finishtime->tv_usec - starttime->tv_usec) / 1000;

  return msec;
}

static MAYBE_UNUSED float LSM6DS0_CalculateAccelerationValue (tS16 accSample)
{
	const LSM6DS0_sensitivityLevelsTy LSM6DS0_sensitivityLevelsAccelerometer[] =
	{
			{2,  0.061E-3},
			{4,  0.122E-3},
			{8, 0.244E-3},
			{16, 0.732E-3}
	};

	// We set initial value to 0 to make sure ot return something valid if range was not properly set
	float accValueG = 0;

	if (2 == LSM6D0_accRng)
	{
		accValueG = (float)accSample * LSM6DS0_sensitivityLevelsAccelerometer[0].sensitivity;
	}
	else if (4 == LSM6D0_accRng)
	{
		accValueG = (float)accSample * LSM6DS0_sensitivityLevelsAccelerometer[1].sensitivity;
	}
	else if (8 == LSM6D0_accRng)
	{
		accValueG = (float)accSample * LSM6DS0_sensitivityLevelsAccelerometer[2].sensitivity;
	}
	else if (16 == LSM6D0_accRng)
	{
		accValueG = (float)accSample * LSM6DS0_sensitivityLevelsAccelerometer[3].sensitivity;
	}

	return accValueG;
}

static MAYBE_UNUSED float LSM6DS0_CalculateAngularRateValue (tS16 gyroSample)
{
	const LSM6DS0_sensitivityLevelsTy LSM6DS0_sensitivityLevelsGyroscope[] =
	{
			{245,  8.75E-3},
			{500,  17.50E-3},
			{2000, 70E-3}
	};

	// We set initial value to 0 to make sure ot return something valid if range was not properly set
	float accValueG = 0;

	if (245 == LSM6D0_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS0_sensitivityLevelsGyroscope[0].sensitivity;
	}
	else if (500 == LSM6D0_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS0_sensitivityLevelsGyroscope[1].sensitivity;
	}
	else if (2000 == LSM6D0_gyroRng)
	{
		accValueG = (float)gyroSample * LSM6DS0_sensitivityLevelsGyroscope[2].sensitivity;
	}

	return accValueG;
}

// End of file
