/*
 * motor_thread.h
 *
 *  Created on: Oct 22, 2015
 *      Author: alberto
 */

#ifndef MOTOR_THREAD_H_
#define MOTOR_THREAD_H_

typedef enum
{
	MOTOR_CMD_NONE  		  = 0,
	MOTOR_CMD_START 		  = 1,
	MOTOR_CMD_STOP 	 	   	  = 2,
	MOTOR_CMD_DIR_FORWARD  	  = 3,
	MOTOR_CMD_DIR_BACKWARD    = 4,
	MOTOR_CMD_DIR_ACCELERATE  = 5,
	MOTOR_CMD_DIR_DECELERATE  = 6
} MOTOR_CommandEnumTy;

typedef enum
{
	MOTOR_DIRECTION_BACKWARD = -1,
	MOTOR_DIRECTION_NONE 	 = 0,
	MOTOR_DIRECTION_FORWARD  = 1
} MOTOR_directionTy;

extern tSInt MOTOR_Command (MOTOR_CommandEnumTy cmd);

extern void* MOTOR_Thread (void *arg);

extern void* MOTOR_ServiceThread (void *arg);

extern void MOTOR_ExitThread (bool exitThread);

extern int MOTOR_SignalHandler (void);

#endif /* MOTOR_THREAD_H_ */
