/*
 * lps25h_lld.h
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#ifndef LPS25H_LLD_H_
#define LPS25H_LLD_H_

extern tS32 LPS25H_Init (void);

extern tS32 LPS25H_Open (void);

extern tS32 LPS25H_Close (void);

extern tS32 LPS25H_ReadPressureAndTemperatureSequence (void);

#endif // LPS25H_LLD_H_

// End of file
