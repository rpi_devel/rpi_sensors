/*
 * lps25h_thread.h
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#ifndef LPS25H_THREAD_H_
#define LPS25H_THREAD_H_

extern void* LPS25H_Thread (void *arg);

extern void LPS25H_ExitThread (tBool exitThread);

extern tS32 LPS25H_ReadTemperatureAndPressureSequence (void);

extern tS32 LPS25H_SignalHandler (void);

#endif // LPS25H_THREAD_H_

// End of file
