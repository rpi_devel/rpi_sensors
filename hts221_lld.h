/*
 * hts221_lld.h
 *
 *  Created on: Mar 20, 2015
 *      Author: alberto
 */

#ifndef HTS221_LLD_H_
#define HTS221_LLD_H_

typedef struct
{
    float x;
    float y;
} pointTy;

typedef struct
{
    pointTy point1;
    pointTy point2;
} linearPointsTy;

extern float LinearInterpolation (linearPointsTy *points, float point0Read);

extern tS32 HTS221_Init (void);

extern tS32 HTS221_Open (void);

extern tS32 HTS221_Close (void);

extern tS32 HTS221_ReadHumidityAndTemperature (tBool readTemperature, tBool readHumidity);

extern tS32 HTS221_ReadTemperatureAndPressureSequence (void);

#endif // HTS221_LLD_H_

// End of file
