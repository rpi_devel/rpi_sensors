/*
 * ipc.cpp
 *
 *  Created on: Apr 28, 2015
 *      Author: alberto
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>		// For shared memory
#include <sys/mman.h>	// For shared memory
#include <sys/stat.h>   // For shared memory mode constants
#include <fcntl.h>      // For shared memory O_* constants
#include<iostream>

//#include <vector>
//#include <cmath>
//#include <boost/tuple/tuple.hpp>

#include "target_config.h"

#include "types.h"
#include "defines.h"

#include "ipc.h"

using namespace std;

// Data for pipes
#if (defined APP_USE_PIPES)
	#define IPC_INPUT_FIFO_NAME 			"/tmp/in_fifo"
	#define IPC_OUTPUT_FIFO_NAME 			"/tmp/out_fifo"

	static tSInt IPC_inputFifoHandle = -1;
	static tSInt IPC_outputFifoHandle = -1;
	static tU8 IPC_rxBuffer[IPC_FIFO_RX_BUFFER_SIZE];
	static tSInt IPC_receivedBytes = 0;
#endif // #if (defined APP_USE_PIPES)

// Data for shared memories
#if (defined APP_USE_SHAREDMEMS)
	static IPC_sharedMemoryRegionTy *IPC_sharedMemPtr;
	static tSInt IPC_sharedMemoryWriterIndex = 0;
	static tSInt IPC_sharedMemoryReaderIndex = 0;
	static int IPC_sharedMemFd;
#endif // #if (defined APP_USE_SHAREDMEMS)

#if (defined APP_USE_MEMORYPOOL)
	#define IPC_MEMORYPOOL_BUFFER_NUM			2

	struct IPC_memoryPoolDataTy
	{
		tBool used;
		IPC_memoryPoolStatusEnumTy status;
		dataVectTy dataVector;
	};

	struct IPC_memoryPoolDataTy IPC_memoryPoolData[IPC_MEMORYPOOL_BUFFER_NUM];
#endif // #if (defined APP_USE_MEMORYPOOL)

#if (defined APP_USE_PIPES)
tSInt IPC_FifoCreateAndOpen (void)
{
	tSInt result;

	printf("Making FIFOs...\n");

	// First step is removing files if they exists
	remove (IPC_INPUT_FIFO_NAME);
	remove (IPC_OUTPUT_FIFO_NAME);

///
// INPUT FIFO
///
	// This will fail if the FIFO already exists in the system from the app previously running, this is fine
	result = mkfifo (IPC_INPUT_FIFO_NAME, 0777);

	if (0 != result)
	{
		// We just go on, this is not a real error
	}

	printf ("Process %d opening FIFO %s\n", getpid(), IPC_INPUT_FIFO_NAME);

	// Possible flags:
	//	O_RDONLY - Open for reading only.
	//	O_WRONLY - Open for writing only.
	//	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately
	//                                          with a failure status if there is no input immediately available (instead of blocking).
	//											Likewise, write requests can also return
	//											immediately with a failure status if the output can't be written immediately
	IPC_inputFifoHandle = open (IPC_INPUT_FIFO_NAME, (O_RDONLY | O_NONBLOCK));

	if (-1 == IPC_inputFifoHandle)
	{
		return ERROR_CODE_GENERIC;
	}

	printf("Opened FIFO for input: %i\n", IPC_inputFifoHandle);

///
// OUTPUT FIFO
///
#if 1
	// This will fail if the FIFO already exists in the system from the app previously running, this is fine
	result = mkfifo (IPC_OUTPUT_FIFO_NAME, 0777);

	if (0 != result)
	{
		// We just go on, this is not a real error
	}

	printf ("Process %d opening FIFO %s\n", getpid(), IPC_OUTPUT_FIFO_NAME);

	// Possible flags:
	//	O_RDONLY - Open for reading only.
	//	O_WRONLY - Open for writing only.
	//	O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately
	//                                          with a failure status if there is no input immediately available (instead of blocking).
	//											Likewise, write requests can also return
	//											immediately with a failure status if the output can't be written immediately
	IPC_outputFifoHandle = open (IPC_OUTPUT_FIFO_NAME, (O_RDWR | O_NONBLOCK));

	if (-1 == IPC_outputFifoHandle)
	{
		return ERROR_CODE_GENERIC;
	}

	printf("Opened FIFO for output: %i\n", IPC_outputFifoHandle);
#endif // #if 0

	return ERROR_CODE_NO_ERROR;
}

tSInt IPC_FifoCheckReceivedBytes (void)
{
	// Read up to 255 characters from the port if they are there
	if (-1 != IPC_inputFifoHandle)
	{
		IPC_receivedBytes = read (IPC_inputFifoHandle, (void*)IPC_rxBuffer, 255);

		if (IPC_receivedBytes < 0)
		{
			// An error occurred (this can happen)
			printf("FIFO Read error\n");

			// Set received bytes to -1 to indicate an error (errors are negative)
			IPC_receivedBytes = -1;
		}
		else if (IPC_receivedBytes == 0)
		{
			// No data waiting
		}
		else
		{
			// Bytes received
			IPC_rxBuffer[IPC_receivedBytes] = '\0';

			printf("FIFO %i bytes read : %s\n", IPC_receivedBytes, IPC_rxBuffer);
		}
	}

	return IPC_receivedBytes;
}

tSInt IPC_FifoGetBytes (tPU8 dataPtr, tSInt maxData)
{
	tSInt rxBytes = IPC_receivedBytes;
	tSInt dataToCopy = IPC_receivedBytes;

	if (maxData < IPC_receivedBytes)
	{
		dataToCopy = maxData;
	}

	memcpy (dataPtr, &IPC_rxBuffer[0], dataToCopy);

	IPC_receivedBytes = 0;

	return rxBytes;
}

tSInt IPC_FifoWrite (void)
{
	tSInt transmittedBytes = 0;
	char IPC_txBuffer[10];

	strcpy (IPC_txBuffer, "I'm here");
	strcat (IPC_txBuffer, "\0");

	// Write bytes
	if (-1 != IPC_outputFifoHandle)
	{
		printf("FIFO writing...\n");

		transmittedBytes = strlen(IPC_txBuffer) + 1;

		write (IPC_outputFifoHandle, IPC_txBuffer, transmittedBytes);

		printf("FIFO %i bytes written : %s\n", transmittedBytes, IPC_txBuffer);
	}

	return transmittedBytes;
}

tSInt IPC_FifoClose (void)
{
	tSInt res;

	// If INPUT FIFO has a valid handle close it
	if (-1 == IPC_inputFifoHandle)
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		// Read remaining bytes and unlink
		read (IPC_inputFifoHandle, (void*)IPC_rxBuffer, 255);

		res = unlink (IPC_INPUT_FIFO_NAME);

		if (0 != res)
		{
			printf ("FIFO unlink failed for INPUT FIFO\n");
		}

		IPC_inputFifoHandle = -1;
	}

	// If OUTPUT FIFO has a valid handle close it
	if (-1 == IPC_outputFifoHandle)
	{
		return ERROR_CODE_GENERIC;
	}
	else
	{
		unlink (IPC_OUTPUT_FIFO_NAME);

		if (0 != res)
		{
			printf ("FIFO unlink failed for OUTPUT FIFO\n");
		}

		IPC_outputFifoHandle = -1;
	}

	return ERROR_CODE_NO_ERROR;
}
#endif // #if (defined APP_USE_PIPES)

#if (defined APP_USE_SHAREDMEMS)
tSInt IPC_SharedMemoryCreateAndOpen (void)
{
	// Create shared memory object and set its size
	printf ("Creating shared memory\n");

	IPC_sharedMemFd = shm_open ("/shared_mem", (O_RDWR | O_CREAT), (S_IRUSR | S_IWUSR));
	if (-1 == IPC_sharedMemFd)
	{
		printf ("Error creating shared memory\n");

		return ERROR_CODE_GENERIC;
	}

	if (-1 == ftruncate (IPC_sharedMemFd, sizeof (IPC_sharedMemoryRegionTy)))
	{
		printf ("Error sizing the shared memory\n");

		return ERROR_CODE_GENERIC;
	}

	IPC_sharedMemPtr = (IPC_sharedMemoryRegionTy *)mmap (NULL, sizeof(IPC_sharedMemoryRegionTy), (PROT_READ | PROT_WRITE), MAP_SHARED, IPC_sharedMemFd, 0);
	if (MAP_FAILED == IPC_sharedMemPtr)
	{
		printf ("Error mapping shared memory\n");

		return ERROR_CODE_GENERIC;
	}

	// Set writer pointer to begin of shared memory space
	IPC_sharedMemoryWriterIndex = 0;
	IPC_sharedMemoryReaderIndex = 0;

	return ERROR_CODE_NO_ERROR;
}

IPC_sharedMemoryRegionTy *IPC_SharedMemoryGetDataPointer (void)
{
	return IPC_sharedMemPtr;
}

IPC_sharedMemoryElemTy *IPC_SharedMemoryGetWriterDataPointer (void)
{
	IPC_sharedMemoryElemTy *resPtr;

	resPtr = &IPC_sharedMemPtr->entry[IPC_sharedMemoryWriterIndex];

	resPtr->used = true;

	if (IPC_sharedMemoryWriterIndex < (IPC_SHARED_MEMORY_ENTRIES_NUM - 1))
	{
		IPC_sharedMemoryWriterIndex++;
	}
	else
	{
		IPC_sharedMemoryWriterIndex = 0;
	}

	return resPtr;
}

IPC_sharedMemoryElemTy *IPC_SharedMemoryGetReaderDataPointer (void)
{
	tBool found = false;
	IPC_sharedMemoryElemTy *resPtr = (IPC_sharedMemoryElemTy *)NULL;

	// Check if currently reader point is used
	if (true == IPC_sharedMemPtr->entry[IPC_sharedMemoryReaderIndex].used)
	{
		resPtr = &IPC_sharedMemPtr->entry[IPC_sharedMemoryReaderIndex];

		resPtr->used = false;

		if (IPC_sharedMemoryReaderIndex < (IPC_SHARED_MEMORY_ENTRIES_NUM - 1))
		{
			IPC_sharedMemoryReaderIndex++;
		}
		else
		{
			IPC_sharedMemoryReaderIndex = 0;
		}

		found = true;
	}

	// Now check if something is used (re-align)
	if (false == found)
	{
		// Re-align
		if (IPC_sharedMemoryWriterIndex > 0)
		{
			IPC_sharedMemoryReaderIndex = IPC_sharedMemoryWriterIndex - 1;
		}
		else if (0 == IPC_sharedMemoryWriterIndex)
		{
			IPC_sharedMemoryReaderIndex = (IPC_SHARED_MEMORY_ENTRIES_NUM - 1);
		}

		// Re-check
		if (true == IPC_sharedMemPtr->entry[IPC_sharedMemoryReaderIndex].used)
		{
			resPtr = &IPC_sharedMemPtr->entry[IPC_sharedMemoryReaderIndex];

			resPtr->used = false;

			if (IPC_sharedMemoryReaderIndex < (IPC_SHARED_MEMORY_ENTRIES_NUM - 1))
			{
				IPC_sharedMemoryReaderIndex++;
			}
			else
			{
				IPC_sharedMemoryReaderIndex = 0;
			}

			found = true;
		}
	}

	return resPtr;
}
#endif // #if (defined APP_USE_SHAREDMEMS)

#if (defined APP_USE_MEMORYPOOL)
#if 0
template <typename TYPE>
tSInt IPC_memoryPool::IPC_MemoryPoolPush (TYPE data)
{
    // Push the data into the
}

template tSInt IPC_memoryPool::IPC_MemoryPoolPush<int>(int);
template tSInt IPC_memoryPool::IPC_MemoryPoolPush<float>(float);
#endif // #if 0

tSInt IPC_MemoryPoolCreate (void)
{
	tSInt res = ERROR_CODE_NO_ERROR;
	tSInt cnt;

	for (cnt = 0; cnt < IPC_MEMORYPOOL_BUFFER_NUM; cnt++)
	{
		IPC_memoryPoolData[cnt].dataVector.clear ();
		IPC_memoryPoolData[cnt].status = IPC_MEMORY_POOL_FREE;
		IPC_memoryPoolData[cnt].used = false;
	}

#if 0
	// TODO: remove it is test only
	for (cnt = 0; cnt < 1000; cnt++)
	{
		IPC_MemoryPoolPush (0, (10 + cnt), 1.009);
	}
#endif

	return res;
}

tSInt IPC_MemoryPoolLockForWrite (void)
{
	// This function will return a negative number if no slot is available
	// for write locking
	tSInt res = ERROR_CODE_RESOURCE_UNAVAIL;
	tSInt cnt;

	// Check if a slot is free and in case lock it for write and return the slot number
	for (cnt = 0; cnt < IPC_MEMORYPOOL_BUFFER_NUM; cnt++)
	{
		if (false == IPC_memoryPoolData[cnt].used)
		{
			IPC_memoryPoolData[cnt].dataVector.clear ();
			IPC_memoryPoolData[cnt].status = IPC_MEMORY_POOL_LOCKED;
			IPC_memoryPoolData[cnt].used = true;

			res = cnt;

			// Report the first available entry
			break;
		}
	}

	return res;
}

tSInt IPC_MemoryPoolGetSize (tSInt id)
{
	if (id >= 0 && id < IPC_MEMORYPOOL_BUFFER_NUM)
	{
		return IPC_memoryPoolData[id].dataVector.size();
	}

	return ERROR_CODE_RESOURCE_UNAVAIL;
}

tSInt IPC_MemoryPoolUnLock (tSInt id)
{
	tSInt res = ERROR_CODE_RESOURCE_UNAVAIL;

	if (id >= 0 && id < IPC_MEMORYPOOL_BUFFER_NUM)
	{
		IPC_memoryPoolData[id].dataVector.clear ();
		IPC_memoryPoolData[id].status = IPC_MEMORY_POOL_FREE;
		IPC_memoryPoolData[id].used = false;

		res = ERROR_CODE_NO_ERROR;
	}

	return res;
}

tSInt IPC_MemoryPoolScheduleForUse (tSInt id)
{
	tSInt res = ERROR_CODE_RESOURCE_UNAVAIL;

	// This function schedule a resource for use
	if (id >= 0 && id < IPC_MEMORYPOOL_BUFFER_NUM)
	{
		if (true  == IPC_memoryPoolData[id].used &&
			IPC_MEMORY_POOL_LOCKED == IPC_memoryPoolData[id].status)
		{
			IPC_memoryPoolData[id].status = IPC_MEMORY_POOL_SCHEDULED_FOR_USE;

			res = ERROR_CODE_NO_ERROR;
		}
	}

	return res;
}

dataVectTy& IPC_MemoryPoolGetVectorReference (tSInt id)
{
	// This vector is used solely for the purpose to return an empty vector
	static dataVectTy tmpVector;

	// Check if we have an invalid value then return an empty vector
	if (id < 0 || id >= IPC_MEMORYPOOL_BUFFER_NUM)
	{
		return tmpVector;
	}

	return IPC_memoryPoolData[id].dataVector;
}

tSInt IPC_MemoryPoolPush (tSInt id, float data1, float data2, float data3, float data4)
{
	tSInt res = ERROR_CODE_RESOURCE_UNAVAIL;

	// This function push data from the memory pool in order to use it
	if (id >= 0 && id < IPC_MEMORYPOOL_BUFFER_NUM)
	{
		IPC_memoryPoolData[id].dataVector.push_back (boost::make_tuple (data1, data2, data3, data4));

		res = ERROR_CODE_NO_ERROR;
	}

	return res;
}

tSInt IPC_MemoryPoolCheckForScheduled (void)
{
	tSInt res = ERROR_CODE_RESOURCE_UNAVAIL;
	tSInt cnt;

	// Check if a slot is free and in case lock it for write and return the slot number
	for (cnt = 0; cnt < IPC_MEMORYPOOL_BUFFER_NUM; cnt++)
	{
		if (true  == IPC_memoryPoolData[cnt].used &&
			IPC_MEMORY_POOL_SCHEDULED_FOR_USE == IPC_memoryPoolData[cnt].status)
		{
			res = cnt;

			break;
		}
	}


	// This function check the resource array to find some entry scheduled for use
	// and return the pool ID

	return res;
}

#endif  // #if (defined APP_USE_MEMORYPOOL)

// End of file
