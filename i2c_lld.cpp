/*
 * i2c_lld.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <bcm2835.h>

#include "i2c_lld.h"

/************************************************************************************/
/*                                                                                  */
/* static char I2C_GetData ( char RegAddress , char BytesNum , char *RegDataPnt )   */
/*                                                                                  */
/************************************************************************************/
char I2C_GetData ( char RegAddress , char BytesNum , char *RegDataPnt )
{
	int wrLen;
	int rdLen;
	char wrBfr [10] = {0x00};
	unsigned char readRes, writeRes;

  // Read data and check result:
  // - BCM2835_I2C_REASON_OK
  // - BCM2835_I2C_REASON_ERROR_NACK
  // - BCM2835_I2C_REASON_ERROR_CLKT
  // - BCM2835_I2C_REASON_ERROR_DATA

  // Send command
  wrLen = BytesNum;
  wrBfr[0] = RegAddress;

  writeRes = bcm2835_i2c_write (wrBfr, wrLen);

  if (BCM2835_I2C_REASON_OK != writeRes)
  {
	  return ( writeRes ) ;
  } /* endif */

  // Extract data
  rdLen = BytesNum;
  readRes = bcm2835_i2c_read (RegDataPnt, rdLen);

  return ( readRes ) ;

} /* end of I2C_GetData() function */

/************************************************************************************************/
/*                                                                                              */
/* static char I2C_GetDataWithAutoIncrement (char RegAddress, char BytesNum, char *RegDataPnt)  */
/*                                                                                              */
/************************************************************************************************/

char I2C_GetDataWithAutoIncrement (char RegAddress, char BytesNum, char *RegDataPnt)
{
	int rdLen;
	char wrBfr [10] = {0x00};
	unsigned char readRes, writeRes;

  // Read data and check result:
  // - BCM2835_I2C_REASON_OK
  // - BCM2835_I2C_REASON_ERROR_NACK
  // - BCM2835_I2C_REASON_ERROR_CLKT
  // - BCM2835_I2C_REASON_ERROR_DATA

  // Send write command asking to read from requested address, length is 1
  wrBfr[0] = RegAddress;

  writeRes = bcm2835_i2c_write (wrBfr, 1);

  if (BCM2835_I2C_REASON_OK != writeRes)
  {
	  return writeRes;
  } /* endif */

  // Send read command reading the requested number of bytes
  rdLen = BytesNum;
  readRes = bcm2835_i2c_read (RegDataPnt, rdLen);

  return readRes;

} /* end of I2C_GetDataWithAutoIncrement() function */

/************************************************************************************/
/*                                                                                  */
/* static char I2C_WriteData ( char RegAddress , char BytesNum , char *RegDataPnt ) */
/*                                                                                  */
/************************************************************************************/
char I2C_WriteData ( char RegAddress , char BytesNum , char *RegDataPnt )
{
  int wrLen ;
  char wrBfr [10] = {0x00};
  unsigned char writeRes;

  wrLen = BytesNum + 1 ;
  wrBfr[0] = RegAddress ;

  memcpy ( &wrBfr [ 1 ] , RegDataPnt , BytesNum ) ;
  writeRes = bcm2835_i2c_write (wrBfr, wrLen);

  return ( writeRes ) ;

} /* end of I2C_WriteData() function */

// End of file

