/*
 * i2C_thread.cpp
 *
 *  Created on: Feb 7, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include <bcm2835.h>

#include "target_config.h"

#include "i2c_thread.h"

#if (defined USE_I2C_DEV)

static pthread_t id;
static bool I2C_exitThread = false;
static I2C_paramsTy I2C_params;
volatile sig_atomic_t I2C_ended = 0;

int I2C_SignalHandler (void)
{
	return I2C_ended;
}

void I2C_ExitThread (bool exitThread)
{
	if (true == exitThread)
	{
		I2C_exitThread = true;
	}
}

bool I2C_GetParameters (I2C_modeEnumTy mode, bcm2835I2CClockDivider clockDiv, unsigned char slaveAddress,
						char *dataBfr, int dataLen)
{
	bool res = false;

    switch (mode)
    {
		case I2C_MODE_INIT:
			I2C_params.slave_address = slaveAddress;
			I2C_params.clkDiv = clockDiv;
			I2C_params.mode = mode;
			res = true;
		break;

		case I2C_MODE_READ:
			if (dataLen < I2C_BUF_MAX_LEN)
			{
				I2C_params.rdLen = dataLen;
				I2C_params.mode = mode;
				res = true;
			}
		break;

		case I2C_MODE_WRITE:
			if (dataLen < I2C_BUF_MAX_LEN)
			{
				memcpy (I2C_params.wrBuf, dataBfr, dataLen);
				I2C_params.rdLen = dataLen;
				I2C_params.mode = mode;
				res = true;
			}
		break;

		case I2C_MODE_CLOSE:
			I2C_params.mode = mode;
			res = true;
		break;

		case I2C_MODE_EXIT:
			I2C_params.mode = mode;
			res = true;
		break;

		default:
			// No code
		break;
    }

    return res;
}

void* I2C_Thread (void *arg)
{
	unsigned char readRes, writeRes;
	unsigned int cnt;
	int initRes;

	// Be sure to stay here
	I2C_exitThread = false;
	I2C_ended = 0;

	// Initialize globals
	memset ((void *)&I2C_params, 0x00, sizeof (I2C_params));
	I2C_params.clkDiv = BCM2835_I2C_CLOCK_DIVIDER_148;

	// Get thread ID to return back at the end
	id = pthread_self ();

///
// USe I2C
//
	initRes = bcm2835_init ();

	if (0 == initRes)
    {
		printf ("Failed to initialize I2C\n");

		return NULL;
    }

	while (false == I2C_exitThread)
    {
	    switch (I2C_params.mode)
	    {
			case I2C_MODE_INIT:
				// Start I2C mode
				bcm2835_i2c_begin ();

				// Setup I2C
				bcm2835_i2c_setSlaveAddress (I2C_params.slave_address);
				bcm2835_i2c_setClockDivider (I2C_params.clkDiv);

				// Return mode to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;

			case I2C_MODE_READ:
				memset (I2C_params.rdBuf, 0x00, I2C_BUF_MAX_LEN);

				readRes = bcm2835_i2c_read (I2C_params.rdBuf, I2C_params.rdLen);

				printf("Read result = %d\n", readRes);

				for (cnt = 0; cnt < I2C_params.rdLen; cnt++)
				{
					printf("Read buffer[%d] = %x\n", cnt, I2C_params.rdBuf[cnt]);
				}

				// Return mode to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;

			case I2C_MODE_WRITE:
				writeRes = bcm2835_i2c_write(I2C_params.wrBuf, I2C_params.wrLen);

				printf("Write result = %d\n", writeRes);

				// Return mode to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;

			case I2C_MODE_CLOSE:
				// Close I2C mode
				bcm2835_i2c_end ();

				// Return mode to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;

			case I2C_MODE_EXIT:
				// Set exit flag
				I2C_exitThread = true;

				// Return mode to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;

			default:
				// Invalid mode, return to idle
				I2C_params.mode = I2C_MODE_NONE;
			break;
	    }
    }

	// Signal thread end
	I2C_ended = 1;

	return (void *)&id;
}

#endif // #if (defined USE_I2C_DEV)

// End of file

