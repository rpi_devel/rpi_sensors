/*
 * hts221_lld.h
 *
 *  Created on: Apr 12, 2015
 *      Author: sandro
 */

#ifndef LSM6DS0_LLD_H_
#define LSM6DS0_LLD_H_

extern tS32 LSM6DS0_Init (void);

extern tS32 LSM6DS0_Open (void);

extern tS32 LSM6DS0_Close (void);

extern tS32 LSM6DS0_ReadAccelerationSequence (tSInt memoryPoolId);

extern void LSM6DS0_AccDataDisplay ( void ) ;

extern void LSM6DS0_SetAccSamplesNum ( int AccSamplesNum ) ;

extern void LSM6DS0_SetAccRange ( int AccelerationRange ) ;

extern void LSM6DS0_SetOutputDataRate ( int OutputDatarate ) ;

extern long int CalculateDeltaTime (struct timeval *starttime, struct timeval *finishtime);

#endif // LSM6DS0_LLD_H_

// End of file
