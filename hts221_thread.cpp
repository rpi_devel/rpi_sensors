/*
 * hts221_thread.cpp
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>		// For 'sig_atomic_t'
#include <semaphore.h>  // For semaphores

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "hts221_lld.h"
#include "hts221_thread.h"

volatile sig_atomic_t HTS221_ended = 0;

static pthread_t hts221Id;

static tBool HTS221_exitThread = false;

static sem_t *HTS221_globalSemaphorePtr = NULL;

void HTS221_ExitThread (tBool exitThread)
{
	if (true == exitThread)
	{
		HTS221_exitThread = true;
	}
}

tS32 HTS221_SignalHandler (void)
{
	return HTS221_ended;
}

void* HTS221_Thread (void *arg)
{
	// Initialize globals
	HTS221_exitThread = false;
	HTS221_ended = 0;

	// Get arguments
	HTS221_globalSemaphorePtr = (sem_t *)arg;

	// Get thread ID to return back at the end
	hts221Id = pthread_self ();

	// Enter critical section
	sem_wait (HTS221_globalSemaphorePtr);

	// Call initialization
	HTS221_Init ();

	// Exit critical section
	sem_post (HTS221_globalSemaphorePtr);

    while (false == HTS221_exitThread)
    {
    	// Enter critical section
    	sem_wait (HTS221_globalSemaphorePtr);

    	// Call data read
    	HTS221_ReadTemperatureAndHumiditySequence ();

    	// Exit critical section
    	sem_post (HTS221_globalSemaphorePtr);

    	// Wait 5s (to avoid to make the file grow to much)
    	sleep (5);
    }

    // Set signal for thread end to 1
    HTS221_ended = 1;

	return (void *)&hts221Id;
}

// End of file
