extern int gpio_dev_open (int gpioNum);

extern int gpio_dev_set_direction (int gpioNum, GPIODEV_directionEnumTy direction);

extern GPIODEV_valEnumTy gpio_dev_get_value (int gpioNum);

extern int gpio_dev_set_value (int gpioNum, GPIODEV_valEnumTy value);

extern int gpio_dev_close (int gpioNum);

// End of file
