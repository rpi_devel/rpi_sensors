/*
 * lsm6ds0_thread.cpp
 *
 *  Created on: Apr 12, 2015
 *      Author: sandro
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>		// For 'sig_atomic_t'
#include <semaphore.h>  // For semaphores

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "lsm6ds0_lld.h"
#include "lsm6ds3_lld.h"
#include "lsm6ds0_thread.h"

#include "ipc.h"

using namespace LSM6DS0_data;

volatile sig_atomic_t AccGyro_ended = 0;

static pthread_t AccGyroId;

static tBool AccGyro_exitThread = false;

static sem_t *AccGyro_globalSemaphorePtr = NULL;

static AccGyro_DeviceEnumTy AccGyro_Device = UNKNOWN_DEVICE;

static tSInt ACCGYRO_memPoolId = -1;

static void ACCGYRO_CheckActions (void);

void AccGyroSelect (tSInt SelectedDeviceId)
{
   if ( LSM6DS0_DEVICE == (AccGyro_DeviceEnumTy)SelectedDeviceId ) {
        AccGyro_Device = LSM6DS0_DEVICE ;
   }
   else if ( LSM6DS3_DEVICE == (AccGyro_DeviceEnumTy)SelectedDeviceId ) {
        AccGyro_Device = LSM6DS3_DEVICE ;
   }
   else {
        AccGyro_Device = UNKNOWN_DEVICE ;
   } /* endif */
}

void AccGyro_ExitThread (tBool exitThread)
{
	if (true == exitThread)
	{
		AccGyro_exitThread = true;
	}
}

tS32 AccGyro_SignalHandler (void)
{
	return AccGyro_ended;
}

void* AccGyro_Thread (void *arg)
{
//	struct sched_param sp;

	// Initialize globals
	AccGyro_exitThread = false;
	AccGyro_ended = 0;
	ACCGYRO_memPoolId = -1;

	// Get arguments
	AccGyro_globalSemaphorePtr = (sem_t *)arg;

#if 0
	// Get thread ID to return back at the end
	AccGyroId = pthread_self ();

	// Set our thread to real time priority
	sp.sched_priority = 1; // Must be > 0. POSIX guarantee 32 levels

	if (pthread_setschedparam (AccGyroId, SCHED_FIFO, &sp))
	{
		fprintf(stderr, "WARNING: Failed to set stepper thread to real-time priority\n");
	}
#endif

	// Enter critical section
	sem_wait (AccGyro_globalSemaphorePtr);

    // Call initialization

    if (LSM6DS3_DEVICE == AccGyro_Device)
    {
         LSM6DS3_Init() ;
    }
    else
    {
         LSM6DS0_Init() ;
    } /* endif */

	// Exit critical section
	sem_post (AccGyro_globalSemaphorePtr);

    while (false == AccGyro_exitThread)
    {
    	// Check for actions
    	ACCGYRO_CheckActions ();

    	// Enter critical section
    	sem_wait (AccGyro_globalSemaphorePtr);

    	// Call data read
        if ( LSM6DS3_DEVICE == AccGyro_Device )
        {
             LSM6DS3_ReadAccelerationSequence (ACCGYRO_memPoolId);
        }
        else
        {
             LSM6DS0_ReadAccelerationSequence (ACCGYRO_memPoolId);
        } /* endif */

    	// Exit critical section
    	sem_post (AccGyro_globalSemaphorePtr);

    	usleep (1500); // wait 1000 usec
    }

    // Set signal for thread end to 1
    AccGyro_ended = 1;

	return (void *)&AccGyroId;
}

static void ACCGYRO_CheckActions (void)
{
	#define ACCGYRO_SAMPLES_NUM_FOR_PLOT_THR		2048

	// Lock the memory pool resource
#if (defined APP_USE_MEMORYPOOL)
	tSInt numberOfSamples;

	// If we have an invalid entry we need to ask for a new slot to write into
	if (ACCGYRO_memPoolId < 0)
	{
		ACCGYRO_memPoolId = IPC_MemoryPoolLockForWrite ();
	}
	else
	{
		// We check for sample quantity and then we decide when to declare to use them
		numberOfSamples = IPC_MemoryPoolGetSize (ACCGYRO_memPoolId);

		if (numberOfSamples >= ACCGYRO_SAMPLES_NUM_FOR_PLOT_THR)
		{
			IPC_MemoryPoolScheduleForUse (ACCGYRO_memPoolId);

			// We need another slot to continue writing because current one just declared readable
			ACCGYRO_memPoolId  = IPC_MemoryPoolLockForWrite ();
		}
	}
#endif // #if (defined APP_USE_MEMORYPOOL)
}

// End of file



