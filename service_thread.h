/*
 * service_thread.h
 *
 *  Created on: May 3, 2015
 *      Author: alberto
 */

#ifndef SERVICE_THREAD_H_
#define SERVICE_THREAD_H_

extern void ST_ExitThread (tBool exitThread);

extern tS32 ST_SignalHandler (void);

extern void* ST_Thread (void *arg);

#endif /* SERVICE_THREAD_H_ */
