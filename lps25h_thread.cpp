/*
 * lps25h_thread.cpp
 *
 *  Created on: Mar 25, 2015
 *      Author: alberto
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>		// For 'sig_atomic_t'
#include <semaphore.h>  // For semaphores

#include "target_config.h"

#include "types.h"
#include "defines.h"
#include "macros.h"

#include "lps25h_lld.h"

#include "lps25h_thread.h"

volatile sig_atomic_t LPS25H_ended = 0;

static pthread_t LPS25H_id;

static tBool LPS25H_exitThread = false;

static sem_t *LPS25H_globalSemaphorePtr = NULL;

void LPS25H_ExitThread (tBool exitThread)
{
	if (true == exitThread)
	{
		LPS25H_exitThread = true;
	}
}

tS32 LPS25H_SignalHandler (void)
{
	return LPS25H_ended;
}

void* LPS25H_Thread (void *arg)
{
	tS32 initRes;

	// Initialize globals
	LPS25H_exitThread = false;
	LPS25H_ended = 0;

	// Get arguments
	LPS25H_globalSemaphorePtr = (sem_t *)arg;

	// Get thread ID to return back at the end
	LPS25H_id = pthread_self ();

	// Enter critical section
	sem_wait (LPS25H_globalSemaphorePtr);

	// Call initialization
	initRes = LPS25H_Init ();

	// Exit critical section
	sem_post (LPS25H_globalSemaphorePtr);

	if (ERROR_CODE_NO_ERROR == initRes)
	{
		while (false == LPS25H_exitThread)
		{
			// Enter critical section
			sem_wait (LPS25H_globalSemaphorePtr);

			// Call data read
			LPS25H_ReadPressureAndTemperatureSequence ();

			// Exit critical section
			sem_post (LPS25H_globalSemaphorePtr);

			// Wait 900ms
			usleep (900000);
		}
	}
	else
	{
		// Print error
	}

    // Set signal for thread end to 1
    LPS25H_ended = 1;

	return (void *)&LPS25H_id;
}

// End of file
