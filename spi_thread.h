/*
 * spi_thread.h
 *
 *  Created on: Jan 31, 2015
 *      Author: alberto
 */

#ifndef SPI_THREAD_H_
#define SPI_THREAD_H_

extern void* SPI_Thread (void *arg);

extern void SPI_ExitThread (bool exitThread);

extern int SPI_SignalHandler (void);

#endif /* SPI_THREAD_H_ */
