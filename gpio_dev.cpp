#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <math.h>

#include "gpio_common.h"
#include "gpio_dev.h"

#define GPIODEV_MAX_NUM_GPIO		255

#define MAX_BUFFER_SIZE				255

// This struct contains the status for open GPIOs.
// GPIOs are used as plain array from 0 to 255
typedef struct
{
	GPIODEV_statusEnumTy status;
	GPIODEV_alternateFnctEnumTy altFnct;
	GPIODEV_directionEnumTy dir;
	GPIODEV_valEnumTy value;
} GPIODEV_dataTy;

static GPIODEV_dataTy GPIODEV_data[GPIODEV_MAX_NUM_GPIO];

static char GPIODEV_buffer[MAX_BUFFER_SIZE];

int gpio_dev_open (int gpioNum)
{
	int exportFd, res = -1;
	char *bfrPtr = &GPIODEV_buffer[0];
	int bfrLen;

	exportFd = open ("/sys/class/gpio/export", O_WRONLY);

	// Check if we opened correctly the GPIO directory
	if (0 != exportFd)
	{
		sprintf (bfrPtr, "%d", gpioNum);

		bfrLen = strlen (bfrPtr) + 1; // Take into account the termination

		// Open the device (it could fail if already open
		if (GPIODEV_STATUS_OPEN != GPIODEV_data[gpioNum].status)
		{
			res = write (exportFd, bfrPtr, bfrLen);

			close (exportFd);

			if (-1 != res)
			{
				GPIODEV_data[gpioNum].status = GPIODEV_STATUS_OPEN;
				GPIODEV_data[gpioNum].altFnct = GPIODEV_ALTFNCT_GPIO;

				res = 0;
			}
			else
			{
				GPIODEV_data[gpioNum].status = GPIODEV_STATUS_ERROR;
				GPIODEV_data[gpioNum].altFnct = GPIODEV_ALTFNCT_UNKNOWN;

				res = -1;
			}
		}
	}

	return res;
}

int gpio_dev_set_direction (int gpioNum, GPIODEV_directionEnumTy direction)
{
	int fd, res = -1;

	// Check error
	if (GPIODEV_STATUS_OPEN != GPIODEV_data[gpioNum].status ||
		GPIODEV_ALTFNCT_GPIO != GPIODEV_data[gpioNum].altFnct)
	{
		return res;
	}

	sprintf (GPIODEV_buffer, "/sys/class/gpio/gpio%d/direction", gpioNum);

	fd = open (GPIODEV_buffer, O_WRONLY);

	// Check if we opened correctly the GPIO file
	if (0 != fd)
	{
		// Set out direction
		if (GPIODEV_DIRECTION_OUT == direction)
		{
			// Set GPIO out direction
			res = write (fd, "out", 3);

			if (-1 != res)
			{
				GPIODEV_data[gpioNum].dir = GPIODEV_DIRECTION_OUT;

				res = 0;
			}
			else
			{
				GPIODEV_data[gpioNum].dir = GPIODEV_DIRECTION_UNKNOWN;

				res = -1;
			}
		}
		else
		{
			// Set GPIO in direction
			res = write (fd, "in", 2);

			if (-1 != res)
			{
				GPIODEV_data[gpioNum].dir = GPIODEV_DIRECTION_IN;

				res = 0;
			}
			else
			{
				GPIODEV_data[gpioNum].dir = GPIODEV_DIRECTION_UNKNOWN;

				res = -1;
			}
		}

		close (fd);
	}

	return res;
}

GPIODEV_valEnumTy gpio_dev_get_value (int gpioNum)
{
	int fd, res = -1;
	char value;
	GPIODEV_valEnumTy retVal = GPIODEV_VALUE_UNKNOWN;

	sprintf (GPIODEV_buffer, "/sys/class/gpio/gpio%d/value", gpioNum);

	fd = open (GPIODEV_buffer, O_RDONLY);

	// Check if we opened correctly the GPIO file
	if (0 != fd)
	{
		res = read (fd, &value, 1);

		// Check if read was successful
		if (-1 != res)
		{
			// Now check GPIO value
			if (value == '0')
			{
				 // Current GPIO status low
				retVal = GPIODEV_VALUE_LOW;
			}
			else
			{
				 // Current GPIO status high
				retVal = GPIODEV_VALUE_HIGH;
			}
		}

		close (fd);
	}

	return retVal;
}

int gpio_dev_set_value (int gpioNum, GPIODEV_valEnumTy value)
{
	int fd, res = -1;

	// Check error
	if (GPIODEV_STATUS_OPEN != GPIODEV_data[gpioNum].status ||
		GPIODEV_ALTFNCT_GPIO != GPIODEV_data[gpioNum].altFnct ||
		GPIODEV_DIRECTION_OUT != GPIODEV_data[gpioNum].dir)
	{
		return res;
	}

	sprintf (GPIODEV_buffer, "/sys/class/gpio/gpio%d/value", gpioNum);

	fd = open (GPIODEV_buffer, O_WRONLY);

	// Check if we open correctly GPIO file
	if (0 != fd)
	{
		// Set value if the GPIO is in out direction
		if (GPIODEV_DIRECTION_OUT == GPIODEV_data[gpioNum].dir &&
			GPIODEV_VALUE_HIGH == value)
		{
			// Set GPIO high status
			res = write (fd, "1", 1);

			if (-1 != res)
			{
				GPIODEV_data[gpioNum].value = GPIODEV_VALUE_HIGH;

				res = 0;
			}
			else
			{
				GPIODEV_data[gpioNum].value = GPIODEV_VALUE_UNKNOWN;

				res = -1;
			}
		}
		else if (GPIODEV_DIRECTION_OUT == GPIODEV_data[gpioNum].dir &&
				 GPIODEV_VALUE_LOW == value)
		{
			// Set GPIO low status
			res = write (fd, "0", 1);

			if (-1 != res)
			{
				GPIODEV_data[gpioNum].value = GPIODEV_VALUE_LOW;

				res = 0;
			}
			else
			{
				GPIODEV_data[gpioNum].value = GPIODEV_VALUE_UNKNOWN;

				res = -1;
			}
		}

		close (fd);
	}

	return res;
}

int gpio_dev_close (int gpioNum)
{
	int unexportFd, res = -1;
	char *bfrPtr = &GPIODEV_buffer[0];
	int bfrLen;

	unexportFd = open ("/sys/class/gpio/unexport", O_WRONLY);

	if (0 != unexportFd)
	{
		sprintf (bfrPtr, "%d", gpioNum);

		bfrLen = strlen (bfrPtr) + 1; // Take into account the termination

		res = write (unexportFd, bfrPtr, bfrLen);

		close (unexportFd);

		if (-1 != res)
		{
			GPIODEV_data[gpioNum].status = GPIODEV_STATUS_CLOSED;

			res = 0;
		}
		else
		{
			GPIODEV_data[gpioNum].status = GPIODEV_STATUS_ERROR;

			res = -1;
		}
	}

	return res;
}

// End of file
